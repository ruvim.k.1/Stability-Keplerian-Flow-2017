
import os; 
import glob; 
import numpy; 
import scipy.misc; 
import scipy.optimize; 
import matplotlib.pyplot as pp; 
import openpiv.scaling; 

def get_dir (what = "in", amps = "0.10A"): 
	folder = "Recordings"; 
	if what == "out-piv": 
		folder = "PIV-Output"; 
	return "/Ruvim/{}/Setup-3-e/{}".format (folder, amps); 

def spyder_is_silly (): 
	return "Silly!"; 

flag__do_plot = True; 
flag__include_title = False; 

figure_size = (6, 4); 

overall_dir = get_dir ("out-piv", "Overall"); 

write_dir = "Plots"; 

I_min = 0; 
I_max = 1.00; 

# "params.txt" file format: 
# [0:6]:		I (A)	A_d()	B_d()	delta{B_d}	A_p()	delta{A_p}	
# [6:10]:		Ap yint est	Ap yint std	Ap slope est	Ap slope std 

all_csv_files = glob.glob (overall_dir + "/*.csv"); 

for filename in all_csv_files: 
	params = numpy.loadtxt (filename); 
	
	parts1 = filename.split (".csv") [0].replace ("\\", "/").split ("/"); 
	last_part = parts1[len (parts1) - 1]; 
	
	write_dir = "{}/{}/{}".format (overall_dir, "Plots", last_part); 
	
	if not os.path.exists (write_dir): 
		os.makedirs (write_dir); 
	
	I_max = params[:, 0].max (); 
	
	if flag__do_plot: 
		pp.figure (figsize = figure_size); 
		if flag__include_title: 
			pp.title ("Scaling Constant versus Current"); 
		pp.xlabel ("Current (A)"); 
		pp.ylabel ("$A_d$"); 
		pp.xlim (I_min, I_max); 
		pp.plot (params[:, 0], params[:, 1], 'o-'); 
		pp.savefig (write_dir + "/scaling-const.png", dpi = 300); 
		
		pp.figure (figsize = figure_size); 
		if flag__include_title: 
			pp.title ("Power Profile versus Current"); 
		pp.xlabel ("Current (A)"); 
		pp.ylabel ("$B_d$"); 
		pp.xlim (I_min, I_max); 
		pp.ylim (-2, 4); 
		pp.errorbar (params[:, 0], params[:, 2], params[:, 3]); 
		pp.savefig (write_dir + "/power-profile.png", dpi = 300); 
		
		pp.figure (figsize = figure_size); 
		if flag__include_title: 
			pp.title ("Proportionality Constant"); 
		pp.xlabel ("Current (A)"); 
		pp.ylabel ("$A_p$"); 
		pp.xlim (I_min, I_max); 
		pp.errorbar (params[:, 0], params[:, 4], params[:, 5]); 
		pp.savefig (write_dir + "/prop-const-mean.png", dpi = 300); 
		
		pp.figure (figsize = figure_size); 
		if flag__include_title: 
			pp.title ("Prop. Const.'s Slope"); 
		pp.xlabel ("Current (A)"); 
		pp.ylabel ("$\\partial A_p / \\partial r$"); 
		pp.xlim (I_min, I_max); 
		pp.errorbar (params[:, 0], params[:, 8], params[:, 9]); 
		pp.savefig (write_dir + "/prop-const-slope.png", dpi = 300); 
		
		pp.figure (figsize = figure_size); 
		if flag__include_title: 
			pp.title ("Prop. Const.'s Y-Int."); 
		pp.xlabel ("Current (A)"); 
		pp.ylabel ("$A_p$ where $r=0$"); 
		pp.xlim (I_min, I_max); 
		pp.errorbar (params[:, 0], params[:, 6], params[:, 7]); 
		pp.savefig (write_dir + "/prop-const-yint.png", dpi = 300); 
		
		pp.figure (figsize = figure_size); 
		if flag__include_title: 
			pp.title ("Top and Bottom Percentiles of Angular Velocity"); 
		pp.xlabel ("Current (A)"); 
		pp.ylabel ("Angular Velocity (deg/s)"); 
		pp.xlim (I_min, I_max); 
		pp.plot (params[:, 0], params[:, 10] * 180 / numpy.pi, label = "10%"); 
		pp.plot (params[:, 0], params[:, 11] * 180 / numpy.pi, label = "90%"); 
		pp.legend (loc = "best"); 
		pp.savefig (write_dir + "/angular-velocity-percentiles.png", dpi = 300); 
		
		pp.close ("all"); 

