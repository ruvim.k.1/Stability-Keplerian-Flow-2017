import os; 
import numpy; 
#import scipy.misc; 
#import scipy.optimize; 
import matplotlib.pyplot as pp; 

write_dir = "Plots"; 

I_min = 0.05; 
I_max = 1.00; 

# "params.txt" file format: 
# [0:6]:		I (A)	A_d()	B_d()	delta{B_d}	A_p()	delta{A_p}	
# [6:10]:		Ap yint est	Ap yint std	Ap slope est	Ap slope std 
params = numpy.loadtxt ("./params.txt"); 
params = params[params[:, 0] < 1, :]; 

if not os.path.exists (write_dir): 
	os.makedirs (write_dir); 

pp.figure (figsize = (6, 4)); 
pp.title ("Scaling Constant versus Current"); 
pp.xlabel ("Current (A)"); 
pp.ylabel ("$A_d$"); 
pp.xlim (I_min, I_max); 
pp.plot (params[:, 0], params[:, 1], 'o-'); 
pp.savefig (write_dir + "/scaling-const.png", dpi = 300); 

pp.figure (figsize = (6, 4)); 
pp.title ("Power Profile versus Current"); 
pp.xlabel ("Current (A)"); 
pp.ylabel ("$B_d$"); 
pp.xlim (I_min, I_max); 
#pp.ylim (-2, 0); 
pp.errorbar (params[:, 0], params[:, 2], params[:, 3]); 
pp.savefig (write_dir + "/power-profile.png", dpi = 300); 

pp.figure (figsize = (6, 4)); 
pp.title ("Proportionality Constant versus Current"); 
pp.xlabel ("Current (A)"); 
pp.ylabel ("$A_p$"); 
pp.xlim (I_min, I_max); 
pp.errorbar (params[:, 0], params[:, 4], params[:, 5]); 
pp.savefig (write_dir + "/prop-const-mean.png", dpi = 300); 

pp.figure (figsize = (6, 4)); 
pp.title ("Prop. Const.'s Slope versus Current"); 
pp.xlabel ("Current (A)"); 
pp.ylabel ("$\\partial A_p / \\partial r$"); 
pp.xlim (I_min, I_max); 
pp.errorbar (params[:, 0], params[:, 6], params[:, 7]); 
pp.savefig (write_dir + "/prop-const-slope.png", dpi = 300); 

pp.figure (figsize = (6, 4)); 
pp.title ("Prop. Const.'s Y-Int. versus Current"); 
pp.xlabel ("Current (A)"); 
pp.ylabel ("$A_p$ where $r=0$"); 
pp.xlim (I_min, I_max); 
pp.errorbar (params[:, 0], params[:, 8], params[:, 9]); 
pp.savefig (write_dir + "/prop-const-yint.png", dpi = 300); 

