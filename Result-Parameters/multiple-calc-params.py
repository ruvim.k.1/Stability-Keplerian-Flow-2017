
import os; 
import glob; 
import numpy; 
import scipy.misc; 
import scipy.optimize; 
import matplotlib.pyplot as pp; 
import openpiv.scaling; 

def get_dir (what = "in", amps = "0.10A"): 
	folder = "Recordings"; 
	if what == "out-piv": 
		folder = "PIV-Output"; 
	return "/Ruvim/{}/Setup-3-e/{}".format (folder, amps); 

def spyder_is_silly (): 
	return "Silly!"; 

flag__do_plot = True; 

overall_dir = get_dir ("out-piv", "Overall"); 

I_min = 0; 
I_max = 1.00; 

# "params.txt" file format: 
# [0:6]:		I (A)	A_d()	B_d()	delta{B_d}	A_p()	delta{A_p}	
# [6:10]:		Ap yint est	Ap yint std	Ap slope est	Ap slope std 

all_csv_files = glob.glob (overall_dir + "/*.csv"); 

tot_head = "DataSet\tBd est\tBd err\tAp est\tAp err" + \
	"\tBtm. Coef. Est.\tBtm. Coef. Err.\tTop Coef. Est.\tTop Coef. Err."; 
tot_data = numpy.zeros ((0, 9), dtype = str); 

num_data = numpy.zeros ((0, 8)); 

for filename in all_csv_files: 
	params = numpy.loadtxt (filename); 
	
	parts1 = filename.split (".csv") [0].replace ("\\", "/").split ("/"); 
	last_part = parts1[len (parts1) - 1]; 
	
	bad = numpy.isnan (params[:, 2]) | numpy.isnan (params[:, 3]) | \
		numpy.isnan (params[:, 4]) | numpy.isnan (params[:, 5]); 
	no_bad = ~bad; 
	
	I = params[:, 0][no_bad]; 
	B_d = params[:, 2][no_bad]; 
	sBd = params[:, 3][no_bad]; 
	A_p = params[:, 4][no_bad]; 
	sAp = params[:, 5][no_bad]; 
	
	bottom_percentiles = params[:, 10][no_bad]; 
	top_percentiles = params[:, 11][no_bad]; 
	
	# Taylor Error Analysis, page 175, chapter 7.2: 
	wBd = 1 / sBd ** 2; 
	wAp = 1 / sAp ** 2; 
	
	# Same page, plus page 176: 
	Bd_best_x = (wBd * B_d).sum () / wBd.sum (); 
	Bd_best_e = 1 / numpy.sqrt (wBd.sum ()); 
	
	Ap_best_x = (wAp * A_p).sum () / wAp.sum (); 
	Ap_best_e = 1 / numpy.sqrt (wAp.sum ()); 
	
	btm_ratio = bottom_percentiles / I; 
	top_ratio = top_percentiles / I; 
	
	btm_best_x = btm_ratio.mean (); 
	btm_best_e = btm_ratio.std (); 
	top_best_x = top_ratio.mean (); 
	top_best_e = top_ratio.std (); 
	
	s_row = numpy.array ([last_part, str (Bd_best_x), str (Bd_best_e), \
			str (Ap_best_x), str (Ap_best_e), \
			str (btm_best_x), str (btm_best_e), str (top_best_x), str (top_best_e)], \
		dtype = str).reshape ((1, -1)); 
	f_row = numpy.array ([Bd_best_x, Bd_best_e, Ap_best_x, Ap_best_e, \
				btm_best_x, btm_best_e, top_best_x, top_best_e]).reshape ((1, -1)); 
	tot_data = numpy.concatenate ((tot_data, s_row), axis = 0); 
	num_data = numpy.concatenate ((num_data, f_row), axis = 0); 

write_dir = "{}/{}".format (overall_dir, "Calculations"); 

if not os.path.exists (write_dir): 
	os.makedirs (write_dir); 
numpy.savetxt (write_dir + "/overall-results.csv", tot_data, \
			delimiter = "\t", header = tot_head, fmt = "%s"); 
numpy.savetxt (write_dir + "/overall-numbers.dat", num_data, delimiter = "\t"); 
spyder_is_silly (); 

