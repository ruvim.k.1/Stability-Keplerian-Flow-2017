60 / 325 

r1 = 197 * 60 / 325 = 36.4 mm 
T1 = 12 s 
w1 = 360 deg / T1 = 30 deg/s 

r2 = 282 * 60 / 325 = 52.1 mm 
T2 = 12 s 
w2 = 30 deg/s 

r3 = 363.6 * 60 / 325 = 67.1 mm 
T3 = 14 s 
w3 = 25.7 deg/s 


if: w = A * r^{-1.5} 
then: A = w * r^1.5 

A1 = w1 * r1^1.5 = 6.588e3 
A2 = w2 * r2^1.5 = 1.649e4 
A3 = w3 * r3^1.5 = 1.413e4 

So, A = 1.5e4 ? 

