# -*- coding: utf-8 -*-
"""
Created on Sat Nov 19 16:06:17 2016

@author: SpongeBob
"""

import random; 
import string; 
import numpy; 
import matplotlib.pyplot as pp; 

data = numpy.loadtxt ("KJ Magnetics N42-14-14 B Strength.txt", skiprows = 1); 

dist = data[:, 0]; 
gaus = data[:, 1]; 

#pp.figure (); 
#pp.plot (dist, numpy.exp (-dist * dist)); 

valid_range = dist >= 0.4; 

dist3 = dist ** 3; 
over_dist3 = 1 / dist3; 
fit3 = numpy.polyfit (over_dist3[valid_range], gaus[valid_range], 1); # Fit a 1/r^3 curve. 
M_arr = gaus[valid_range] * dist3[valid_range] / 2; 
M_mean = M_arr.mean (); 
M_std = M_arr.std (); 
gaus3 = 2 * M_mean * over_dist3; 

#samples_x = over_dist3[valid_range]; 
#samples_y = gaus[valid_range]; 
#
#taylor_fit_N = valid_range.sum (); # Number of points. 
#taylor_fit_delta = taylor_fit_N * (samples_x ** 2).sum () - samples_x.sum () ** 2; 
#taylor_fit_A = ((samples_x ** 2).sum () * samples_y.sum () - \
#				samples_x.sum () * (samples_x * samples_y).sum ()) \
#			/ taylor_fit_delta; 
#taylor_fit_B = (taylor_fit_N * (samples_x * samples_y).sum () - \
#				samples_x.sum () * samples_y.sum ()) \
#			/ taylor_fit_delta; 
#taylor_sigma_y = numpy.sqrt (1 / (taylor_fit_N - 2) * ( 
#				(samples_y - taylor_fit_A - taylor_fit_B * samples_x) ** 2 
#			).sum ()); 
#taylor_sigma_A = taylor_sigma_y * numpy.sqrt ((samples_x ** 2).sum () / taylor_fit_delta); 
#taylor_sigma_B = taylor_sigma_y * numpy.sqrt (taylor_fit_N / taylor_fit_delta); 

#t_M_in = numpy.array ([taylor_fit_B / 2, taylor_sigma_B / 2]); 
t_M_in = numpy.array ([M_mean, M_std]); 
t_M_mm = t_M_in * (25.4 ** 3); 
t_M_m = t_M_mm * (1e-3 ** 3); 


pp.figure (); 
pp.xlabel ("Position (in)"); 
pp.ylabel ("M (G in^3)"); 
pp.plot (dist[valid_range], M_arr, 'o', label = "$M = r^3 B (r) / 2$"); 
pp.plot (dist[valid_range], t_M_in[0] * numpy.ones_like (dist[valid_range]), linewidth = 2, label = "Dipole Fit"); 
pp.legend (loc = "best"); 
pp.savefig ("KJ_Magnetics_N42_14_14_B_Strength_M_fit_plot.png", dpi = 300); 


pp.figure (); 
pp.xlabel ("Position (in)"); 
pp.ylabel ("Field (gauss)"); 
pp.plot (dist[valid_range], gaus[valid_range], 'o', label = "K&J Calculated Points"); 
pp.plot (dist[valid_range], gaus3[valid_range], label = "$1/r^3$ Dipole Fit", linewidth = 3); 
pp.legend (loc = "best"); 
pp.savefig ("KJ_Magnetics_N42_14_14_B_Strength_just_fit3.eps", dpi = 300); 
pp.savefig ("KJ_Magnetics_N42_14_14_B_Strength_just_fit3.png", dpi = 300); 
#pp.savefig ("".join (random.choice (string.ascii_letters + string.digits) for _ in range (5)) + ".png"); 



pp.figure (figsize = (6, 4)); 
#pp.title ("Theoretical Magnetic Field Strength Above Magnet"); 
pp.xlabel ("Position (in)"); 
pp.ylabel ("Field (gauss)"); 
pp.plot (dist, gaus, 'o', label = "K&J Calculated Points"); 
pp.plot (dist[gaus3 < 6e3], gaus3[gaus3 < 6e3], label = "$1/r^3$ Dipole Fit", linewidth = 3); 
pp.legend (loc = "best"); 
pp.savefig ("KJ_Magnetics_N42_14_14_B_Strength_with_fit3.eps", dpi = 300); 
pp.savefig ("KJ_Magnetics_N42_14_14_B_Strength_with_fit3.png", dpi = 300); 
#pp.savefig ("".join (random.choice (string.ascii_letters + string.digits) for _ in range (5)) + ".png"); 



with open ("KJ_Magnetics_N42_14_14_B_Strength_Fit_Result.txt", "w") as result: 
	M_in = fit3[0] / 2; 
	print ("Result of the magnetic dipole moment fit on the tail of the field strength data: \n", file = result); 
	print ("\tM = {} gauss / in^3\n".format (M_in), file = result); 
	M_mm = M_in * (25.4 ** 3); 
	print ("\tM = {} gauss / mm^3\n".format (M_mm), file = result); 
	M_m = M_mm * (1e-3 ** 3); 
	print ("\tM = {} gauss / m^3\n".format (M_m), file = result); 

with open ("KJ_Magnetics_N42_14_14_B_Strength_Fit_Result_with_Uncertainty.txt", "w") as result: 
	print ("Result of the magnetic dipole moment fit on the tail of the field strength data: \n", file = result); 
	print ("\tM = {} ± {} gauss / in^3\n".format (t_M_in[0], t_M_in[1]), file = result); 
	print ("\tM = {} ± {} gauss / mm^3\n".format (t_M_mm[0], t_M_mm[1]), file = result); 
	print ("\tM = {} ± {} gauss / m^3\n".format (t_M_m[0], t_M_m[1]), file = result); 



