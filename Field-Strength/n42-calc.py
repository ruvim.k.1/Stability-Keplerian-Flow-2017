# -*- coding: utf-8 -*-
"""
Created on Fri May 12 15:17:53 2017

@author: Borrero Lab
"""

import numpy; 
import matplotlib.pyplot as pp; 

meas = numpy.loadtxt ("n42-meas.csv"); 

i_start = 0; 
i_step = 1; 

r = meas[i_start::i_step, 0]; 
B = meas[i_start::i_step, 3]; 

some_r = numpy.linspace (r.min (), r.max (), 64); 
some_1 = numpy.ones_like (some_r); 

M = (1/2) * B * r ** 3; 
M_mean = M.mean (); 
M_med = numpy.median (M); 
M_std = M.std (); 

pp.figure (figsize = (6, 4)); 
pp.xlabel ("Position (mm)"); 
pp.ylabel ("M (G mm^3)"); 
pp.plot (r, M, 'o', label = "$M = \\frac{1}{2} B (r) r^3$ Samples"); 
pp.plot (some_r, M_med * some_1, label = "Median"); 
pp.plot (some_r, M_mean * some_1, label = "Mean"); 
pp.legend (loc = "best"); 
pp.savefig ("notitle-n42-M-plot.png", bbox_inches = "tight", dpi = 300); 
pp.title ("Dipole Magnetization Constant from B(r) Samples"); 
pp.savefig ("n42-M-plot.png", bbox_inches = "tight", dpi = 300); 

pp.figure (figsize = (6, 4)); 
pp.xlabel ("Position (mm)"); 
pp.ylabel ("M (G mm^3)"); 
pp.plot (r, M, 'o', label = "$M = \\frac{1}{2} B (r) r^3$ Samples"); 
pp.plot (some_r, M_mean * some_1, label = "Mean", linewidth = 2); 
pp.legend (loc = "best"); 
pp.savefig ("notitle-n42-M-plot2.png", bbox_inches = "tight", dpi = 300); 
pp.title ("Dipole Magnetization Constant from B(r) Samples"); 
pp.savefig ("n42-M-plot2.png", bbox_inches = "tight", dpi = 300); 

pp.figure (figsize = (6, 4)); 
pp.xlabel ("Position (mm)"); 
pp.ylabel ("Field (G)"); 
pp.plot (r, B, 'o', label = "Measurements"); 
pp.plot (some_r, 2 * M_mean * some_r ** (-3), label = "Dipole Mean", linewidth = 2); 
pp.legend (loc = "best"); 
pp.savefig ("notitle-n42-B-plot-mean.png", bbox_inches = "tight", dpi = 300); 
pp.title ("Dipole Fit onto Field Measurements above Magnet"); 
pp.savefig ("n42-B-plot-mean.png", bbox_inches = "tight", dpi = 300); 

pp.figure (figsize = (6, 4)); 
pp.xlabel ("Position (mm)"); 
pp.ylabel ("Field (G)"); 
pp.plot (r, B, 'o', label = "Measurements"); 
pp.plot (some_r, 2 * M_med * some_r ** (-3), label = "Dipole Median", linewidth = 2); 
pp.legend (loc = "best"); 
pp.savefig ("notitle-n42-B-plot-med.png", bbox_inches = "tight", dpi = 300); 
pp.title ("Dipole Fit onto Field Measurements above Magnet"); 
pp.savefig ("n42-B-plot-med.png", bbox_inches = "tight", dpi = 300); 


with open ("n42-result.txt", "w") as file: 
	print ("M = {} ± {} mm^3 G".format (M_mean, M_std), file = file); 


