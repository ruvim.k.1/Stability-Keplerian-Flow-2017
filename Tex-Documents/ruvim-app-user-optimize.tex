
\chapter{ Optimizing Magnet Arrangement \label{ch:user-optimize} } 

This chapter is meant to be a guide to using our project's Python code to optimize a magnet arrangement. The code may be downloaded from: \\

\archiveBaseURL{}\url{/t17/User-Optimize-Code.zip} \\

Our code requires Python with scientific libraries (NumPy, etc.). An easy way to acquire all the libraries with Python is to download the Anaconda package. Installing Anaconda is not difficult, so we leave the procedures for that up to the reader to figure out. Our code also requires the Bokeh package, which does not come pre-installed with Anaconda. The first section will guide the reader through the brief process of installing Bokeh, while \sect{section:user-optimize:using-python} instructs the reader on how to use our code (no programming required!) to optimize a magnet arrangement. \\

\input{install-bokeh} 

\section{ Using Python Code \label{section:user-optimize:using-python} } 

% The process for optimizing a magnet arrangement is extremely simple, and it does not require programming. However, it does require the Bokeh library and the user-optimize code. The code may be downloaded from: \\

% The Python code may be downloaded from: 

% \archiveBaseURL{}\url{/t17/User-Optimize-Code.zip} \\

Once the ZIP archive is downloaded and extracted, it may be easily run. If on Linux, open a Terminal (Ctrl + Alt + T), change into the directory with the user-optimize.py Python script, and run the following: \\

\texttt{bokeh serve --show user-optimize.py} \\

If on Windows, we have provided a batch script with the above command saved in it. Simply double-click the ``user-optimize.bat'' file to launch the Python program. \\

\begin{figure}[!h] 
	\begin{center} 
		\includegraphics[width=6in]{Figures/April-5/Bokeh-iface.png} 
	\end{center} 
	\caption{ The Bokeh library allowed us to create sliders and text input for allowing the user to optimize the magnet arrangement parameters. \label{scr:apr5:bokeh-iface} } 
\end{figure} 

We thought about what would be the best form of user input, and we searched online for libraries that allow for interactive plots in Python. \textit{Bokeh} seemed attractive because it can be used to create sliders. All the user would have to do is slide a slider and watch the graph change. We installed Bokeh on the Borrero Lab computer and built the interface shown in \fig{scr:apr5:bokeh-iface}. The sliders on the left control the height of each magnet ring below the XY-plane, and the sliders on the right control the radius of each ring. The radius is in \si{mm}, but the height is in reciprocal length units to make fine-tuning the magnetic field easier. These units are converted back to \si{mm} before being saved to the CSV spreadsheet. There is currently no way to adjust the rotation angles from the user interface, so these must be adjusted manually in the spreadsheet that the Python code saves to. The ``Water Z'' slider controls how high the water level is above the XY-plane, the default being 0. This slider was created because we needed to see how a mistake in the water level height can affect the magnetic field profile at the water's surface. \\

\fig{scr:apr5:bokeh-plot} shows the feedback part of the user-optimize Python tool we developed to assist us in optimizing for the magnet arrangement in Setup-3. In the plot, the purple dots represent our physical measurements of what the magnetic field actually was. These were not there when we were designing the magnet arrangement; they appear only after we build the setup and take and record measurements of the magnetic field. When we built the setup, the physically-measured magnetic field was, for the most part, only about 80\% of the predicted field, so we set the $M$ to be 80\% of the original $M$ for that plot for Setup-3. The two sliders, ``Water Z'' and ``Magnet M'', are there mainly for checking to make sure the water is positioned correctly vertically and the magnetization constant $M$ is calculated correctly. The ``Magnet M'' slider controls how much to scale the magnets' magnetization constant by before calculating the predicted magnetic field to put in the plot. \\

\begin{figure}[!h] 
	\begin{center} 
		\includegraphics[width=6in]{Figures/April-26/Bokeh-plot.png} 
	\end{center} 
	\caption{ The Bokeh plot of the theoretical predicted magnetic field. The code takes a few regularly-spaced azimuthal angles and computes radial field profiles for each. In the plot, green is the mean of these profiles. Orange is the mean profile plus and minus the standard deviation of the profiles. Yellow is the maximum and minimum fields, taken from these sample profiles. The purple dots are the measured magnetic field samples from ``measurements.txt,'' and the thick blue are the template curves. Horizontal units are \si{mm}. This is the interactive plot that the user actually sees when changing the sliders shown in \fig{scr:apr5:bokeh-iface}. The pink rectangles are magnets below the XY-plane, the horizontal blue is the water surface, and the horizontal black is the water cell's floor, assuming a water depth of \SI{5}{mm}; the vertical units for all these objects are \si{mm}. So, this plot is the feedback part of the tool that we used to optimize for Setup-3. \label{scr:apr5:bokeh-plot} } 
\end{figure} 

We know that our optimized magnetic field has the desired power profile because of the thick blue template lines. The templates are just square-root curves scaled up to fit the data, for the purple dots, and the predicted curve, for the green curve. There are both components in this plot, so there are two template curves in the background. These template curves are meant mainly for the optimization part of the workflow. When dragging the sliders to move magnet rings around, the user tries to make the green curve match the thick blue template behind it. The default template is $\propto r^{0.5}$ (square-root), but that can be changed by changing one or both of the text inputs that say ``Power'' and ``0.5'', seen in \fig{scr:apr5:bokeh-iface}. The ``0.5'' means to shape the template as $r^{0.5}$; ``1.5'' would have made $r^{1.5}$, and so on. As seen here, the template curve is useful for finding the optimal magnet arrangement. \\

\begin{figure}[h] 
	\begin{center} 
		\includegraphics[width=6in]{Figures/May-10/csv-text-import-w-tab.png} 
	\end{center} 
	\caption{ The ``magnet-arrangement.csv'' file shows the arrangement parameters that can be copy-pasted to FreeCAD. \label{user-optimize:scr:may10:magnet-arrangement} } 
\end{figure} 

One other important part of the user-optimize tool is the physical magnet arrangement plotted at the bottom, also seen in \fig{scr:apr5:bokeh-plot}. The code plots magnets as light-pink rectangles, the water surface as a thin, horizontal, blue line, and the floor of the water cell as the horizontal black line underneath that. These are as important as the template curves because without them, the user cannot see if they are spacing magnets apart enough, or if the magnets are close to overlapping in space. \\

The first time the Python program is downloaded and run, it will show only the overall parameters' sliders (power, water level, and magnetization). However, behind the scenes, user-optimize creates two folders: Optimized-Params and User-Optimize. Therefore, when running it for the first time, do the following: 

\begin{enumerate} 
	\item Close the browser window or tab that has the Bokeh interface. Close the Terminal or Command Prompt window that was opened earlier for launching the Python script. This is to avoid data loss problems related to overwriting saved data. 
	\item Open the ``Optimized-Params'' folder. 
	\item Double-click on the ``magnet-arrangement.csv'' file - it should open with either Microsoft Office Excel or LibreOffice Calc. 
	\item If a dialog box comes up with import options, as in \fig{user-optimize:scr:may10:magnet-arrangement}, ensure the ``Tab'' and ``Comma'' checkboxes are checked, and click OK. 
	\item There should open a spreadsheet with no rows other than the header with column labels. Under the ``Magnets in Ring'' column, insert some magnet counts, however many magnets whichever ring should have. For example, if the first ring should have 12 magnets, type ``12'' and hit Enter. Repeat for however many rings need to be added. If a ring needs to be south-side up, then enter a ``-1'' in the ring's row under the ``Orientation'' column. 
	\item Filling out the other columns (ring radius, height, etc.) is okay, but it is not required - the user-optimize script will fill in missing values with defaults. 
	\item Save the spreadsheet back to the same CSV file, and \textit{close the spreadsheet window} to avoid data overwrite problems. 
	\item Open the Python program again. 
	\item Voila! There should now be controls for changing the added rings' ``strength'' (a one-over-radius scaled value) and radius. 
\end{enumerate} 

For an existing magnet arrangement CSV file, just open it with Excel or Calc to manually view or edit the parameters, or run the Bokeh Python script to see what the predicted magnetic field looks like. After building the magnet arrangement as a physical setup and measuring the magnetic field with a gaussmeter, the measured values may be saved as a text file into the same folder as ``user-optimize.py'' in order for user-optimize to recognize the measurements and plot them on top of the predicted curve, as in \fig{scr:apr5:bokeh-plot}. The text file should be named ``measurements.txt,'' and it should have one measurement recorded per line, while each line is of the format: relative radial position (mm), tab-space, magnetic field (gauss). So, in other words, it is a tab-delimited CSV file with the radial position and magnetic field as the columns. \\

The code treats the radial positions in ``measurements.txt'' as relative positions, and it adds \SI{18}{mm} to them to obtain absolute positions. This is for easier data entry when measuring the magnetic field. The position is easier to measure from the center cylindrical part than from the center. To change this \SI{18}{mm} to some other offset, save a number (for example, ``18'') as a new file called ``meas-offs.txt''. The \texttt{user-optimize.py} will check for a file with that name, and it will use the number it reads as an offset for the radial positions. The units are \si{mm}, but do not include them in the ``meas-offs.txt'' file. \\




