\chapter{ Background \label{ch:bg} } 



\section{ Astrophysical Accretion Disks } 

It is believed that stars accumulate matter using accretion disks. Matter falling into a star will first fall into an elliptical orbit around the star, forming a disk. Then the matter that is nearest the star will come in contact with the star, and become part of the star. The process is called \textit{accretion}. Stars are so far away that the details of accretion disks cannot be resolved through telescopes directly, but there is indirect evidence for their existence \cite{LX_1997}. In this thesis, the existence of accretion disks is not questioned, but we want to find the mechanism by which angular momentum transport is enhanced in these disks; this mechanism is still not understood.  \\

\begin{figure}[h] 
	\begin{center} 
		\includegraphics[width=.7\linewidth]{Figures/accretion-disk-model.png} 
	\end{center} 
	\caption{ An accretion disk can be modeled as a thin disk whose matter orbits at an angular velocity $\Omega (r)$ \protect\cite{Shu_GD_92_VAD}. \label{fig:accretion-disk-model} } 
\end{figure} 

% $a \equiv \left( k T / m \right)^{1/2}$

Accretion disks consist of a thin gas and small solids in orbit, but, other than a small correction due to a pressure gradient, both gases and solids in an accretion disk travel in the same Keplerian velocity profile \cite{KL_GI_Rev16}. Thus, I will use a gaseous disk approximation in this discussion. \\

An accretion disk is not necessarily thin; it can be toroidal \cite{H_NS_95}. However, for the purposes of this discussion, we will assume that the disk is approximately two-dimensional and that the density of the gas is low. A model of a thin disk is shown in \fig{fig:accretion-disk-model}. This approximation holds as long as the thermal speed $u_{rms}$ of the gas in the disk is much less than its orbital speed $r \Omega$, i.e. $u_{rms} \ll r \Omega$ where $r$ is the radial cylindrical coordinate \cite{Shu_GD_92_VAD}. The thermal speed can be understood by thinking back to the discussion on kinetic theory from \chp{ch:intro}. In kinetic theory, particles in a gas are constantly flying around, and their root mean square velocity can be shown to be 
% Commas. $u_{rms}$ 

\begin{equation} 
u_{rms} = \sqrt{ \frac{3 R T}{M} } , 
\end{equation} 

\noindent where $R$ is the ideal gas constant, $T$ is the temperature of the gas, and $M$ is the molar mass of the gas (see section 5.8 in Tro \cite{ChemText}). Shu has used the condition that the thermal speed is small compared to the orbit of the disk around the star, i.e., $r \Omega \gg u_{rms}$, to ignore the thickness of the disk, because the characteristic vertical height is $H = u_{rms} / \Omega$ for the disk \cite{Shu_GD_92_VAD}. Thus, the disk is considered to be thin, and $H \ll r$ holds. \\

For most of the disk, the angular velocity of the orbiting gas decreases outwards in a Keplerian velocity profile. Near the star, however, there is a region called the \textit{boundary layer}, where the velocity decreases inwards. There, the angular momentum is transported from the disk to the star \cite{PC_BLWM_13}. Dissipation of the gas kinetic energy creates high temperatures at this boundary layer and emits high-frequency radiation \cite{Shu_GD_92_VAD}. In the analysis below, we ignore the boundary layer, but it is useful vocabulary for the reader who will go explore the literature after reading this thesis. \\

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%% TODO:  Answer the question of why this is relevant to my project. %%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 



%The possibility of efficient momentum transport by this boundary layer has also been explored briefly in the literature \cite{BRS_AM_12,CP_BL_12,PC_BLWM_13}. \\






\section{ Keplerian Velocity Profile } 

Let us now derive the Keplerian profile. An accretion disk can be modeled as a thin disk in cylindrical coordinates ($r$, $\phi$, $z$). With $z \approx 0$ (thin disk), and with the mass of the disk much less than that of the central object, centrifugal balance can be shown, in the radial force per unit mass, 

\begin{equation} 
r \Omega^2 = \frac{G M}{r^2} , 
\end{equation} 

\noindent where $r \Omega^2$ is the centripetal acceleration, and $G M / r^2$ is the gravitational acceleration of an object due to the central object of mass $M$ a distance $r$ away \cite{Shu_GD_92_VAD}. Here, $G$ is the gravitational constant. This satisfies Kepler's third law and gives the Keplerian angular velocity profile \cite{Shu_GD_92_VAD,KL_GI_Rev16}, as 

\begin{equation} 
\label{eqn:omega-k} 
\Omega_k = \sqrt{ \frac{G M}{r^3} } , 
\end{equation} 

\noindent which can also be expressed more generally as 

\begin{equation} 
\label{eqn:prop-omega-k} 
\Omega_k \propto r^{-3/2} 
\end{equation} 

\noindent for any Keplerian fluid flow. \\




%\section{ Keplerian Disk Viscosity \label{section:bg:k-vis} } 

%This section repeats some information already presented at the end of \sect{section:intro:accr-disks}, but with an equation. Keplerian flows have viscosity, but viscous transport is insufficient for the amounts of angular momentum transport required in accretion disks \cite{Shu_GD_92_VAD}. To show the effect of viscosity, \eqn{eqn:omega-k} can be rewritten as 

%\begin{equation} 
%r \frac{d \Omega}{d r} = - \frac{3}{2} \Omega , 
%\end{equation} 

%\noindent which is the shear in a Keplerian disk. From this relation, Shu shows that the time scale for viscous accretion to reach a quasi-steady state is on the order of magnitude of 

%\begin{equation} 
%t_{acc} = \frac{r^2}{\nu} 
%\end{equation} 

%\noindent where $\nu$ is the kinematic viscosity of the gas. This $t_{acc}$ is estimated to be $~ 3 \times 10^{14} \text{ } \si{yr}$, about 7 to 8 orders of magnitude longer than the expected age of such disks and around 20 thousand times the age of the universe \cite{Shu_GD_92_VAD}. This shows a need for an anomalous viscosity source if accretion disks are to be explained. The possible sources of such a viscosity are controversial. Turbulence would provide such a source of enhanced transport, but does turbulence happen for a purely Keplerian disk? There is analytical evidence \cite{KS_S_1975}, and evidence from simulations \cite{KS_GL_1999,KS_HBW_1999}. However, Keplerian flow's stability has yet to be proved. On the other hand, if a valid experiment proves Keplerian flow to be unstable, then that might explain angular momentum transport in accretion disks. The stability of a Keplerian flow will be tested in the data and analysis chapters experimentally. \\







\section{ Lorentz Force \label{section:bg:lorentz} } 

Our experimental apparatus makes use of the Lorentz force to drive a Keplerian fluid flow. For a continuous charge distribution, the Lorentz force density is 

\begin{equation} 
\label{eqn:lorentz-force-density} 
\vec{f} = \rho \vec{E} + \vec{J} \times \vec{B} , 
\end{equation} 

where $\vec{f}$ is the force density, $\rho$ is the charge density, $\vec{E}$ is the electric field, $\vec{J}$ is the (electric) current density, and $\vec{B}$ is the magnetic field. In our experiment, where we have an electrically-neutral electrolytic solution, the net charge is zero, so we may safely drop the $\rho$ term. \\

The quantity $\vec{J} \times \vec{B}$ may be calculated by noting that $\vec{J}$ is the current per unit area through a surface. If we assume that the current leaving the cylindrical electrodes of our system is uniformly distributed over the fluid layer, then this area is the cylindrical electrode's surface area that touches the fluid. If the fluid's thickness is $z$, then the current density is 

\begin{equation} 
\vec{J} = \frac{-I \hat{r}}{2 \pi r z} . 
\end{equation} 

Here, we used the negative sign in front of $I$ to denote that the electric current flows from the larger-radius electrode to the smaller-radius electrode, in the $-\hat{r}$ direction. Assuming the magnetic field is directed strictly in the $\hat{z}$ direction, the quantity $\vec{J} \times \vec{B}$, and therefore the force density as a function of $r$, becomes 

\begin{equation} 
\vec{f} (r) = \vec{J} \times \vec{B} = \frac{I B(r)}{2 \pi r z} \hat{\theta} . 
\end{equation} 

Finally, we can solve for the magnitude of the magnetic field if we know the force density needed. It will be 

\begin{equation} 
\label{eqn:b-rzif} 
B (r) = \frac{2 \pi r z}{I} f (r) . 
\end{equation} 

If the forcing is small, in a damped fluid flow, the velocity of an infinitesimally-small fluid element should be directly proportional to the force density of the net force on the element because the time-dependent terms of the Navier-Stokes equation will be negligible \cite{Purcell_1977}. We let $A_p$ be the proportionality constant, just so we can determine a numerical value later in the experiment. Then \eqn{eqn:b-rzif} becomes 

\begin{equation} 
\label{eqn:B-gen-prop} 
B (r) = \frac{2 \pi r z}{A_p I} v_{\phi} (r) = \frac{2 \pi r z}{A_p I} \left( r \Omega (r) \right) = \frac{2 \pi z}{A_p I} r^2 \Omega (r) , 
\end{equation} 

\noindent where we have used that the azimuthal velocity, $v_{\phi}$, is equal to $r \Omega$ for uniform circular motion, $r$ is the radial position, and $\Omega$ is the angular velocity. This is a very important result because it shows that the angular velocity at any point in a thin layer of conducting fluid in cylindrical symmetry can be finely controlled simply by adjusting the magnetic field strength as a function of radial position. For a Keplerian angular velocity, $\Omega_k = A_k r^{-3/2}$, where $A_k$ is some proportionality constant. With this knowledge, \eqn{eqn:B-gen-prop} becomes 

\begin{equation} 
\label{eqn:B-k-prop} 
B (r) = \frac{2 \pi z A_k}{A_p I} r^{1/2} . 
\end{equation} 

The constant $A_p$ is a physical constant for our setup, and it has units $\frac{\si{mm}^3 \  \si{rad}}{\text{A} \ \text{G} \  \si{s} }$. Its value may be found empirically. The constant $A_k$, on the other hand, is a control parameter, with units $\frac{\si{mm}^{3/2} \si{rad}}{\si{s}}$; we may adjust $A_k$ arbitrarily to control our flow's velocity scale. \\


% Segway into next chapter: 

In the next chapter, we describe how we went about designing and building a system with the magnetic field profile shown in \eqn{eqn:B-k-prop}. We will look at the experimental specifics, such as magnet size and type, and describe how we took velocimetry data on fluid in the system. \\
















