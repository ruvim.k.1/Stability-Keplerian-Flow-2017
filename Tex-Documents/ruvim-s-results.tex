\chapter{ Results and Discussion \label{ch:res} } 

This chapter presents the results from the experiments. First we show the general trends for Setup-3 in \sect{section:res:trends}. Then we show examples of low-level analysis in \sect{section:res:low-level-3} and \sect{section:res:low-level-1}. Finally, we conclude with our final thoughts in \sect{section:res:final}. \\



\section{ High-level trends from Setup-3 \label{section:res:trends} } 

In this section, we examine the trends in Setup-3. We do this by varying the electric current and seeing how that affects the parameters found in the previous section. Namely, we are interested in the numerical value of the power in the power law fit of the angular velocity samples from PIV. This is the power part of $\propto r^P$. We are also interested in the proportionality constant, $A_p$. \\

\begin{figure}[!ht] 
	\begin{center} 
		\includegraphics[width=6in]{Figures/May-15/overall-below-100/angular-velocity-percentiles.png} 
	\end{center} 
	\caption{ The angular velocity is proportional to the current. \label{fig:trend:w-percentile} } 
\end{figure} 

\begin{figure}[!ht] 
	\begin{center} 
		\includegraphics[width=6in]{Figures/May-15/overall-below-100/power-profile.png} 
	\end{center} 
	\caption{ The angular velocity's power profile jumps around a lot for small electric currents but evens out to around $-1.2$ at larger currents. \label{fig:trend:w-profile} } 
\end{figure} 

\begin{figure}[!ht] 
	\begin{center} 
		\includegraphics[width=6in]{Figures/May-15/overall-below-100/prop-const-mean.png} 
	\end{center} 
	\caption{ The constant, $A_p$, is chaotic for very small currents, but evens out starting from around \SI{20}{mA}. \label{fig:trend:ap} } 
\end{figure} 

The currents were varied between \SI{0}{A} and \SI{1}{A}, but the data for currents greater than \SI{0.5}{A} jumped around a lot due to the imaging particles centrifuging out from the system's spinning. Thus, for the numerical findings, only data from currents of \SI{0.5}{A} and less were taken into account. \\

For \SI{500}{mA} and below, the weighted-average results are 

\begin{equation} 
\Omega (r) \propto r ^{ \left( -1.157 \pm 0.009 \right) } , 
\end{equation} 

\noindent for the angular velocity, and 

\begin{equation} 
A_p = \left( 627 \pm 2 \right) \  \frac{\si{mm}^3 \ \si{rad}}{\si{A} \ \si{G} \ \si{s}} , 
\end{equation} 

\noindent for the $A_p$ constant. \\

In \fig{fig:trend:w-percentile}, it can be seen that the fastest 10\%, as well as the slowest 10\%, of the fluid has angular velocity directly proportional to the current. This is in agreement with \eqn{eqn:B-gen-prop}, as expected. \\

The power law's power, $P$, is shown in \fig{fig:trend:w-profile}. It is approximately constant, except our knowledge of it is rather uncertain for currents below \SI{25}{mA} or so. This is possibly due to PIV error at such low velocities, but it may also be because of air currents in the room. \\

The proportionality constant fit trends are shown in \fig{fig:trend:ap}. Our $A_p$ constant is also uncertain for small currents, possibly for the same reasons as the $P$, discussed above. This constant's fit value becomes clear sooner than the power $P$, however, as its uncertainty and standard deviation drops starting at around \SI{15}{mA}. Overall, because $A_p$ is mostly constant, this data agrees with our initial derivation in \eqn{eqn:B-gen-prop}. \\






\section{ A snapshot of Setup-3 \label{section:res:low-level-3} } 

This section presents a snapshot of the type of data that went into the trends shown in the previous section. The $\Omega (r)$ power law and the $A_p$ constant were found as described in \chp{ch:mtd}. Data from a trial where the electric current was $I = \SI{21}{mA}$ is shown in \fig{fig:exp-02-piv-field} and \fig{fig:exp-02-piv-flat}. The angular velocity power law was 

\begin{equation} 
\Omega (r) \propto r ^{ \left( -1.32 \pm 0.07 \right) } , 
\end{equation} 

\noindent where $\Omega (r)$ is angular velocity at radial position $r$. The magnetic field for Setup-3 was designed such that it is $\propto r^{+0.5}$, so the fact that our $\Omega (r)$ power law is approximately $0.5$ minus $2$ is in approximate agreement with \eqn{eqn:B-gen-prop}. For an estimate of how \textit{strong} the flow is compared to the other parameters ($B (r)$, $I$, etc.), the $A_p$ constant is a more reliable measure. The $A_p$ parameter's value from this Setup-3 data was found to be 

\begin{equation} 
A_p = \left( 645 \pm 15 \right) \  \frac{\si{mm}^3 \ \si{rad}}{\si{A} \ \si{G} \ \si{s}} . 
\end{equation} 

% For the plots in this section, the current of \SI{21}{mA} was used because it was one of the prettier plots. \\

\begin{figure}[!ht] 
	\begin{subfigure}[b]{0.5\textwidth} 
		\includegraphics[width=\linewidth]{Figures/April-27/Setup-3/21mA/param-map-v_t.png} 
		%\includegraphics[width=\linewidth]{Figures/April-28/forgot-to-add.png} 
		\caption{ False-color $v_\theta$ plot. \label{img:exp-02-piv-field:magn} } 
	\end{subfigure} 
	\begin{subfigure}[b]{0.5\textwidth} 
		\includegraphics[width=\linewidth]{Figures/April-27/Setup-3/21mA/uv-field.png} 
		\caption{ $(u, v)$ vector field plot. \label{img:exp-02-piv-field:vect} } 
	\end{subfigure} 
	\caption{ Setup-3. After the PIV analysis, vector fields were obtained representing particle velocities, $(u, v)$. The velocities' magnitudes, $v_\theta$, are shown in false-color on the left, and the actual $(u, v)$ vector field is shown on the right. The vectors are pointing mostly in the counter-clockwise direction. \label{fig:exp-02-piv-field} } 
\end{figure} 

\begin{figure}[!ht] 
	\begin{subfigure}[b]{0.5\textwidth} 
		\includegraphics[width=\linewidth]{Figures/May-15/Setup-3/angular-velocity--all.png} 
		\caption{ Angular velocity. \label{img:exp-02-piv-flat:angular} } 
	\end{subfigure} 
	\begin{subfigure}[b]{0.5\textwidth} 
		\includegraphics[width=\linewidth]{Figures/April-27/Setup-3/21mA/Ap-data.png} 
		\caption{ $A_p$ samples. \label{img:exp-02-piv-flat:magn} } 
	\end{subfigure} 
	\caption{ Setup-3. Left: The flattened ($r$, $\Omega (r)$) data (flattened means we turned the 2D data into a 1D array) was fit against a power law, $y = A r^P$. The fit was only done on the part of the curve that fit a power law. Right: The $A_p$ samples (see previous chapter) and their mean value as the green curve. \label{fig:exp-02-piv-flat} } 
\end{figure} 










\section{ A snapshot of Setup-1 \label{section:res:low-level-1} \label{section:res:single} } 

The data from Setup-1 was also analyzed using PIV. As discussed in the previous chapter, the velocity profile and the $A_p$ parameter were found. \fig{fig:exp-01-piv-field} shows the average velocity field obtained from the OpenPIV analysis after validation. \fig{fig:exp-01-piv-flat} shows the angular velocity profile and the $A_p$ parameter. From the $v_\theta$ fit, the power law was 

\begin{equation} 
\Omega (r) \propto r ^{ \left( -3.52 \pm 0.08 \right) } . 
\end{equation} 

\begin{figure}[!ht] 
	\begin{subfigure}[b]{0.5\textwidth} 
		\includegraphics[width=\linewidth]{Figures/April-27/Setup-1/param-map-v_t.png} 
		\caption{ False-color $v_\theta$ plot. \label{img:exp-01-piv-field:magn} } 
	\end{subfigure} 
	\begin{subfigure}[b]{0.5\textwidth} 
		\includegraphics[width=\linewidth]{Figures/April-27/Setup-1/uv-field.png} 
		\caption{ $(u, v)$ vector field plot. \label{img:exp-01-piv-field:vect} } 
	\end{subfigure} 
	\caption{ Setup-1. After the PIV analysis, vector fields were obtained representing particle velocities, $(u, v)$. The velocities' magnitudes, $v_\theta$, are shown in false-color on the left, and the actual $(u, v)$ vector field is shown on the right. The vectors are pointing mostly in the counter-clockwise direction. \label{fig:exp-01-piv-field} } 
\end{figure} 

\begin{figure}[!ht] 
	\begin{subfigure}[b]{0.5\textwidth} 
		\includegraphics[width=\linewidth]{Figures/May-15/Setup-1/angular-velocity--all.png} 
		\caption{ Angular velocity. \label{img:exp-01-piv-flat:angular} } 
	\end{subfigure} 
	\begin{subfigure}[b]{0.5\textwidth} 
		\includegraphics[width=\linewidth]{Figures/April-27/Setup-1/Ap-data.png} 
		\caption{ $A_p$ samples. \label{img:exp-01-piv-flat:magn} } 
	\end{subfigure} 
	\caption{ Setup-1. Left: The flattened ($r$, $\Omega (r)$) data was fit against a power law, $y = A r^P$. The fit was only done on the part of the curve that fit a power law. Right: The $A_p$ samples (see previous chapter) and their mean value as the green curve. \label{fig:exp-01-piv-flat} } 
\end{figure} 

The magnetic field for Setup-1 was approximately $\propto r^{-1.5}$, and the $\Omega (r)$ being $\propto r^{-3.5}$ is also in agreement with \eqn{eqn:B-gen-prop}. The $A_p$ parameter from Setup-1 data was 

\begin{equation} 
A_p = \left( 561 \pm 15 \right) \  \frac{\si{mm}^3 \ \si{rad}}{\si{A} \ \si{G} \ \si{s}} . 
\end{equation} 


% Next, the analysis in \fig{fig:exp-01-piv-flat} was performed. The array of (x, y) positions was turned into an array of $r$, radial, positions. The array of (u, v) was similarly turned into an array of $v_\theta$, the velocity magnitudes. The $v_\theta$ was divided by $r$ These two data sets were flattened into one dimension, and then a power curve was fit to it using polyfit () to make a linear fit, \fig{img:exp-01-piv-flat:angular}. The $v_\theta$ was divided by $r$ to give angular velocity, and the same analysis was done on it as well, \fig{img:exp-01-piv-flat:magn}. \\





% \noindent where $\Omega (r)$ is angular velocity at radial position $r$. The magnetic field for Setup-1 was designed such that it is $\propto r^{-1.5}$, so the fact that our $\Omega (r)$ power law is approximately $-1.5$ minus $2$ is in agreement with \eqn{eqn:B-gen-prop}. For an estimate of how \textit{strong} the flow is compared to the other parameters ($B (r)$, $I$, etc.), the $A_p$ constant is a more reliable measure. The $A_p$ parameter's value from this Setup-1 data was found to be 












\section{ Final Thoughts \label{section:res:final} } 

We built two pieces of experimental apparatus in this thesis. One had a Keplerian magnetic field, and one drove an almost-Keplerian fluid flow. No instability has been observed with this Setup-3 yet. However, the power was not quite $-1.5$ as expected for a Keplerian profile, so Keplerian flow will still need to be continued to be investigated in the future. \\

For future experiments of this sort, we have determined the $A_p$ constant to help with designing these fluid flows. The constant is 

\begin{equation} 
A_p = \left( 627 \pm 2 \right) \  \frac{\si{mm}^3 \ \si{rad}}{\si{A} \ \si{G} \ \si{s}} , 
\end{equation} 

\noindent and it can be used in the equation 

\begin{equation} 
\Omega (r) = \frac{ A_p I B (r) }{2 \pi z r^2 } 
\end{equation} 

\noindent where $I$ is the electric current, $B (r)$ is the magnetic field at radial position $r$, and $z$ is the water level in the cell. \\

One of the limitations, as alluded to in \chp{ch:intro}, was Ekman effects. In a system like this, fluid elements near the floor are slowed by shear, and this slowing makes the centrifugal force on those fluid elements weaker than the centrifugal force on the elements traveling faster above them. Thus, there was Ekman pumping in our system, where at the water surface the fluid was rotating and moving radially outward slowly. On the other hand, the fluid near the bottom rotated in the same direction but slowly moved radially inward. This was particularly noticeable in Setup-3 and for currents greater than \SI{1}{A}. Our PIV analysis was not affected very much by this because we only focused on small currents, but a future study may be to find a fix to this Ekman pumping. \\

We may also make some improvements to make the angular flow more Keplerian. As it is, the center cylinder is fixed, but if it rotated with the flow, then the flow would not drastically fall off near the center. Another idea is to investigate how $A_p$ is affected by the other parameters. The $A_p$ is derived to be a constant, but in the real world it still varies a little with radial position. Of course, if not for the boundary conditions, $A_p$ would probably not vary with radial position. \\

There were limitations to this study, but it was still informative. While we cannot make a definite conclusion about Keplerian flows, we can at least say that a flow that is close to Keplerian seems pretty stable, as we have not observed any instability in our setup yet. \\































