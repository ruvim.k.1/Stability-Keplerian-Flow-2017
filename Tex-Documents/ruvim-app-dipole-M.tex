
\chapter{ Finding Dipole Magnetization Constant \label{ch:dipole-M} } 

To simplify magnetic field prediction, we made the simplifying assumption that our magnets behave like point dipoles. With this assumption, the magnetization constant was all we needed to start predicting the magnetic fields caused by complicated magnet arrangements. This appendix describes how we used an online magnetic field calculator to find the magnetization constant. \\

Before we discuss the magnetic field, first we will note that we used N42-grade magnets that were $1/4''$ thick and $1/4''$ in diameter. The strongest grade available was N52, but N42 was the best strength for the money, making it a suitable choice for us. As far as size, smaller is better if a smooth magnetic field is desired - contribution from many discrete magnets can be smoothed better than the contribution from few strong magnets. On the other hand, we had to handle the magnets by hand, so they needed to be large enough in size to be handled. We decided that $1/4'' \times 1/4''$ will be the most practical trade-off for the experiments in this thesis. \\


\section{ Magnetization Constant from K\&J Magnetics Calculator } 

To find the dipole magnetization constant of our magnets, we first went to the K\&J Magnetics website and found a magnetic field strength calculator \cite{KJ_BFieldCalc}. We used it to probe different positions in space above the magnet and then performed a dipole fit on the collected data. This section tells how we arrived at the $M = \left( 2.20 \pm 0.05 \right) \times 10^5 \  \si{G mm^3}$ result. \\

\begin{figure}[h] 
	\begin{center} 
		\includegraphics[width=6in]{Figures/KJ_Magnetics_N42_14_14_B_Strength_with_fit3.eps} 
	\end{center} 
	\caption{ Magnetic field strength calculated for our N42 $1/4'' \times 1/4''$ magnet from the K\&J Magnetics website \protect\cite{KJ_BFieldCalc}. A magnetic dipole fit has been placed over the tail, where position is at least \SI{0.4}{in} from the center of the magnet. The position was measured vertically from the center of a magnet that is oriented with its north axis up. \label{plot:kj-n42-b-curve} } 
\end{figure} 

%\begin{figure}[h] 
%	\begin{center} 
%		\includegraphics[width=6in]{Figures/KJ_Magnetics_N42_14_14_B_Strength_with_fit3.eps} 
%	\end{center} 
%	\caption{ Magnetic field strength calculated for our N42 $1/4'' \times 1/4''$ magnet from the K\&J Magnetics website \protect\cite{KJ_BFieldCalc}. A magnetic dipole fit has been placed over the tail, where position is at least \SI{0.4}{in} from the center of the magnet. The position is in inches measured vertically from the center of a magnet that is oriented with its north axis up. \label{plot:kj-n42-b-curve} } 
%\end{figure} 

\begin{figure}[h] 
	\begin{center} 
		\includegraphics[width=6in]{Figures/May-12/Field-Strength/KJ_Magnetics_N42_14_14_B_Strength_M_fit_plot.png} 
	\end{center} 
	\caption{ The intermediate ($r$, $r^3 B (r) / 2$) form used to fit $M$, the dipole magnetization constant. \label{plot:kj-n42-b-fit-interm} } 
\end{figure} 

Since we chose to treat our magnets as dipoles, we needed to find the dipole magnetization constant of our magnets. Conveniently, the K\&J Magnetics website had a magnetic field calculator for their cylindrical magnets, from which we were able to approximate this constant. \fig{plot:kj-n42-b-curve} shows the finite element calculations, done by K\&J's magnetic-field calculator, as well as a magnetic dipole fit on the tail of the data-set. If our assumption is valid that far from the magnet the field should be approximated by a dipole, then we should be able to fit a $\propto 1/r^3$ curve on the data samples. If the curve fits the data well, it will give us an accurate estimate of the magnetic dipole moment. In the figure, the field strength is plotted as a function of vertical position above the center of the magnet. \fig{plot:kj-n42-b-fit-interm} shows how well our $M$ fits the data. The data deviates only a little from being a constant. \\

% As a note, unless otherwise stated, all our magnetic field values in this thesis are in gauss. This is because the K\&J Magnetics calculator returns results in gauss, because our gauss meter measures in gauss, and because all our magnetic fields are less than \SI{1}{T}, so it does not make sense to use Tesla as our units. Returning back to the dipole fit, 

\begin{figure}[h] 
	\begin{center} 
		\includegraphics{Figures/April-5/Magnetic-Field-KJ-Probe.png} 
	\end{center} 
	\caption{ The K\&J Magnetics finite element magnetic field calculator was used to find the predicted magnetic fields directly above the magnet, a distance $r$ away from its center. Image by \protect\cite{KJ_BFieldCalc}. \label{scr:kj-magnet} } 
\end{figure} 

The magnetic field's radial component, $B_r$, in spherical coordinates can be written as \cite{NASA_DipoleEqn} 

\begin{equation} 
\label{eqn:B_r} 
B_r = \frac{2 M \cos (\theta)}{r^3}, 
\end{equation} 

\noindent where $M$ is the magnetization constant of the dipole and $r$ is the distance from the dipole center. The data in \fig{plot:kj-n42-b-curve} was collected for varying distances probing directly over the \textit{top} of the magnet, i.e., $0 ^\circ$ from the vertical. This means the spherical coordinate $\theta = 0^\circ$, and therefore $\cos (\theta) = 1$. From \fig{scr:kj-magnet}, it can also be seen that $B_r = B_z$, where $B_z$ is the vertical component of the magnetic field in Cartesian coordinates. Plugging these into \eqn{eqn:B_r}, we can simplify the magnetic field to 

\begin{equation} 
B_z = B_r = \frac{2 M}{r^3} . 
\end{equation} 

This equation may be rewritten in terms of $M$ as 

\begin{equation} 
\label{eqn:M-from-Bz} 
M = \frac{1}{2} r^3 B_z . 
\end{equation} 

% \noindent so $M$ is easy to find. 

% For that reason, we were really interested in finding this $M$. 

Again, our goal was to be able to find parameters that we can use to arrange many magnetic dipoles, such that the parameters will give us a certain magnetic field profile. If we find $M$ for this special case where we know $B_r$, and where $B_r = B_z$, then we can plug the $M$ back into the general \eqn{eqn:B_r} or into one of the other equations from \cite{NASA_DipoleEqn} to find any component of the magnetic field for any position or orientation in space. To find $M$, we fit a $2 M /r^3$ curve over the tail of the data in \fig{plot:kj-n42-b-curve}. The tail was chosen to be $\ge \SI{0.4}{in}$ because that was where the curve started looking the most like $\propto 1/r^3$ when we plotted it. In other words, we used \eqn{eqn:M-from-Bz} to calculate an $M$ estimate for every data sample above \SI{0.4}{in}. Then the mean and standard deviation of these $M$ values were used as the best estimate, $M = \left( 2.20 \pm 0.05 \right) \times 10^5 \  \si{G mm^3}$. \\



\section{ Measuring the Constant from an Actual Magnet } 

Next, we set out to make the same measurements as in the previuos section but using a gaussmeter and a real magnet. We needed a precise method of positioning the gaussmeter probe. The department has a milling machine, but we were worried about its metal parts becoming magnetized and affecting the magnetic field. Therefore, we 3D printed plastic parts to serve as a probe positioning mechanism. \\

\begin{figure}[h] 
	\begin{center} 
		\includegraphics[width=6in]{Figures/May-12/Six-Rulers-1-Labeled.png} 
	\end{center} 
	\caption{ We designed and printed six rulers for accurately placing our gaussmeter probe at known positions above a magnet. The slots are spaced such that the slots' centers are \SI{3}{mm} apart. \label{img:rulers-for-dipole-B} } 
\end{figure} 

\begin{figure}[h] 
	\begin{center} 
		\includegraphics[width=2in]{Figures/May-12/Probing-Vertical-B.png} 
	\end{center} 
	\caption{ The slots were just a little bit too big, so we put tape around the probe and magnet in order for them to fit perfectly. \label{img:probing-dipole-B} } 
\end{figure} 

The parts we made are shown in \fig{img:rulers-for-dipole-B} and \fig{img:probing-dipole-B}. We printed six of these \textit{rulers} that had 10 gaussmeter probe slots each, spaced \SI{3}{mm} apart from each other. The first ruler's first gaussmeter probe slot was \SI{6.0}{mm} above the center of the magnet slot, and this first slot position increased by \SI{0.5}{mm} for each consecutive ruler, as the labels describe in \fig{img:rulers-for-dipole-B}. This way, we were able to measure the magnetic field at positions in increments of \SI{0.5}{mm}, starting from \SI{6.0}{mm} and ending just before \SI{36.0}{mm}. \\

\begin{figure}[h] 
	\begin{center} 
		\includegraphics[width=6in]{Figures/May-12/Field-Strength/notitle-n42-B-plot-mean.png} 
	\end{center} 
	\caption{ Magnetic field strength measured for our N42 $1/4'' \times 1/4''$ magnet using the Kleinert lab's gaussmeter. The position was measured vertically from the center of the magnet. \label{plot:meas-b-curve} } 
\end{figure} 

\begin{figure}[h] 
	\begin{center} 
		\includegraphics[width=6in]{Figures/May-12/Field-Strength/notitle-n42-M-plot2.png} 
	\end{center} 
	\caption{ The intermediate $M$ form (\eqn{eqn:M-from-Bz}) used to fit the curve in \fig{plot:meas-b-curve}. \label{plot:meas-b-interm} } 
\end{figure} 

To find $M$ from these, we used the same procedure and \eqn{eqn:M-from-Bz} as in the previous section. The results are shown in \fig{plot:meas-b-curve} and \fig{plot:meas-b-interm}, and the numerical result was $M = \left( 2.01 \pm 0.09 \right) \times 10^5 \  \si{G mm^3}$. The difference between this result and that of the previous section is approximately $\left(0.19 \pm 0.14\right) \times 10^5 \  \si{G mm^3}$. The $0.19$ is only slightly larger than the uncertainty, $0.14$, in the difference, so the discrepancy is not large between the two $M$ values. Thus, the numerically-calculated $M$ from the previous section was used in all the code for this project. \\


\section{ Using the Dipole Magnetization Constant } 

At this point, having a numerical value for our $M$, we were in a position to be able to calculate the magnetic field for any magnet arrangement using the following equations 

\begin{subequations} 
\label{appx:eqn:B_xyz} 
\begin{equation} 
B_x = 3 M x z / r^5 , 
\end{equation} 
\begin{equation} 
B_y = 3 M y z / r^5 , 
\end{equation} 
\begin{equation} 
B_z = M \left( 3 z^2 - r^2 \right) / r^5 , 
\end{equation} 
\end{subequations} 

\noindent where $x$, $y$, and $z$ are cartesian coordinates, $r$ is the distance from the origin, and the magnetic dipole is oriented up and positioned at the origin \cite{NASA_DipoleEqn}. \\


