
\chapter{ Magnet Slot Radius \label{ch:m-slot-r} } 

The magnets we purchased were advertised as having a quarter-inch diameter, but in reality they deviate a little bit. We needed to measure the magnets' actual diameter and height. On average, we measured the magnet's diameter to be \SI{6.274}{mm}, and its height to be \SI{6.31}{mm}. The radius, then, was \SI{3.14}{mm}. However, the 3D printer does not print magnet slots of the exact radius specified in the CAD model. Therefore, we also needed to find out how does the radius specified in the model relate to the radius actually printed for the magnet slot. This appendix tells how we accomplished this. \\

\begin{figure}[h] 
	\begin{center} 
		\includegraphics[width=.5\linewidth]{Figures/Photos-01/Magnet-Slot-Size-Test.png} 
	\end{center} 
	\caption{ A 3D print was made with magnet slots of varying radii, starting from \SI{3}{mm}, stopping at \SI{3.5}{mm}, and going up by $\left( 1/32 \right) \si{mm}$ with each slot. \label{img:m-slot-sz-test} } 
\end{figure} 

\begin{table} 
\caption{ The magnet slot test tray, where each slot was given one radius in the CAD model, but ended up with a slightly different radius due to 3D printing imperfections. \label{tbl:magnet-slot-radii} } 
\begin{center} 
\begin{tabular}{ c | c | c | c } 
	Increment & Model R (mm) & Printed R (mm) & Notes \\
	\hline 
	0	& 3.00	& 2.85	& \\
	1	& 3.03	& 2.90	& \\
	2	& 3.06	& 2.91	& \\
	3	& 3.09	& 2.96	& \\
	4	& 3.13	& 2.97	& \\
	5	& 3.16	& 3.06	& \\
	6	& 3.19	& 3.02	& \\
	7	& 3.22	& 3.10	& \\
	8	& 3.25	& 3.09	& \\
	9	& 3.28	& 3.13	& \\
	10	& 3.31	& 3.21	& Magnet fits \textbf{very} barely, and gets stuck. \\
	11	& 3.34	& 3.25	& Magnet fits snuggly. \\
	12	& 3.38	& 3.34	& \\
	13	& 3.41	& 3.30	& Magnet fits loosely. \\
	14	& 3.44	& 3.36	& \\
	15	& 3.47	& 3.39	& \\
	16	& 3.50	& 3.43	& \\
\end{tabular} 
\end{center} 
\end{table} 

To find the optimal radius for the slot to fit the magnet just tightly enough, the test tray in \fig{img:m-slot-sz-test} was designed and printed. The radii in the test tray started from \SI{3.0}{mm} and went up by $1/32$ \si{mm} with each slot until \SI{3.5}{mm}. As it turned out, at a slot radius of $\left(3 + 10/32\right) \si{mm}$, the magnet barely fit, and it was very difficult to take it out. This made that radius perfect for the Setup-3 slots used in this thesis. The details of the magnet slot size test may be found in \tbl{tbl:magnet-slot-radii}. \\

\begin{figure}[h] 
	\begin{center} 
		\includegraphics[width=6in]{Figures/May-12/model-vs-printed-inner-R-plot-notitle.png} 
	\end{center} 
	\caption{ Slots of different sizes for holding our quarter-inch magnets were measured, and a linear relation between the slot size as modeled in CAD and the slot size as printed was found. \label{plot:mslot-model-vs-printed} } 
\end{figure} 

We had the perfect slot size figured out, but we noticed that the relation between printed and model radius looked linear for the radius range \SI{3.0}{mm} to \SI{3.5}{mm}. Therefore, we fit a line, obtaining \fig{plot:mslot-model-vs-printed}. Numerically, the result was: 

\begin{equation} 
\label{eqn:slot-p-m} 
P = \left( -0.70 \pm 0.14 \  \si{mm} \right) + \left( 1.18 \pm 0.04 \right)  M , 
\end{equation} 

\noindent where $P$ is the printed inner radius, and $M$ is the modeled inner radius. This result is only usable for small slots roughly the size of a quarter-inch magnet; the equation yields absurd values for $P$ when used on $M$ values far outside this limit. Nevertheless, \fig{plot:mslot-model-vs-printed} and \eqn{eqn:slot-p-m} give us a correction relation that we may use to find the radius we should use in our CAD model if we want a hole roughly the size of a magnet to print accurately. \\



