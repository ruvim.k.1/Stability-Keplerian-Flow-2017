# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 23:13:23 2017

@author: Borrero Lab
"""

import os, sys; 

sys.path.append (os.path.abspath (".")); 
import ring_optimize as ro; 

# No need to import Bokeh here ... 
# Import pyplot instead. 

import matplotlib.pyplot as pp; 

import numpy; 



program = ro.SimpleSetup ("optimize-params.txt"); 

program.set_up (); 


state = program.optimize.last_result.x; 
hs = program.optimize.get_hs (state); 
rs = program.optimize.get_rs (state); 
da = program.optimize.get_da (state); 

hs += 0; # in mm 
program.optimize.last_result.x = program.optimize.make_state (hs, rs, da); 


lf = "\n"; 
mm = " mm"; 
s_rd = "Radius: "; 
s_ht = "Height: "; 
s_gl = "Angle: "; 
print (lf); 
print (s_rd + (mm + lf + s_rd).join (rs.astype (str)) + mm); 
print (lf); 
print (s_ht + (mm + lf + s_ht).join (hs.astype (str)) + mm); 
print (lf); 
print (s_gl + (lf + s_gl).join (da.astype (str))); 
print (lf); 


# Set up a data source: 

m_range = (program.cell_params.m_constrain_min, \
					program.cell_params.m_constrain_max); 
# B-field doesn't super matter anyway. 
B_max = program.params_data[program.PARAM.B_MAX]; 

write_dir = "Optimized-Plots"; 

N = 128; 
xs = ys = numpy.linspace (-program.cell_params.m_constrain_max, \
				+program.cell_params.m_constrain_max, N); 
x = xs.reshape (N, 1); 
y = ys.reshape (1, N); 

dx = dy = xs[1] - xs[0]; 

meshX = meshY = numpy.concatenate ((xs - dx / 2, [xs[-1] + dx / 2])); 
state = program.optimize.last_result.x; 

B_actual = program.optimize.b_field_calc.net_field_z (state, x, y); 
r = numpy.sqrt (x ** 2 + y ** 2); 


pp.figure (figsize = (12, 12)); 
pp.title ("Magnetic Field"); 
pp.xlabel ("Position X (mm)"); 
pp.ylabel ("Position Y (mm)"); 
pp.pcolormesh (meshX, meshY, B_actual); 
pp.savefig (write_dir + "/optimized-by-hand.png", dpi = 90); 


rs = r.ravel (); 
bs = B_actual.ravel (); 
mask = (rs > program.cell_params.r_min) * (rs < program.cell_params.r_max); 
some_rs = numpy.linspace (program.cell_params.r_min, program.cell_params.r_max, N); 
A = ro.calc_power_coef (rs[mask], bs[mask], 1/2, True); 

pp.figure (); 
pp.title ("Theoretical Dipole Magnetic Field Slice"); 
pp.xlabel ("Radial Position (mm)"); 
pp.ylabel ("Vertical Magnetic Field (gauss)"); 
pp.plot (rs, bs, 'o', markersize = 1, label = "Bz Samples"); 
pp.plot (some_rs, A * some_rs ** (1/2), label = "Power Fit"); 
pp.legend (loc = "best"); 
pp.savefig (write_dir + "/optimized-slice.png", dpi = 300); 


