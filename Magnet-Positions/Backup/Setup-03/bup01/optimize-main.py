# -*- coding: utf-8 -*-
"""
Created on Mon Feb  6 16:38:00 2017

@author: Ruvim
"""

import os, sys; 

sys.path.append (os.path.abspath (".")); 
import ring_optimize as ro; 


program = ro.SimpleSetup ("optimize-params.txt"); 

program.set_up (); 
program.run (profile = ro.profile_sqrt, optimize_what = ro.OPTIMIZE.DA, \
							method = ro.METHOD.PROFILE); 

#program.plot (); 


