README file made by Ruvim Kondratyev on 2/10/17. 


This folder has the important data and result files from my first actual attempt at 
creating an experimental system with Keplerian magnetic field. 

I ran the Python code pk-1-same-test.py (see sub-folder Python-Code-Archive in here) to 
optimize the magnet arrangement for a Keplerian profile above the magnets. 

Next, I ran pk-1b-plot.py to plot the results, which is what all the image files in 
this folder are about. 

All numbers are in millimeters (to make designing convenient, since many CAD and 3D 
software support millimeters, and since using meters is absurd; even inches are too 
big for the sizes we're dealing with). 



Here is the magnet arrangement the code came up with: 

The magnet rings are: 
Ring 1, Ring 2,	Ring 3,	Ring 4,	Ring 5,	Ring 6 

Number of magnets per ring: 
8	16	16	16	16	32

Magnet ring heights: 
24.64169547,	49.96486316,	52.30811669,	40.74574882,	52.14566875,	48.82075651 

Magnet ring radii: 
16.92352051,	61.86933232,	41.88027152,	32.83853061,	72.96859753,	102.10565008 

Magnet ring orientations (radians): 
2.95042763,	0.20495975,	0.,	0.,	0.00665448,	0. 


The rings are NOT in order of increasing radius; how SciPy optimize gave them to us, 
the ring order of increasing radius would actually be: 

Ring 1,	Ring 4,	Ring 3,	Ring 2,	Ring 5,	Ring 6 



Folder contents: 


Plots: 

2d-magnet-arrangement.png	->	shows a top-down view of the magnet arrangement 
actual-color-mesh.png		->	a pyplot pcolormesh () false-color plot of 
					    the predicted actual theoretical magnetic 
					    field due to the magnet arrangement found 
desired-color-mesh.png		->	a pyplot pcolormesh () false-color plot of 
					    the desired (Keplerian) theoretical field, 
					    which is what pk-1-same-test.py aims to 
					    optimize the magnet arrangement for 
desired-vs-field-slice.png	->	a y=0 slice of the 2D map of the magnetic field, 
					    plotted with both desired and actual predicted 

Text files: 

pk-1-magnet-counts.txt		->	the input magnet counts for each ring that is fed 
					    to the Python code to optimize (the code infers 
					    the ring count from the number of magnet counts 
					    provided) 
pk-1-same-test.txt		->	the resulting 'state' vector that scipy.optimize.minimize 
					    came up with in working with my Python code 



Where N is the number of magnet rings, the state vector contains the following: 
-> N numbers representing the magnet rings' heights below the water 
-> N numbers representing the magnet rings' radii 
-> N numbers representing the magnet rings' relative orientations, 
	defined in radians relative to the first ring 


Note: 
THE RING ORDER IS NOT NECESSARILY FROM SMALLEST RADII TO LARGEST RADII; 
this is because scipy.optimize.minimize doesn't know anything about our 
system; it only knows that it needs to tweak the state vector to make the 
cost function return the smallest value possible, which means it could 
give us all sorts of weird results, starting from out-of-order rings and 
including rings overlapping each other in space (a non-physical result). 



See the folder "Technical-Drawings" for the actual 3D models; "ring-v1.FCStd" is what I 
ended up using for creating these rings, and I exported them to files named "to-60-ringX.stl", 
where X is the ring number, and the "60" part indicates that this is the ring where I optimized 
the magnet arrangement to have 60 gauss at the inner edge of the donut-shaped water cell. 

See also the sub-folder "Technical-Drawing-Archive" for these files. 


