# -*- coding: utf-8 -*-
"""
Created on Mon Feb  6 17:56:19 2017

@author: Ruvim
"""

import os, sys; 

sys.path.append (os.path.abspath (".")); 
import ring_optimize as ro; 

import numpy; 

# Prepare the parameters for ring optimization: 
cell_params = ro.CellParameters (B_max = 60); # Water cell parameters. 
optimize_params = ro.OptimizationParameters (); 
# Set the number of magnets for each ring: 
cell_params.M_num = numpy.loadtxt ("pk-1-magnet-counts.txt", dtype = int); 

# Create a result plotter instance: 
optimize = ro.RingOptimizeContext (cell_params, optimize_params); 
plot_params = ro.PlotParameters (); 
plot_params.readCellParameters (cell_params); 
plotter = ro.RingResultPlotter (optimize, plot_params); 

# Read the result of last time's optimization: 
state = numpy.loadtxt ("pk-1-same-test.txt"); 


# Plot!: 
plotter.plot3 (state); 
plotter.plot4 (state); 

