# -*- coding: utf-8 -*-
"""
Created on Mon Feb  6 16:38:00 2017

@author: Ruvim
"""

import os, sys; 

sys.path.append (os.path.abspath (".")); 
import ring_optimize as ro; 

import numpy; 

# Prepare the parameters for ring optimization: 
cell_params = ro.CellParameters (B_max = 60); # Water cell parameters. 
optimize_params = ro.OptimizationParameters (); 
# Optionally, we can also customize these before passing them 
# to RingOptimizeContext (); 

# For example, set the number of magnets for each ring: 
cell_params.M_num = numpy.loadtxt ("pk-1-magnet-counts.txt", dtype = int); 


# Create a ring optimize context: 
optimize = ro.RingOptimizeContext (cell_params, optimize_params); 

# Optimize!: 
result = optimize.find_profile (); 
state = result.x; 

# Save the result: 
numpy.savetxt ("pk-1-same-test.txt", state); 

