# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 08:32:07 2017

@author: Borrero Lab
"""

import os, sys; 

sys.path.append (os.path.abspath (".")); 
import ring_optimize as ro; 

import numpy; 

# Prepare the parameters for ring optimization: 
cell_params = ro.CellParameters (B_max = 60); # Water cell parameters. 
optimize_params = ro.OptimizationParameters (); 
# Set the number of magnets for each ring: 
cell_params.M_num = numpy.loadtxt ("pk-1-magnet-counts.txt", dtype = int); 

# Create a result plotter instance: 
optimize = ro.RingOptimizeContext (cell_params, optimize_params); 
plot_params = ro.PlotParameters (); 
plot_params.readCellParameters (cell_params); 
plotter = ro.RingResultPlotter (optimize, plot_params); 

# Read the result of last time's optimization: 
state = numpy.loadtxt ("pk-1-same-test.txt"); 

# Read the measurements: 
meas = numpy.loadtxt ("measurements-setup-01-a.txt"); 
meas[:, 0] += 53.5 / 2; # Offset the measurement distance to turn it into a radial position. 

# Plot!: 
plotter.plot3d (state, meas, x_err = 2); 

