# Original Author:  Ruvim Kondratyev 
# Willamette University Physics, 
# Dr. Daniel Borrero-Echevery, 
# Fluid Dynamics Laboratory 

import numpy; 
import matplotlib.pyplot as pp; 
import scipy.optimize; 
import scipy.integrate; 



# Fit from KJ Magnetics N42-14-14 B Strength.py, from folder Field-Strength: 
M = 217209.80135205924; # for mm, valid from r = 0.4 in 

# Magnet grid construction parameters: 
B_max = 200; # gauss 
m_min = 30; 
m_max = 90; 
m_add = [10, 20]; 

# Measurement probe bounds: 
p_min = 30; 
p_max = 90; 



# Construct the magnet grid: 
magnet_horizontal_positions = numpy.concatenate ((m_add, 
			numpy.linspace (m_min, m_max, 3))); 
#hpos = []; 
#magnet_horizontal_positions = numpy.array (hpos); 
#magnet_horizontal_positions = numpy.array ([0]); 

def make_h0 (height = 1e1): 
	return height * numpy.ones_like (magnet_horizontal_positions); 

h0 = make_h0 (2e1); # 2 cm 



# Construct the B-field probes: 
probe_points = numpy.linspace (p_min, p_max, 81); 



# B-field measurement: 
def B_x (r, x, z): 
	return 3 * M * x * z / r ** 5; 
def B_y (r, y, z): 
	return 3 * M * y * z / r ** 5; 
def B_z (r, z): 
	return M * (3 * z ** 2 - r ** 2) / r ** 5; 

# Generation of the x, y, z, and r parameters for the dipole B-field equations: 
def get_x (): 
	return probe_points.reshape (len (probe_points), 1) - magnet_horizontal_positions.reshape (1, len (magnet_horizontal_positions)); 
def get_y (): 
	return numpy.zeros ((len (probe_points), 1)); 
def get_z (hs): 
	return hs.reshape (1, len (hs)); 
def get_r (hs): 
	return numpy.sqrt (get_x () ** 2 + get_y () ** 2 + get_z (hs) ** 2); 

# The functions that return the B-field grids in the correct array shapes (the returned arrays are "in shape"): 
def inshape_fieldX (hs): 
	return B_x (get_r (hs), get_x (), get_z (hs)); 
def inshape_fieldY (hs): 
	return B_y (get_r (hs), get_y (), get_z (hs)); 
def inshape_fieldZ (hs): 
	return B_z (get_r (hs), get_z (hs)); 

# This function returns a grid of the net X,Y,Z magnetic field: 
def net_field_xyz (hs): 
	x = get_x (); 
	y = get_y (); 
	z = get_z (hs); 
	r = numpy.sqrt (x * x + y * y + z * z); 
	netX = B_x (r, x, z).sum (axis = 1); 
	netY = B_y (r, y, z).sum (axis = 1); 
	netZ = B_z (r, z).sum (axis = 1); 
	return numpy.array ([netX, netY, netZ]); 

# These functions return just a 1D array of the magnetic field: 
def net_field_x (hs): 
	return inshape_fieldX (hs).sum (axis = 1); 
def net_field_y (hs): 
	return inshape_fieldY (hs).sum (axis = 1); 
def net_field_z (hs): 
	return inshape_fieldZ (hs).sum (axis = 1); 
def net_field_abs (hs): 
	return numpy.sqrt ((net_field_xyz (hs) ** 2).sum (axis = 0)); 




# Optimization-related functions for our Keplerian system: 

def desired_strength (r): 
	return B_max * r ** (-3/2); 

def norm (x): 
	return numpy.sqrt (numpy.sum (x * x)); 

def to_scalar (x): 
	return norm (x); 

def grid_to_minimize_z (hs): 
	return desired_strength (probe_points) - inshape_fieldZ (hs).sum (axis = 1); 

def to_minimize_inZ (hs): 
	return to_scalar (grid_to_minimize_z (hs)); 

def grid_to_minimize_abs (hs): 
	return desired_strength (probe_points) - net_field_abs (hs).sum (axis = 1); 

def to_minimize_inAbs (hs): 
	return to_scalar (grid_to_minimize_abs (hs)); 

def work (): 
	return scipy.optimize.minimize (to_minimize_inZ, h0); 





# Test plot functions: 

kj_calc_files = [ 
	"kj-n42-2525-1cm-above--b-field.txt", 
	"kj-n42-2525-2cm-above--b-field.txt", 
	"kj-n42-2525-4cm-above--b-field.txt" 
]; 
kj_calc_hts = [10, 20, 40]; 
def test_data (): 
	# The following script assumes that the input files have (in that order): 
	# 1. Have 0 or more rows of negative positions, followed by 
	# 2. 1 row of the zero position, followed by 
	# 3. 0 or more rows of positive positions. 
	for filename, ht in zip (kj_calc_files, kj_calc_hts): 
		data = numpy.loadtxt (filename); 
		n_neg = (data[:, 0] < 0).sum (); 
		n_pos = (data[:, 0] > 0).sum (); 
		n_count = max ([n_neg, n_pos]); 
		rows = numpy.zeros ((2 * n_count + 1, data.shape[1])); 
		rows[n_count] = data[n_neg]; # 0 comes after the negative numbers. 
		if n_neg > n_pos: 
			vals = data[0 : n_neg][::-1]; 
			vals[:, 1] *= -1; 
		else: 
			vals = data[n_neg + 1 :]; 
		rows[:n_count] = vals[::-1].copy (); 
		rows[:n_count, 0] *= -1; 
		rows[:n_count, 1] *= -1; 
		rows[n_count + 1 :] = vals; 
		test_plot1 (ht, rows[:, 0], rows[:, 1], rows[:, 2]); 

def test_plot1 (height, kj_x_pos, kj_bx_vals, kj_by_vals): 
	Bx = net_field_x (make_h0 (height)); 
	Bz = net_field_z (make_h0 (height)); 
	# Do Bx: 
	pp.figure (); 
	pp.title ("Theoretical B_x at z={} mm above magnet plane".format (height)); 
	pp.xlabel ("Measurement Horizontal Position (mm)"); 
	pp.ylabel ("B_x (gauss)"); 
	pp.xlim (p_min, p_max); 
	pp.plot (kj_x_pos, kj_bx_vals, label = "KJ Calculated"); 
	pp.plot (probe_points, Bx, label = "Dipole Approx."); 
	pp.legend (loc = "best"); 
	pp.savefig ("theoretical-bx-{}mm-above-plane.png".format (height)); 
	# Do By: 
	pp.figure (); 
	pp.title ("Theoretical B_z at z={} mm above magnet plane".format (height)); 
	pp.xlabel ("Measurement Horizontal Position (mm)"); 
	pp.ylabel ("B_z (gauss)"); 
	pp.xlim (p_min, p_max); 
	pp.plot (kj_x_pos, kj_by_vals, label = "KJ Calculated"); 
	pp.plot (probe_points, Bz, label = "Dipole Approx."); 
	pp.legend (loc = "best"); 
	pp.savefig ("theoretical-by-{}mm-above-plane.png".format (height)); 
	# Done! 

def test_plot2 (heights): 
	Bz = net_field_z (heights); 
	pp.figure (); 
	pp.title ("Theoretical Bz"); 
	pp.xlabel ("Measurement Horizontal Position (mm)"); 
	pp.ylabel ("B_z (gauss)"); 
	pp.xlim (p_min, p_max); 
	pp.plot (probe_points, desired_strength (probe_points), label = "Desired Profile"); 
	pp.plot (probe_points, Bz, label = "Theoretical Field"); 
	pp.legend (loc = "best"); 
	pp.savefig ("desired-vs-field.png"); 



r = work (); 
test_plot2 (r.x); 
