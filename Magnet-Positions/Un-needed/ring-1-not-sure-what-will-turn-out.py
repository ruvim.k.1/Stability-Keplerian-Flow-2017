# Original Author:  Ruvim Kondratyev 
# Willamette University Physics, 
# Dr. Daniel Borrero-Echevery, 
# Fluid Dynamics Laboratory 

import numpy; 
import matplotlib.pyplot as pp; 
import scipy.optimize; 
import scipy.integrate; 



# Fit from KJ Magnetics N42-14-14 B Strength.py, from folder Field-Strength: 
M = 217209.80135205924; # for mm, valid from r = 0.4 in 

# Magnet grid construction parameters: 
B_max = 200; # gauss 
m_height_min = 10; 
m_height_max = 200; 
m_constrain_min = 10; # From how much we can fit ... 
m_constrain_max = 150; # Up to what we can build using the 3D printer. 
m_min = 30; 
m_max = 90; 
M_num = [4, 5, 6]; # Number of magnets in each ring. 

# Measurement probe bounds: 
p_min = 30; 
p_max = 90; 



# Construct the magnet grid: 
magnet_horizontal_positions = \
			numpy.linspace (m_min, m_max, len (M_num)); 
#hpos = []; 
#magnet_horizontal_positions = numpy.array (hpos); 
#magnet_horizontal_positions = numpy.array ([0]); 
n_magnets_per_ring = numpy.array (M_num); 
n_magnets_max_per_ring = n_magnets_per_ring.max (); 
M_grid = numpy.zeros ((len (M_num), n_magnets_max_per_ring)); 
for ring in range (0, len (M_num)): 
	M_grid[ring, 0 : n_magnets_per_ring[ring]] = M; 
# Use this 'for' loop only once, and then all the rest is 
# numpy numerical processing using grids (no more loops!). 
M_rows = M_grid.shape[0]; 
M_cols = M_grid.shape[1]; 
M_use = M_grid.reshape ((1, M_rows, M_cols)); 
# Axes: (probe, ring, magnet) 

def make_h0 (height = 1e1): 
	return height * numpy.ones_like (magnet_horizontal_positions); 

h0 = make_h0 (2e1); # 2 cm 



# Construct the B-field probes: 
probe_points = numpy.linspace (p_min, p_max, 81); 



# B-field measurement: 
def B_x (M, r, x, z): 
	return 3 * M * x * z / r ** 5; 
def B_y (M, r, y, z): 
	return 3 * M * y * z / r ** 5; 
def B_z (M, r, z): 
	return M * (3 * z ** 2 - r ** 2) / r ** 5; 

# State: 
def get_hs (state): 
	return state[0: len (h0)]; 
def get_rs (state): 
	return state[len (h0) : len (h0) + len (magnet_horizontal_positions)]; 
def make_state (hs, rs): 
	state = numpy.zeros ((len (h0) + len (magnet_horizontal_positions),)); 
	state[0 : len (h0)] = hs[0 : len (h0)]; 
	state[len (h0) : len (h0) + len (magnet_horizontal_positions)] = \
		rs[0 : len (magnet_horizontal_positions)]; 
	return state; 

# Generation of the x, y, z, and r parameters for the dipole B-field equations: 
def get_x (state): 
	rs = get_rs (state); 
	return probe_points.reshape (len (probe_points), 1, 1) - rs.reshape (1, len (rs), 1); 
def get_y (state): 
	return numpy.zeros ((len (probe_points), 1, 1)); 
def get_z (state): 
	hs = get_hs (state); 
	return hs.reshape (1, len (hs), 1); 
def get_r (state): 
	return numpy.sqrt (get_x (state) ** 2 + get_y (state) ** 2 + get_z (state) ** 2); 

# The functions that return the B-field grids in the correct array shapes (the returned arrays are "in shape"): 
def inshape_fieldX (state): 
	return B_x (M_use, get_r (state), get_x (state), get_z (state)).sum (axis = 2); 
def inshape_fieldY (state): 
	return B_y (M_use, get_r (state), get_y (state), get_z (state)).sum (axis = 2); 
def inshape_fieldZ (state): 
	return B_z (M_use, get_r (state), get_z (state)).sum (axis = 2); 

# This function returns a grid of the net X,Y,Z magnetic field: 
def net_field_xyz (state): 
	x = get_x (state); 
	y = get_y (state); 
	z = get_z (state); 
	r = numpy.sqrt (x * x + y * y + z * z); 
	netX = B_x (M_use, r, x, z).sum (axis = 2).sum (axis = 1); 
	netY = B_y (M_use, r, y, z).sum (axis = 2).sum (axis = 1); 
	netZ = B_z (M_use, r, z).sum (axis = 2).sum (axis = 1); 
	return numpy.array ([netX, netY, netZ]); 

# These functions return just a 1D array of the magnetic field: 
def net_field_x (state): 
	return inshape_fieldX (state).sum (axis = 1); 
def net_field_y (state): 
	return inshape_fieldY (state).sum (axis = 1); 
def net_field_z (state): 
	return inshape_fieldZ (state).sum (axis = 1); 
def net_field_abs (state): 
	return numpy.sqrt ((net_field_xyz (state) ** 2).sum (axis = 0)); 




# Optimization-related functions for our Keplerian system: 

def desired_strength (r): 
	return B_max * r ** (-3/2); 

def norm (x): 
	return numpy.sqrt (numpy.sum (x * x)); 

def to_scalar (x): 
	return norm (x); 

def grid_to_minimize_z (state): 
	return desired_strength (probe_points) - net_field_z (state); 

def to_minimize_inZ (state): 
	return to_scalar (grid_to_minimize_z (state)); 

def grid_to_minimize_abs (state): 
	return desired_strength (probe_points) - net_field_abs (state).sum (axis = 1); 

def to_minimize_inAbs (state): 
	return to_scalar (grid_to_minimize_abs (state)); 

def work (): 
	b_hs = tuple ((m_height_min, m_height_max) for i in range (0, len (h0))); 
	b_rs = tuple ((m_constrain_min, m_constrain_max) for i in range (0, len (magnet_horizontal_positions))); 
	b = b_hs + b_rs; 
	return scipy.optimize.minimize (to_minimize_inZ, make_state (h0, magnet_horizontal_positions), \
		bounds = b); 





# Test plot functions: 

kj_calc_files = [ 
	"kj-n42-2525-1cm-above--b-field.txt", 
	"kj-n42-2525-2cm-above--b-field.txt", 
	"kj-n42-2525-4cm-above--b-field.txt" 
]; 
kj_calc_hts = [10, 20, 40]; 
def test_data (): 
	# The following script assumes that the input files have (in that order): 
	# 1. Have 0 or more rows of negative positions, followed by 
	# 2. 1 row of the zero position, followed by 
	# 3. 0 or more rows of positive positions. 
	for filename, ht in zip (kj_calc_files, kj_calc_hts): 
		data = numpy.loadtxt (filename); 
		n_neg = (data[:, 0] < 0).sum (); 
		n_pos = (data[:, 0] > 0).sum (); 
		n_count = max ([n_neg, n_pos]); 
		rows = numpy.zeros ((2 * n_count + 1, data.shape[1])); 
		rows[n_count] = data[n_neg]; # 0 comes after the negative numbers. 
		if n_neg > n_pos: 
			vals = data[0 : n_neg][::-1]; 
			vals[:, 1] *= -1; 
		else: 
			vals = data[n_neg + 1 :]; 
		rows[:n_count] = vals[::-1].copy (); 
		rows[:n_count, 0] *= -1; 
		rows[:n_count, 1] *= -1; 
		rows[n_count + 1 :] = vals; 
		test_plot1 (ht, rows[:, 0], rows[:, 1], rows[:, 2]); 

def test_plot1 (height, kj_x_pos, kj_bx_vals, kj_by_vals): 
	state = make_state (make_h0 (height), magnet_horizontal_positions); 
	Bx = net_field_x (state); 
	Bz = net_field_z (state); 
	# Do Bx: 
	pp.figure (); 
	pp.title ("Theoretical B_x at z={} mm above magnet plane".format (height)); 
	pp.xlabel ("Measurement Horizontal Position (mm)"); 
	pp.ylabel ("B_x (gauss)"); 
	pp.xlim (p_min, p_max); 
	pp.plot (kj_x_pos, kj_bx_vals, label = "KJ Calculated"); 
	pp.plot (probe_points, Bx, label = "Dipole Approx."); 
	pp.legend (loc = "best"); 
	pp.savefig ("theoretical-bx-{}mm-above-plane.png".format (height)); 
	# Do By: 
	pp.figure (); 
	pp.title ("Theoretical B_z at z={} mm above magnet plane".format (height)); 
	pp.xlabel ("Measurement Horizontal Position (mm)"); 
	pp.ylabel ("B_z (gauss)"); 
	pp.xlim (p_min, p_max); 
	pp.plot (kj_x_pos, kj_by_vals, label = "KJ Calculated"); 
	pp.plot (probe_points, Bz, label = "Dipole Approx."); 
	pp.legend (loc = "best"); 
	pp.savefig ("theoretical-by-{}mm-above-plane.png".format (height)); 
	# Done! 

def test_plot2 (state): 
	Bz = net_field_z (state); 
	pp.figure (); 
	pp.title ("Theoretical Bz"); 
	pp.xlabel ("Measurement Horizontal Position (mm)"); 
	pp.ylabel ("B_z (gauss)"); 
	pp.xlim (p_min, p_max); 
	pp.plot (probe_points, desired_strength (probe_points), label = "Desired Profile"); 
	pp.plot (probe_points, Bz, label = "Theoretical Field"); 
	pp.legend (loc = "best"); 
	pp.savefig ("desired-vs-field.png"); 



r = work (); 
test_plot2 (r.x); 
