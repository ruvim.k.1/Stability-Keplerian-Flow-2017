# Original Author:  Ruvim Kondratyev 
# Willamette University Physics, 
# Dr. Daniel Borrero-Echevery, 
# Fluid Dynamics Laboratory 

import numpy; 
import matplotlib.pyplot as pp; 
import scipy.optimize; 
import scipy.integrate; 



# Fit from KJ Magnetics N42-14-14 B Strength.py, from folder Field-Strength: 
#M = 217209.80135205924; # for mm, valid from r = 0.4 in 
M = 5000; 

# Magnet grid construction parameters: 
B_max = 200; # gauss 
m_height_min = 10; 
m_height_max = 200; 
m_constrain_min = 10; # From how much we can fit ... 
m_constrain_max = 150; # Up to what we can build using the 3D printer. 
m_min = 30; 
m_max = 200; 
M_num = [1, 3, 5]; # Number of magnets in each ring. 

# Measurement probe bounds: 
p_cnt = 35; 
p_min = 30; 
p_max = 90; 
s_cnt = 81; 
s_min = -200; 
s_max = +200; 
# p means probe for optimization, while s means for showing (plotting) 



# Construct the magnet grid: 
magnet_horizontal_positions = \
			numpy.linspace (m_min, m_max, len (M_num)); 
zero_delta_angles = numpy.zeros_like (magnet_horizontal_positions); 
#hpos = []; 
#magnet_horizontal_positions = numpy.array (hpos); 
#magnet_horizontal_positions = numpy.array ([0]); 
n_magnets_per_ring = numpy.array (M_num); 
n_magnets_max_per_ring = n_magnets_per_ring.max (); 
A_grid = numpy.zeros ((len (M_num), n_magnets_max_per_ring)); # Rotation angles. 
M_grid = numpy.zeros ((len (M_num), n_magnets_max_per_ring)); # Magnetization grid. 
for ring in range (0, len (M_num)): 
	M_grid[ring, 0 : n_magnets_per_ring[ring]] = M; 
	A_grid[ring, 0 : n_magnets_per_ring[ring]] = numpy.linspace (0, 2 * numpy.pi, n_magnets_per_ring[ring], endpoint = False); 
# Use this 'for' loop only once, and then all the rest is 
# numpy numerical processing using grids (no more loops!). 
M_rows = M_grid.shape[0]; 
M_cols = M_grid.shape[1]; 
M_use = M_grid.reshape ((1, 1, M_rows, M_cols)); 
A_use = A_grid.reshape ((1, 1, M_rows, M_cols)); 
# Axes: (probe X, probe Y, ring, magnet) 

def make_h0 (height = 1e1): 
	return height * numpy.ones_like (magnet_horizontal_positions); 

h0 = make_h0 (2e1); # 2 cm 



# Construct the B-field probes: 
probe_xs = numpy.linspace (p_min, p_max, p_cnt); 
probe_ys = numpy.linspace (p_min, p_max, p_cnt); 
#probe_points = probe_xs.reshape (len (probe_xs), 1) + \
#			probe_ys.reshape (1, len (probe_ys)); 
probe_dx = (probe_xs[1] - probe_xs[0]); 
probe_dy = (probe_ys[1] - probe_ys[0]); 
probe_mesh_coord_x = numpy.append (probe_xs - probe_dx / 2, probe_xs[-1] + probe_dx / 2); 
probe_mesh_coord_y = numpy.append (probe_ys - probe_dy / 2, probe_ys[-1] + probe_dy / 2); 

probe_lenX = len (probe_xs); 
probe_lenY = len (probe_ys); 

show_xs = numpy.linspace (s_min, s_max, s_cnt); 
show_ys = numpy.linspace (s_min, s_max, s_cnt); 
show_dx = show_xs[1] - show_xs[0]; 
show_dy = show_ys[1] - show_ys[0]; 
show_mesh_coord_x = numpy.append (show_xs - show_dx / 2, show_xs[-1] + show_dx / 2); 
show_mesh_coord_y = numpy.append (show_ys - show_dy / 2, show_ys[-1] + show_dy / 2); 



# B-field measurement: 
def B_x (M, x, y, z): 
	r = xyz_to_r (x, y, z); 
	return 3 * M * x * z / r ** 5; # B_x 
def B_y (M, x, y, z): 
	r = xyz_to_r (x, y, z); 
	return 3 * M * y * z / r ** 5; # B_y 
def B_z (M, x, y, z): 
	r = xyz_to_r (x, y, z); 
	return M * (3 * z ** 2 - r ** 2) / r ** 5; # B_z 
# Note that these functions first calculate xp and yp (x prime and 
# y prime); this is because they take into account the angle by which 
# the magnet is rotated about the Z axis (specified by A). 


# State: 
def get_hs (state): 
	return state[0: len (h0)]; 
def get_rs (state): 
	return state[len (h0) : len (h0) + len (magnet_horizontal_positions)]; 
def get_da (state): 
	base_index = len (h0) + len (magnet_horizontal_positions); 
	return state[base_index : base_index + len (zero_delta_angles)]; 
def make_state (hs, rs, dA): 
	state = numpy.zeros ((len (h0) + \
		len (magnet_horizontal_positions) +\
		len (zero_delta_angles),)); 
	state[0 : len (h0)] = hs[0 : len (h0)]; 
	state[len (h0) : len (h0) + len (magnet_horizontal_positions)] = \
		rs[0 : len (magnet_horizontal_positions)]; 
	state[len (h0) + len (magnet_horizontal_positions) : \
		len (h0) + len (magnet_horizontal_positions) + \
		len (zero_delta_angles)] = \
		dA[0 : len (zero_delta_angles)]; 
	return state; 

# Generation of the x, y, z, and r parameters for the dipole B-field equations: 
def get_x (state, A, xs, ys): 
	rs = get_rs (state); 
	dA_linear = get_da (state); 
	dA = dA_linear.reshape ((1, 1, len (dA_linear), 1)); 
	x = xs.reshape (len (xs), 1, 1, 1); 
	y = ys.reshape (1, len (ys), 1, 1); 
	cosA = numpy.cos (A + dA); 
	sinA = numpy.sin (A + dA); 
	xp = cosA * x - sinA * y; 
	return xp - rs.reshape (1, 1, len (rs), 1); 
def get_y (state, A, xs, ys): 
	dA_linear = get_da (state); 
	dA = dA_linear.reshape ((1, 1, len (dA_linear), 1)); 
	x = xs.reshape (len (xs), 1, 1, 1); 
	y = ys.reshape (1, len (ys), 1, 1); 
	cosA = numpy.cos (A + dA); 
	sinA = numpy.sin (A + dA); 
	yp = sinA * x + cosA * y; 
	return yp; 
def get_z (state, A, xs, ys): 
	hs = get_hs (state); 
	return hs.reshape (1, 1, len (hs), 1); 

def xyz_to_r (x, y, z): 
	return numpy.sqrt (x * x + y * y + z * z); 

# The functions that return the B-field grids in the correct array shapes (the returned arrays are "in shape"): 
def inshape_fieldX (state, xs, ys): 
	return B_x (M_use, get_x (state, A_use, xs, ys), get_y (state, A_use, xs, ys), get_z (state, A_use, xs, ys)); 
def inshape_fieldY (state, xs, ys): 
	return B_y (M_use, get_x (state, A_use, xs, ys), get_y (state, A_use, xs, ys), get_z (state, A_use, xs, ys)); 
def inshape_fieldZ (state, xs, ys): 
	return B_z (M_use, get_x (state, A_use, xs, ys), get_y (state, A_use, xs, ys), get_z (state, A_use, xs, ys)); 

# This function returns a grid of the net X,Y,Z magnetic field: 
def net_field_xyz (state, xs, ys): 
	x = get_x (state, A_use, xs, ys); 
	y = get_y (state, A_use, xs, ys); 
	z = get_z (state, A_use, xs, ys); 
	# Sum due to each magnet in the ring (axis 3), 
	# and due to each ring (axis 2): 
	netX = B_x (M_use, x, y, z).sum (axis = 3).sum (axis = 2); 
	netY = B_y (M_use, x, y, z).sum (axis = 3).sum (axis = 2); 
	netZ = B_z (M_use, x, y, z).sum (axis = 3).sum (axis = 2); 
	return numpy.array ([netX, netY, netZ]); 

# These functions return just a 2D grid of the magnetic field: 
def net_field_x (state, xs, ys): 
	return inshape_fieldX (state, xs, ys).sum (axis = 3).sum (axis = 2); 
def net_field_y (state, xs, ys): 
	return inshape_fieldY (state, xs, ys).sum (axis = 3).sum (axis = 2); 
def net_field_z (state, xs, ys): 
	return inshape_fieldZ (state, xs, ys).sum (axis = 3).sum (axis = 2); 
def net_field_abs (state, xs, ys): 
	return numpy.sqrt ((net_field_xyz (state, xs, ys) ** 2).sum (axis = 0)); 




# Optimization-related functions for our Keplerian system: 

def desired_strength (r): 
	result = B_max * r ** (-3/2); 
	result[r < 5] = 0; 
	return result; 

def desired_strength_2d (x, y): 
	r = numpy.sqrt (x.reshape ((len (x), 1)) ** 2 + y.reshape ((1, len (y))) ** 2); 
	return desired_strength (r); 

def norm (x): 
	return numpy.sqrt (numpy.sum (x * x)); 

def to_scalar (x): 
	return norm (x); 

def grid_to_minimize_z (state): 
	return desired_strength_2d (probe_xs, probe_ys) - net_field_z (state, probe_xs, probe_ys); 

def to_minimize_inZ (state): 
	return to_scalar (grid_to_minimize_z (state)); 

def grid_to_minimize_abs (state): 
	return desired_strength_2d (probe_xs, probe_ys) - net_field_abs (state, probe_xs, probe_ys); 

def to_minimize_inAbs (state): 
	return to_scalar (grid_to_minimize_abs (state)); 

def work (): 
	b_hs = tuple ((m_height_min, m_height_max) for i in range (0, len (h0))); 
	b_rs = tuple ((m_constrain_min, m_constrain_max) for i in range (0, len (magnet_horizontal_positions))); 
	b_as = tuple ((0, numpy.pi) for i in range (0, len (zero_delta_angles))); 
	b = b_hs + b_rs + b_as; 
	return scipy.optimize.minimize (to_minimize_inZ, make_state (h0, \
				magnet_horizontal_positions, zero_delta_angles), \
		bounds = b); 





# Test plot functions: 


def test_plot3a (state): 
	pp.figure (figsize = (4, 4)); 
	pp.title ("Desired Profile"); 
	pp.xlabel ("Measurement Position X (mm)"); 
	pp.ylabel ("Measurement Position Y (mm)"); 
	pp.xlim (s_min, s_max); 
	pp.ylim (s_min, s_max); 
	pp.pcolormesh (show_mesh_coord_x, show_mesh_coord_y, desired_strength_2d (show_xs, show_ys), label = "Desired Profile"); 
	pp.savefig ("desired-color-mesh.png"); 

def test_plot3b (state): 
	Bz = net_field_z (state, show_xs, show_ys); 
	pp.figure (figsize = (4, 4)); 
	pp.title ("Actual Theoretical"); 
	pp.xlabel ("Measurement Position X (mm)"); 
	pp.ylabel ("Measurement Position Y (mm)"); 
	pp.xlim (s_min, s_max); 
	pp.ylim (s_min, s_max); 
	pp.pcolormesh (show_mesh_coord_x, show_mesh_coord_y, Bz); 
	pp.savefig ("actual-color-mesh.png"); 

def test_plot3c (state): 
	hs = get_hs (state); 
	rs = magnet_horizontal_positions; 
	dA = get_da (state); 
	Bz = net_field_z (make_state (hs, rs, dA), show_xs, show_ys); 
	pp.figure (figsize = (4, 4)); 
	pp.title ("No Particular Configuration"); 
	pp.xlabel ("Measurement Position X (mm)"); 
	pp.ylabel ("Measurement Position Y (mm)"); 
	pp.xlim (s_min, s_max); 
	pp.ylim (s_min, s_max); 
	pp.pcolormesh (show_mesh_coord_x, show_mesh_coord_y, Bz); 
	pp.savefig ("just-playing-with-magnets.png"); 


r = work (); 
test_plot3a (r.x); 
test_plot3b (r.x); 
test_plot3c (r.x); 
