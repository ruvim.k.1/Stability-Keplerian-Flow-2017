# Original Author:  Ruvim Kondratyev 
# Willamette University Physics, 
# Dr. Daniel Borrero-Echevery, 
# Fluid Dynamics Laboratory 

import numpy; 
import matplotlib.pyplot as pp; 
import scipy.optimize; 
import scipy.integrate; 



# Fit from KJ Magnetics N42-14-14 B Strength.py, from folder Field-Strength: 
M = 217209.80135205924; # for mm, valid from r = 0.4 in 

magnet_radius = 3; # mm 
magnet_height = 6; # mm 

# Magnet grid construction parameters: 
B_max = 200; # gauss 
m_height_min = 10; 
m_height_max = 200; 
m_constrain_min = 10; # From how much we can fit ... 
m_constrain_max = 150; # Up to what we can build using the 3D printer. 
m_min = 30; 
m_max = 200; 
M_num = [4, 8, 10, 12]; # Number of magnets in each ring. 

# The minimum and maximum radii that "matter" for optimization: 
r_min = 30; 
r_max = 90; 

# Measurement probe bounds: 
p_cnt = 15; 
p_min = -r_max; 
p_max = +r_max; 
s_cnt = 81; 
s_min = -90; 
s_max = +90; 
# p means probe for optimization, while s means for showing (plotting) 



avl_colors = numpy.array (['b', 'g', 'r', 'c', 'm', 'y', 'k']); 




# Construct the magnet grid: 
magnet_horizontal_positions = \
			numpy.linspace (m_min, m_max, len (M_num)); 
zero_delta_angles = numpy.zeros_like (magnet_horizontal_positions); 
#hpos = []; 
#magnet_horizontal_positions = numpy.array (hpos); 
#magnet_horizontal_positions = numpy.array ([0]); 
n_magnets_per_ring = numpy.array (M_num); 
n_magnets_max_per_ring = n_magnets_per_ring.max (); 
I_grid = numpy.zeros ((len (M_num), n_magnets_max_per_ring)); # Just a grid of 1s and 0s for whether there is a magnet or not. 
R_grid = numpy.zeros ((len (M_num), n_magnets_max_per_ring), dtype = int); # A "row number" grid of row numbers. 
A_grid = numpy.zeros ((len (M_num), n_magnets_max_per_ring)); # Rotation angles. 
M_grid = numpy.zeros ((len (M_num), n_magnets_max_per_ring)); # Magnetization grid. 
for ring in range (0, len (M_num)): 
	I_grid[ring, 0 : n_magnets_per_ring[ring]] = 1; 
	R_grid[ring, 0 : n_magnets_per_ring[ring]] = ring; 
	M_grid[ring, 0 : n_magnets_per_ring[ring]] = M; 
	A_grid[ring, 0 : n_magnets_per_ring[ring]] = numpy.linspace (0, 2 * numpy.pi, n_magnets_per_ring[ring], endpoint = False); 
# Use this 'for' loop only once, and then all the rest is 
# numpy numerical processing using grids (no more loops!). 
M_rows = M_grid.shape[0]; 
M_cols = M_grid.shape[1]; 
I_use = I_grid.reshape ((1, 1, M_rows, M_cols)); 
R_use = R_grid.reshape ((1, 1, M_rows, M_cols)); 
M_use = M_grid.reshape ((1, 1, M_rows, M_cols)); 
A_use = A_grid.reshape ((1, 1, M_rows, M_cols)); 
# Axes: (probe X, probe Y, ring, magnet) 

def make_h0 (height = 1e1): 
	return height * numpy.ones_like (magnet_horizontal_positions); 

h0 = make_h0 (2e1); # 2 cm 



# Construct the B-field probes: 
probe_xs = numpy.linspace (p_min, p_max, p_cnt); 
probe_ys = numpy.linspace (p_min, p_max, p_cnt); 
#probe_points = probe_xs.reshape (len (probe_xs), 1) + \
#			probe_ys.reshape (1, len (probe_ys)); 
probe_dx = (probe_xs[1] - probe_xs[0]); 
probe_dy = (probe_ys[1] - probe_ys[0]); 
probe_mesh_coord_x = numpy.append (probe_xs - probe_dx / 2, probe_xs[-1] + probe_dx / 2); 
probe_mesh_coord_y = numpy.append (probe_ys - probe_dy / 2, probe_ys[-1] + probe_dy / 2); 

probe_lenX = len (probe_xs); 
probe_lenY = len (probe_ys); 

show_xs = numpy.linspace (s_min, s_max, s_cnt); 
show_ys = numpy.linspace (s_min, s_max, s_cnt); 
show_dx = show_xs[1] - show_xs[0]; 
show_dy = show_ys[1] - show_ys[0]; 
show_mesh_coord_x = numpy.append (show_xs - show_dx / 2, show_xs[-1] + show_dx / 2); 
show_mesh_coord_y = numpy.append (show_ys - show_dy / 2, show_ys[-1] + show_dy / 2); 

r_cnt = 131; 
slice_rs = numpy.linspace (r_min, r_max, r_cnt); 



# B-field measurement: 
def B_x (M, x, y, z): 
	r = xyz_to_r (x, y, z); 
	return 3 * M * x * z / r ** 5; # B_x 
def B_y (M, x, y, z): 
	r = xyz_to_r (x, y, z); 
	return 3 * M * y * z / r ** 5; # B_y 
def B_z (M, x, y, z): 
	r = xyz_to_r (x, y, z); 
	return M * (3 * z ** 2 - r ** 2) / r ** 5; # B_z 
# Note that these functions first calculate xp and yp (x prime and 
# y prime); this is because they take into account the angle by which 
# the magnet is rotated about the Z axis (specified by A). 


# State: 
def get_hs (state): 
	return state[0: len (h0)]; 
def get_rs (state): 
	return state[len (h0) : len (h0) + len (magnet_horizontal_positions)]; 
def get_da (state): 
	base_index = len (h0) + len (magnet_horizontal_positions); 
	return state[base_index : base_index + len (zero_delta_angles)]; 
def make_state (hs, rs, dA): 
	state = numpy.zeros ((len (h0) + \
		len (magnet_horizontal_positions) +\
		len (zero_delta_angles),)); 
	state[0 : len (h0)] = hs[0 : len (h0)]; 
	state[len (h0) : len (h0) + len (magnet_horizontal_positions)] = \
		rs[0 : len (magnet_horizontal_positions)]; 
	state[len (h0) + len (magnet_horizontal_positions) : \
		len (h0) + len (magnet_horizontal_positions) + \
		len (zero_delta_angles)] = \
		dA[0 : len (zero_delta_angles)]; 
	return state; 

# Generation of the x, y, z, and r parameters for the dipole B-field equations: 
def get_x (state, A, xs, ys): 
	rs = get_rs (state); 
	dA_linear = get_da (state); 
	dA = dA_linear.reshape ((1, 1, len (dA_linear), 1)); 
	angle = -(A + dA); 
	x = xs.reshape (len (xs), 1, 1, 1); 
	y = ys.reshape (1, len (ys), 1, 1); 
	cosA = numpy.cos (angle); 
	sinA = numpy.sin (angle); 
	xp = cosA * x - sinA * y; 
	return xp - rs.reshape (1, 1, len (rs), 1); 
def get_y (state, A, xs, ys): 
	dA_linear = get_da (state); 
	dA = dA_linear.reshape ((1, 1, len (dA_linear), 1)); 
	angle = -(A + dA); 
	x = xs.reshape (len (xs), 1, 1, 1); 
	y = ys.reshape (1, len (ys), 1, 1); 
	cosA = numpy.cos (angle); 
	sinA = numpy.sin (angle); 
	yp = sinA * x + cosA * y; 
	return yp; 
def get_z (state, A, xs, ys): 
	hs = get_hs (state); 
	return hs.reshape (1, 1, len (hs), 1); 

def xyz_to_r (x, y, z): 
	return numpy.sqrt (x * x + y * y + z * z); 

# The functions that return the B-field grids in the correct array shapes (the returned arrays are "in shape"): 
def inshape_fieldX (state, xs, ys): 
	return B_x (M_use, get_x (state, A_use, xs, ys), get_y (state, A_use, xs, ys), get_z (state, A_use, xs, ys)); 
def inshape_fieldY (state, xs, ys): 
	return B_y (M_use, get_x (state, A_use, xs, ys), get_y (state, A_use, xs, ys), get_z (state, A_use, xs, ys)); 
def inshape_fieldZ (state, xs, ys): 
	return B_z (M_use, get_x (state, A_use, xs, ys), get_y (state, A_use, xs, ys), get_z (state, A_use, xs, ys)); 

# This function returns a grid of the net X,Y,Z magnetic field: 
def net_field_xyz (state, xs, ys): 
	x = get_x (state, A_use, xs, ys); 
	y = get_y (state, A_use, xs, ys); 
	z = get_z (state, A_use, xs, ys); 
	# Sum due to each magnet in the ring (axis 3), 
	# and due to each ring (axis 2): 
	netX = B_x (M_use, x, y, z).sum (axis = 3).sum (axis = 2); 
	netY = B_y (M_use, x, y, z).sum (axis = 3).sum (axis = 2); 
	netZ = B_z (M_use, x, y, z).sum (axis = 3).sum (axis = 2); 
	return numpy.array ([netX, netY, netZ]); 

# These functions return just a 2D grid of the magnetic field: 
def net_field_x (state, xs, ys): 
	return inshape_fieldX (state, xs, ys).sum (axis = 3).sum (axis = 2); 
def net_field_y (state, xs, ys): 
	return inshape_fieldY (state, xs, ys).sum (axis = 3).sum (axis = 2); 
def net_field_z (state, xs, ys): 
	return inshape_fieldZ (state, xs, ys).sum (axis = 3).sum (axis = 2); 
def net_field_abs (state, xs, ys): 
	return numpy.sqrt ((net_field_xyz (state, xs, ys) ** 2).sum (axis = 0)); 




# Optimization-related functions for our Keplerian system: 

def desired_strength (r): 
	result = B_max * r ** (-3/2); 
	result[numpy.isnan (result) + numpy.isinf (result)] = 0; 
	return result; 

def desired_strength_2d (x, y): 
	return desired_strength (get_r_2d (x, y)); 

def get_r_2d (x, y): 
	return numpy.sqrt (x.reshape ((len (x), 1)) ** 2 + y.reshape ((1, len (y))) ** 2); 

def norm (x): 
	return numpy.sqrt (numpy.sum (x * x)); 

def to_scalar (x): 
	#return numpy.sum (x * x); # Least squares? 
	return norm (x); 

def grid_to_minimize_z (state): 
	grid =  desired_strength_2d (probe_xs, probe_ys) - net_field_z (state, probe_xs, probe_ys); 
	r = get_r_2d (probe_xs, probe_ys); 
	grid[r < r_min] = 0; 
	grid[r > r_max] = 0; 
	return grid; 

def to_minimize_inZ (state): 
	return to_scalar (grid_to_minimize_z (state)); 

def grid_to_minimize_abs (state): 
	grid = desired_strength_2d (probe_xs, probe_ys) - net_field_abs (state, probe_xs, probe_ys); 
	r = get_r_2d (probe_xs, probe_ys); 
	grid[r < r_min] = 0; 
	grid[r > r_max] = 0; 
	return grid; 

def to_minimize_inAbs (state): 
	return to_scalar (grid_to_minimize_abs (state)); 

def work (): 
	b_hs = tuple ((m_height_min, m_height_max) for i in range (0, len (h0))); 
	b_rs = tuple ((m_constrain_min, m_constrain_max) for i in range (0, len (magnet_horizontal_positions))); 
	b_as = tuple ((0, numpy.pi) for i in range (0, len (zero_delta_angles))); 
	b = b_hs + b_rs + b_as; 
	return scipy.optimize.minimize (to_minimize_inZ, make_state (h0, \
				magnet_horizontal_positions, zero_delta_angles), \
		bounds = b); 





# Test plot functions: 


def test_plot3a (state): 
	pp.figure (figsize = (4, 4)); 
	pp.title ("Desired Profile"); 
	pp.xlabel ("Measurement Position X (mm)"); 
	pp.ylabel ("Measurement Position Y (mm)"); 
	pp.xlim (s_min, s_max); 
	pp.ylim (s_min, s_max); 
	desired_grid = desired_strength_2d (show_xs, show_ys); 
	grid_r = get_r_2d (show_xs, show_ys); 
	desired_grid[grid_r < r_min] = 0; 
	desired_grid[grid_r > r_max] = 0; 
	pp.pcolormesh (show_mesh_coord_x, show_mesh_coord_y, desired_grid, label = "Desired Profile"); 
	pp.savefig ("desired-color-mesh.png"); 

def test_plot3b (state): 
	Bz = net_field_z (state, show_xs, show_ys); 
	pp.figure (figsize = (4, 4)); 
	pp.title ("Actual Theoretical"); 
	pp.xlabel ("Measurement Position X (mm)"); 
	pp.ylabel ("Measurement Position Y (mm)"); 
	pp.xlim (s_min, s_max); 
	pp.ylim (s_min, s_max); 
	grid_r = get_r_2d (show_xs, show_ys); 
	Bz[grid_r < r_min] = 0; 
	Bz[grid_r > r_max] = 0; 
	pp.pcolormesh (show_mesh_coord_x, show_mesh_coord_y, Bz); 
	pp.savefig ("actual-color-mesh.png"); 

def test_plot3d (state): 
	Bz = net_field_z (state, slice_rs, show_ys); 
	Bz_slice = Bz[:, show_ys == 0]; 
	pp.figure (); 
	pp.title ("Desired vs Actual Field"); 
	pp.xlabel ("Radial Position (mm)"); 
	pp.ylabel ("Field Strength (gauss)"); 
	pp.xlim (slice_rs.min (), slice_rs.max ()); 
	pp.plot (slice_rs, desired_strength_2d (slice_rs, numpy.array ([0])), label = "Desired"); 
	pp.plot (slice_rs, Bz_slice, label = "Actual"); 
	pp.legend (loc = "best"); 
	pp.savefig ("desired-vs-field-slice.png"); 

def test_plot4 (state): 
	test_plot4a (state); 
	test_plot4b (state); 

# This function draws circles where all the magnets will go: 
def test_plot4a (state): 
	rs = get_rs (state); 
	dA = get_da (state); 
	mask = M_use != 0; # mask tells us True if there is a magnet there 
	a = (A_use + dA.reshape ((1, 1, len (dA), 1)))[mask]; 
	r = (I_use * rs.reshape ((1, 1, len (rs), 1)))[mask]; 
	x = r * numpy.cos (a); 
	y = r * numpy.sin (a); 
	s = magnet_radius * 4; # Convert to pt? 
	pp.figure (figsize = (6,6)); 
	pp.title ("Magnet Arrangement"); 
	pp.xlabel ("Position X (mm)"); 
	pp.ylabel ("Position Y (mm)"); 
	pp.scatter (x, y, s, avl_colors[R_use[mask]]); 
	pp.savefig ("2d-magnet-arrangement.png"); 

def test_plot4b (state): 
	hs = get_hs (state); 
	rs = get_rs (state); 
	pp.figure (); 
	pp.title ("Magnet Ring Heights"); 
	pp.plot (rs, 2 * hs.max () - hs, 'o', label = "Ring Heights"); 
	pp.savefig ("ring-heights.png"); 


def np_str (x): 
	return numpy.char.mod ("%f", x); 



r = work (); 
print ("Results: "); 
print ("Ring Depths: {}".format (", ".join (np_str (get_hs (r.x))))); 
print ("Ring Radii: {}".format (", ".join (np_str (get_rs (r.x))))); 
print ("Ring Angle Deltas: {}".format (", ".join (np_str (get_da (r.x))))); 

test_plot3a (r.x); 
test_plot3b (r.x); 
test_plot3d (r.x); 

test_plot4 (r.x); 

