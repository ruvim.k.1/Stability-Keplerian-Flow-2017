# Original Author:  Ruvim Kondratyev 
# Willamette University Physics, 
# Dr. Daniel Borrero-Echevery, 
# Fluid Dynamics Laboratory 

import numpy; 
import matplotlib.pyplot as pp; 
import scipy.optimize; 
import scipy.integrate; 



# Fit from KJ Magnetics N42-14-14 B Strength.py, from folder Field-Strength: 
M = 217209.80135205924; # for mm, valid from r = 0.4 in 

# Magnet grid construction parameters: 
B_max = 200; # gauss 
m_min = -30; 
m_max = +30; 

# Measurement probe bounds: 
p_min = -6e1; 
p_max = 6e1; 



# Construct the magnet grid: 
magnet_horizontal_positions = numpy.linspace (m_min, m_max, 
										4); 
#magnet_horizontal_positions = numpy.array ([0]); 
h0 = numpy.ones_like (magnet_horizontal_positions) * 2e1; # 2 cm 



# Construct the B-field probes: 
probe_points = numpy.linspace (p_min, p_max, 125); 



# B-field measurement: 
def B_x (r, x, z): 
	return 3 * M * x * z / r ** 5; 
def B_y (r, y, z): 
	return 3 * M * y * z / r ** 5; 
def B_z (r, z): 
	return M * (3 * z ** 2 - r ** 2) / r ** 5; 

# Generation of the x, y, z, and r parameters for the dipole B-field equations: 
def get_x (): 
	return probe_points.reshape (len (probe_points), 1) - magnet_horizontal_positions.reshape (1, len (magnet_horizontal_positions)); 
def get_y (): 
	return numpy.zeros ((len (probe_points), 1)); 
def get_z (hs): 
	return hs.reshape (1, len (hs)); 
def get_r (hs): 
	return numpy.sqrt (get_x () ** 2 + get_y () ** 2 + get_z (hs) ** 2); 

# The functions that return the B-field grids in the correct array shapes (the returned arrays are "in shape"): 
def inshape_fieldX (hs): 
	return B_x (get_r (hs), get_x (), get_z (hs)); 
def inshape_fieldY (hs): 
	return B_y (get_r (hs), get_y (), get_z (hs)); 
def inshape_fieldZ (hs): 
	return B_z (get_r (hs), get_z (hs)); 

# This function returns a grid of the net X,Y,Z magnetic field: 
def net_field_xyz (hs): 
	x = get_x (); 
	y = get_y (); 
	z = get_z (hs); 
	r = numpy.sqrt (x * x + y * y + z * z); 
	netX = B_x (r, x, z).sum (axis = 1); 
	netY = B_y (r, y, z).sum (axis = 1); 
	netZ = B_z (r, z).sum (axis = 1); 
	return numpy.array ([netX, netY, netZ]); 

# These functions return just a 1D array of the magnetic field: 
def net_field_x (hs): 
	return inshape_fieldX (hs).sum (axis = 1); 
def net_field_y (hs): 
	return inshape_fieldY (hs).sum (axis = 1); 
def net_field_z (hs): 
	return inshape_fieldZ (hs).sum (axis = 1); 
def net_field_abs (hs): 
	return numpy.sqrt ((net_field_xyz (hs) ** 2).sum (axis = 0)); 




# Optimization-related functions for our Keplerian system: 

def desired_strength (r): 
	return B_max * r ** (-3/2); 

def norm (x): 
	return numpy.sqrt (numpy.sum (x * x)); 

def to_scalar (x): 
	return norm (x); 

def grid_to_minimize_z (hs): 
	return desired_strength (probe_points) - inshape_fieldZ (hs).sum (axis = 1); 

def to_minimize_inZ (hs): 
	return to_scalar (grid_to_minimize_z (hs)); 

def grid_to_minimize_abs (hs): 
	return desired_strength (probe_points) - net_field_abs (hs).sum (axis = 1); 

def to_minimize_inAbs (hs): 
	return to_scalar (grid_to_minimize_abs (hs)); 

def work (): 
	return scipy.optimize.minimize (to_minimize_inZ, h0); 





# Test plot functions: 

def test_plot1 (): 
	fld = net_field_z (h0); 
	pp.figure (); 
	pp.xlabel ("Measurement Position (mm)"); 
	pp.ylabel ("B_z (gauss)"); 
	pp.xlim (p_min, p_max); 
	pp.plot (probe_points, fld); 
	print ("Approximate Max. Field: {}".format (fld.max ())); 

def test_plot2 (): 
	fld = net_field_abs (h0); 
	pp.figure (); 
	pp.xlabel ("Measurement Position (mm)"); 
	pp.ylabel ("B_net (gauss)"); 
	pp.xlim (p_min, p_max); 
	pp.plot (probe_points, fld); 
	print ("Approximate Max. Field: {}".format (fld.max ())); 

