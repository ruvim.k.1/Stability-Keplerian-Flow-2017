import numpy; 
import matplotlib.pyplot as pp; 
import scipy.optimize; 
import scipy.integrate; 

# Fit from KJ Magnetics N42-14-14 B Strength.py, from folder Field-Strength: 
#M_in = 13.043844005597688; 
#M_mm = 213750.30652574563; 
#M_m = 0.00021375030652574564; 

#M = M_mm; 
M = 217209.80135205924; # for mm, valid from r = 0.4 in 
#M = 226696.71793345897; # for mm, valid from r = 0.65 in 
#M = 224343.80427109156; # for mm, valid from r = 0.6 in 
#M = 226667.10842129827; # for mm, valid from r = 0.5 in 

B_max = 200; # gauss 

p_min = -1e2; 
p_max = 1e2; 

#magnet_horizontal_positions = numpy.linspace (p_min, p_max, 
#										4); 
magnet_horizontal_positions = numpy.array ([0]); 
h0 = numpy.ones_like (magnet_horizontal_positions) * 2e1; # 2 cm 

probe_points = numpy.linspace (p_min, p_max, 125); 


def B_x (r, x, z): 
	return 3 * M * x * z / r ** 5; 
def B_y (r, y, z): 
	return 3 * M * y * z / r ** 5; 
def B_z (r, z): 
	return M * (3 * z ** 2 - r ** 2) / r ** 5; 

def desired_strength (r): 
	return B_max * r ** (-3/2); 

def get_x (): 
	return probe_points.reshape (len (probe_points), 1) - magnet_horizontal_positions.reshape (1, len (magnet_horizontal_positions)); 

def get_y (): 
	return numpy.zeros ((len (probe_points), 1)); 

def get_z (hs): 
	return hs.reshape (1, len (hs)); 

def get_r (hs): 
	return numpy.sqrt (get_x () ** 2 + get_y () ** 2 + get_z (hs) ** 2); 

def inshape_fieldX (hs): 
	return B_x (get_r (hs), get_x (), get_z (hs)); 
def inshape_fieldY (hs): 
	return B_y (get_r (hs), get_y (), get_z (hs)); 
def inshape_fieldZ (hs): 
	return B_z (get_r (hs), get_z (hs)); 
def inshape_fieldAbs (hs): 
	return numpy.sqrt (inshape_fieldX (hs) ** 2 + inshape_fieldY (hs) ** 2 + inshape_fieldZ (hs) ** 2); 

def test_plot1 (): 
	fld = inshape_fieldZ (h0).sum (axis = 1); 
	pp.plot (probe_points, fld); 
	print ("Approximate Max. Field: {}".format (fld.max ())); 
	area = scipy.integrate.simps (fld, probe_points); 
	print ("Area under B_z curve: {}".format (area)); 

def test_plot2 (): 
	fld = inshape_fieldAbs (h0).sum (axis = 1); 
	pp.plot (probe_points, fld); 
	print ("Approximate Max. Field: {}".format (fld.max ())); 

