# -*- coding: utf-8 -*-
"""
Created on Fri May 12 12:56:45 2017

@author: Borrero Lab
"""

import numpy; 
import matplotlib.pyplot as pp; 

radii = numpy.loadtxt ("model-vs-printed-inner-R.csv"); 

samples_x = radii[:, 0]; 
samples_y = radii[:, 1]; 

taylor_fit_N = radii.shape[0]; # Number of points. 
taylor_fit_delta = taylor_fit_N * (samples_x ** 2).sum () - samples_x.sum () ** 2; 
taylor_fit_A = ((samples_x ** 2).sum () * samples_y.sum () - \
				samples_x.sum () * (samples_x * samples_y).sum ()) \
			/ taylor_fit_delta; 
taylor_fit_B = (taylor_fit_N * (samples_x * samples_y).sum () - \
				samples_x.sum () * samples_y.sum ()) \
			/ taylor_fit_delta; 
taylor_sigma_y = numpy.sqrt (1 / (taylor_fit_N - 2) * ( 
				(samples_y - taylor_fit_A - taylor_fit_B * samples_x) ** 2 
			).sum ()); 
taylor_sigma_A = taylor_sigma_y * numpy.sqrt ((samples_x ** 2).sum () / taylor_fit_delta); 
taylor_sigma_B = taylor_sigma_y * numpy.sqrt (taylor_fit_N / taylor_fit_delta); 

some_xs = numpy.linspace (samples_x.min (), samples_x.max (), 2); 

pp.figure (figsize = (6, 4)); 
pp.xlabel ("Modeled Inner Radius (mm)"); 
pp.ylabel ("Printed Inner Radius (mm)"); 
pp.xlim (some_xs[0] - 0.1, some_xs[-1] + 0.1); 
pp.plot (samples_x, samples_y, 'o', label = "Samples"); 
pp.plot (some_xs, taylor_fit_A + taylor_fit_B * some_xs, label = "Linear Fit"); 
pp.legend (loc = "best"); 
pp.savefig ("model-vs-printed-inner-R-plot-notitle.png", dpi = 300); 
pp.title ("Modeled versus Printed Magnet Slot Radius"); 
pp.savefig ("model-vs-printed-inner-R-plot.png", dpi = 300); 

with open ("model-vs-printed-inner-R-fit.txt", "w") as file: 
	print ("Model versus printed inner radius: ", file = file); 
	print ("\tP = ({} ± {}) + ({} ± {}) * M".format (taylor_fit_A, taylor_sigma_A, taylor_fit_B, taylor_sigma_B), file = file); 
	print ("where P - printed, M - modeled; now, sigma_P = {}".format (taylor_sigma_y), file = file); 
	print ("", file = file); 

