# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 13:19:18 2017

@author: Borrero Lab
"""

import openpiv.tools; 

import os; 
import numpy; 
import scipy.misc; 
import scipy.optimize; 
import matplotlib.pyplot as pp; 

from PIL import Image; 

def get_dir (what = "in", amps = "0.10A"): 
	folder = "Recordings"; 
	if what == "out-piv": 
		folder = "PIV-Output"; 
	return "/Ruvim/{}/Setup-3-e/{}".format (folder, amps); 

def spyder_is_silly (): 
	return "Silly!"; 

n_frames = 1250; 

ls = ["0.000A", "0.001A", "0.002A", "0.003A", 
						"0.004A", 
						"0.005A", 
						"0.006A", 
						"0.007A", 
						"0.008A", 
						"0.009A", 
						"0.010A", 
						"0.011A", 
						"0.012A", 
						"0.013A", 
						"0.014A", 
						"0.015A", 
						"0.016A", 
						"0.017A", 
						"0.018A", 
						"0.019A", 
						"0.020A", 
						"0.021A", 
						"0.022A", 
						"0.023A", 
						"0.024A", 
						"0.025A", 
						"0.030A", 
						"0.035A", 
						"0.040A", 
						"0.045A", 
						"0.050A", 
						"0.055A", 
						"0.060A", 
						"0.065A", 
						"0.070A", 
						"0.075A", 
						"0.080A", 
						"0.085A", 
						"0.090A", 
						"0.095A", 
						"0.100A", 
						"0.150A", 
						"0.200A", 
						"0.250A", 
						"0.300A", 
						"0.350A", 
						"0.400A", 
						"0.450A", 
						"0.500A", 
						"0.550A", 
						"0.600A", 
						"0.650A", 
						"0.700A", 
						"0.750A", 
						"0.800A", 
						"0.850A", 
						"0.900A", 
						"0.950A", 
						"1.000A", 
						"1.050A" 
]; 

#l = "1.000A"; 
#l = "0.100A"; 

error_log_filename = "errors.txt"; 

for l in ls: 
	read_dir = get_dir ("in", l); 
	write_dir = get_dir ("out-piv", l); 
	
	print ("Calculating background for {} ...".format (l)); 
	
	if not os.path.exists (read_dir): 
		with open (error_log_filename, "a") as file: 
			print ("Could not find directory {}".format (read_dir), file = file); 
		continue; 
	
	read_dir = get_dir ("in", l); 
	write_dir = get_dir ("out-piv", l); 
	
	first_frame = openpiv.tools.imread (read_dir + "/frame0001.bmp"); 
	need_shape = (n_frames,) + first_frame.shape; 
	
	big_data = numpy.zeros (need_shape, dtype = first_frame.dtype); 
	
	for i in range (0, n_frames): 
		n = i + 1; 
		frame = openpiv.tools.imread (read_dir + "/frame{}.bmp".format ("%04d" % n)); 
		big_data[i] = frame; 
	
	# Calculate statistics: 
	avg = big_data.mean (axis = 0); 
	#std = big_data.std (axis = 0); 
	mn = big_data.min (axis = 0); 
	mx = big_data.max (axis = 0); 
	
	if not os.path.exists (write_dir): 
		os.makedirs (write_dir); 
	
	scipy.misc.imsave (write_dir + "/stat_mean.bmp", avg); 
	scipy.misc.imsave (write_dir + "/stat_min.bmp", mn); 
	scipy.misc.imsave (write_dir + "/stat_max.bmp", mx); 
	
	numpy.save (write_dir + "/stat_mean.bin", avg); 
	numpy.save (write_dir + "/stat_min.bin", mn); 
	numpy.save (write_dir + "/stat_max.bin", mx); 

