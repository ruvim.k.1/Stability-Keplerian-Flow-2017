# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 17:03:21 2017

@author: Borrero Lab
"""

import openpiv.tools; 
import openpiv.process; 
import openpiv.scaling; 
import openpiv.validation; 
import openpiv.filters; 

import os; 
import numpy; 
import scipy.misc; 
import scipy.optimize; 
import matplotlib.pyplot as pp; 

def get_dir (what = "in", amps = "0.10A"): 
	folder = "Recordings"; 
	if what == "out-piv": 
		folder = "PIV-Output"; 
	return "/Ruvim/{}/Setup-03/{}".format (folder, amps); 

def spyder_is_silly (): 
	return "Silly!"; 

l = "0.60A"; 
read_dir = get_dir ("in", l); 
write_dir = get_dir ("out-piv", l); 

frame_1 = openpiv.tools.imread (read_dir + "/frame0001.bmp"); 
frame_2 = openpiv.tools.imread (read_dir + "/frame0002.bmp"); 

#bg_frame = openpiv.tools.imread (write_dir + "/stat_min.bmp"); 
bg_frame = numpy.load (write_dir + "/stat_min.bin.npy"); 

frame_1 = (frame_1 - bg_frame).astype (numpy.int32); 
frame_2 = (frame_2 - bg_frame).astype (numpy.int32); 

px_per_mm = 27 / 6.6; # 108.977 / 20.05; 

mm_center = numpy.array ([504, 565]) / px_per_mm; 
mm_innerR = 200 / px_per_mm; 
mm_outerR = 400 / px_per_mm; 
mm_uncertainty = 3; # How many mm uncertain about the center. 

x = numpy.arange (frame_1.shape[0]).reshape (frame_1.shape[0], 1) / px_per_mm; 
y = numpy.arange (frame_1.shape[1]).reshape (1, frame_1.shape[1]) / px_per_mm; 
r = numpy.sqrt ((x - mm_center[0]) ** 2 + (y - mm_center[1]) ** 2); 

spyder_is_silly (); 

cut_out_mask = (r < mm_innerR) + (r > mm_outerR); 

frame_1[cut_out_mask] = 0; 
frame_2[cut_out_mask] = 0; 

scipy.misc.imsave (write_dir + "/minus-bg-0001.bmp", frame_1); 
scipy.misc.imsave (write_dir + "/minus-bg-0002.bmp", frame_2); 

vis_frame = frame_1.copy (); 

vis_frame[r < 2] = 255; 
vis_frame[(r > mm_innerR - 2) * (r < mm_innerR)] = 255; 

scipy.misc.imsave (write_dir + "/minus-bg-vis.bmp", vis_frame); 


def optimize_center (center, x, y, u, v, r_from, r_to): 
	r = numpy.sqrt ((x - center[0]) ** 2 + (y - center[1]) ** 2); 
	v_r = numpy.sqrt (u ** 2 + v ** 2); 
	v_mask = (r > r_from) * (r < r_to); 
	v_part = v_r[v_mask]; 
	r_part = r[v_mask]; 
	fit_x = 1 / numpy.log (r_part); 
	fit_y = numpy.log (v_part) * fit_x; 
	bad_mask = numpy.isnan (fit_x) * numpy.isnan (fit_y) * \
			numpy.isinf (fit_x) * numpy.isinf (fit_y); 
	good_mask = ~bad_mask; 
	if good_mask.sum () < 1: 
		print ("Warning:  good_mask has no True values; center: {}, {}" \
									.format (center[0], center[1])); 
		return 1e8; 
	fit = numpy.polyfit (fit_x[good_mask], fit_y[good_mask], 1); 
	A = numpy.exp (fit[0]); 
	B = fit[1]; 
	f_part = A * r_part ** B; 
	diff = numpy.abs (v_part - f_part).sum (); 
	print ("Diff: {}, {} -> {}".format (center[0] * px_per_mm, center[1] * px_per_mm, diff)); 
	return diff; 
# optimize_center (mm_center + numpy.array ([0, 0]) / px_per_mm, x, y, u, v, mm_innerR, mm_outerR) 

def optimize_center2 (center, x, y, u, v, r_from, r_to, rel_offset): 
	return optimize_center (center, x, y, u, v, r_from, r_to) - rel_offset; 

win_size = 10; 
search_size = 15; 
piv_overlap = 2; 

x, y = openpiv.process.get_coordinates (image_size = frame_1.shape, \
			window_size = win_size, overlap = piv_overlap); 

u, v, sig2noise = openpiv.process.extended_search_area_piv (frame_1, frame_2, \
							window_size = win_size, overlap = piv_overlap, \
							dt = 0.02, \
							search_area_size = search_size, 
							sig2noise_method = 'peak2peak'); 

px_x = x.copy (); 
px_y = y.copy (); 

x, y, u, v = openpiv.scaling.uniform (x, y, u, v, scaling_factor = px_per_mm); 

x_copy = x.copy (); 
y_copy = y.copy (); 

# Find the center: 
a = (x, y, u, v, \
								(mm_innerR + mm_outerR) / 2, \
								mm_outerR); 
a2 = a + (optimize_center (mm_center, *a),); 
b = ((mm_center[0] - mm_uncertainty, \
									mm_center[0] + mm_uncertainty), \
	(mm_center[1] - mm_uncertainty, mm_center[1] + mm_uncertainty)); 
# This line used to have the optimize center code. 

spyder_is_silly (); 

u_copy = u.copy (); 
v_copy = v.copy (); 

#u[numpy.abs (u) > 25] = numpy.nan; 
#v[numpy.abs (v) > 25] = numpy.nan; 

spyder_is_silly (); 

# For some reason the x and y are switched from the convention I thought it 
# used, so I have to account for that by this code: 
## Let's find the x and y for the center: 
#
#x -= mm_center[1]; 
#y -= mm_center[0]; 
#r = numpy.sqrt (x ** 2 + y ** 2); 

#u[r < mm_innerR] = numpy.nan; 
#u[r > mm_outerR] = numpy.nan; 

#v[r < mm_innerR] = numpy.nan; 
#v[r > mm_outerR] = numpy.nan; 

u[numpy.isnan (u)] = 0; 
v[numpy.isnan (v)] = 0; 

pp.quiver (x, y, u, v, numpy.hypot (u, v)); 
pp.savefig ("test1.png", dpi = 1200); 

val_u, val_v, mask = openpiv.validation.local_median_val (u, v, \
					u_threshold = 2, v_threshold = 2); 

val_u[numpy.isnan (val_u)] = 0; 
val_v[numpy.isnan (val_v)] = 0; 

#val_u, val_v = openpiv.filters.replace_outliers (val_u, val_v, method = 'localmean', \
#			kernel_size = 2); 

val_u, val_v, mask = openpiv.validation.sig2noise_val (val_u, val_v, sig2noise, threshold = 1.3); 

pp.quiver (x, y, val_u, val_v, numpy.hypot (val_u, val_v)); 
pp.savefig ("test2m.png", dpi = 1200); 

val_u[numpy.isnan (val_u)] = 0; 
val_v[numpy.isnan (val_v)] = 0; 

#val_u, val_v = openpiv.filters.replace_outliers (val_u, val_v, method = 'localmean', \
#			kernel_size = 2); 

pp.quiver (x, y, val_u, val_v, numpy.hypot (val_u, val_v)); 
pp.savefig ("test2q.png", dpi = 1200); 

raw_u = u.copy (); 
raw_v = v.copy (); 

u, v = val_u, val_v; 


v_r = numpy.sqrt (u ** 2 + v ** 2); 

v_r_vis = v_r.copy (); 

pp.figure (); 
pp.pcolormesh (x, y, v_r_vis); 
pp.savefig (write_dir + "/param-map-draft-vr_vis.png", dpi = 1200); 

# Let's find the x and y for the center: 
v_max = v_r.max (); 
#v_check = v_max * 0.5; 
#c_mask = v_r > v_check; 
#x_possible = numpy.array ([x[c_mask].min (), x[c_mask].max ()]); 
#y_possible = numpy.array ([y[c_mask].min (), y[c_mask].max ()]); 
#r_center = numpy.array ([x_possible.mean (), y_possible.mean ()]); 

r_center = numpy.array ([mm_center[1], mm_center[0]]); 

#need_x_min = x[(x >= x_possible[0]) * (x <= x_possible[1]) * \
#			(y >= y_possible[0]) * (y <= r_center[1]) * (v_r == 0)].min (); 
#need_x_max = x[(x >= x_possible[0]) * (x <= x_possible[1]) * \
#			(y >= r_center[1]) * (y <= y_possible[1]) * (v_r == 0)].max (); 
#need_y_min = y[(y >= y_possible[0]) * (y <= y_possible[1]) * \
#			(x >= x_possible[0]) * (x <= r_center[0]) * (v_r == 0)].min (); 
#need_y_max = y[(y >= y_possible[0]) * (y <= y_possible[1]) * \
#			(x >= r_center[0]) * (y <= x_possible[1]) * (v_r == 0)].max (); 
#r_center = numpy.array ([(need_x_min + need_x_max) / 2, \
#					(need_y_min + need_y_max) / 2]); 
x -= r_center[0]; 
y -= r_center[1]; 
r = numpy.sqrt (x ** 2 + y ** 2); 


v_w = v_r / r; # angular velocity 









rav_r = r.ravel (); 
rav_v = v_r.ravel (); 
rav_d = v_w.ravel () * 180 / numpy.pi; 

r_tail = 65; 
r_stop = 90; 

fit_mask = (rav_v > 0); # * (rav_r > mm_innerR) * (rav_r < mm_outerR); 
fit_input_additional_mask = (rav_r[fit_mask] >= r_tail) * \
						(rav_r[fit_mask] <= r_stop); 

fit_ds = rav_d[fit_mask]; 
fit_rs = rav_r[fit_mask]; 

fit_used_rs = fit_rs[fit_input_additional_mask]; 



fit_x = 1 / numpy.log (fit_rs); 
fit_y = numpy.log (fit_ds) * fit_x; 

fit_used_x = fit_x[fit_input_additional_mask]; 

fit = numpy.polyfit (fit_used_x, \
				fit_y[fit_input_additional_mask], 1); 

A_d = numpy.exp (fit[0]); 
B_d = fit[1]; 



fit_vs = rav_v[fit_mask]; 
fit_y = numpy.log (fit_vs) * fit_x; 

fit = numpy.polyfit (fit_used_x, \
				fit_y[fit_input_additional_mask], 1); 
A_v = numpy.exp (fit[0]); 
B_v = fit[1]; 



some_rs = numpy.linspace (max (fit_rs.min (), 1), fit_rs.max (), 128); 
some_x = 1 / numpy.log (some_rs); 






if not os.path.exists (write_dir): 
	os.makedirs (write_dir); 
openpiv.tools.save (x, y, u, v, mask, write_dir + "/exp1_001.txt"); 




pp.figure (); 
pp.title ("Angular Velocity Fit Intermediate"); 
pp.xlabel ("1/ln(r)"); 
pp.ylabel ("ln($\omega$)/ln(r)"); 
#pp.errorbar (r_m, d_m, d_s, r_s, fmt = 'o', label = "Data"); 
pp.plot (fit_x, numpy.log (fit_ds) * fit_x, 'o', label = "Samples"); 
pp.plot (fit_used_x, numpy.log (A_d) * fit_used_x + B_d, linewidth = 2, label = "Linear Fit"); 
pp.legend (loc = "best"); 
pp.savefig (write_dir + "/interm-angular-velocity.png", dpi = 1200); 


pp.figure (); 
pp.title ("|Velocity| Fit Intermediate"); 
pp.xlabel ("1/ln(r)"); 
pp.ylabel ("ln($v$)/ln(r)"); 
#pp.errorbar (r_m, v_m, v_s, r_s, fmt = 'o', label = "Data"); 
pp.plot (fit_x, numpy.log (fit_vs) * fit_x, 'o', label = "Samples"); 
pp.plot (fit_used_x, numpy.log (A_d) * fit_used_x + B_d, linewidth = 2, label = "Linear Fit"); 
pp.legend (loc = "best"); 
pp.savefig (write_dir + "/interm-magnitude-velocity.png", dpi = 1200); 




pp.figure (); 
pp.title ("Angular Velocity Distribution"); 
pp.xlabel ("Radial Position (mm)"); 
pp.ylabel ("Angular Velocity (deg/s)"); 
#pp.errorbar (r_m, d_m, d_s, r_s, fmt = 'o', label = "Data"); 
pp.plot (fit_rs, fit_ds, 'o', markersize = 1, label = "Samples")
pp.plot (fit_used_rs, A_d * fit_used_rs ** B_d, linewidth = 2, label = "Exp. Fit"); 
pp.legend (loc = "best"); 
pp.savefig (write_dir + "/angular-velocity.png", dpi = 1200); 


pp.figure (); 
pp.title ("|Velocity| Distribution"); 
pp.xlabel ("Radial Position (mm)"); 
pp.ylabel ("|Velocity| (mm/s)"); 
#pp.errorbar (r_m, v_m, v_s, r_s, fmt = 'o', label = "Data"); 
pp.plot (fit_rs, fit_vs, 'o', markersize = 1, label = "Samples"); 
pp.plot (fit_used_rs, A_v * fit_used_rs ** B_v, linewidth = 2, label = "Exp. Fit"); 
pp.legend (loc = "best"); 
pp.savefig (write_dir + "/magnitude-velocity.png", dpi = 1200); 






pp.figure (); 
pp.title ("Angular Velocity Histogram"); 
pp.xlabel ("Angular Velocity (deg/s)"); 
pp.ylabel ("Likelihood"); 
#pp.errorbar (r_m, v_m, v_s, r_s, fmt = 'o', label = "Data"); 
pp.hist (fit_ds, bins = 50); 
#pp.legend (loc = "best"); 
pp.savefig (write_dir + "/hist-angular-velocity.png", dpi = 1200); 

pp.figure (); 
pp.title ("Velocity Histogram"); 
pp.xlabel ("|Velocity| (mm/s)"); 
pp.ylabel ("Likelihood"); 
#pp.errorbar (r_m, v_m, v_s, r_s, fmt = 'o', label = "Data"); 
pp.hist (fit_vs, bins = 50); 
#pp.legend (loc = "best"); 
pp.savefig (write_dir + "/hist-magnitude-velocity.png", dpi = 1200); 



pp.figure (); 
pp.pcolormesh (x, y, r); 
pp.savefig (write_dir + "/param-map-r.png", dpi = 1200); 

pp.figure (); 
pp.pcolormesh (x, y, v_r); 
pp.savefig (write_dir + "/param-map-vr.png", dpi = 1200); 



v_r_vis = v_r.copy (); 
v_r_vis[r < mm_innerR / 2] = r[r < mm_innerR / 2]; 

pp.figure (); 
pp.pcolormesh (x, y, v_r_vis); 
pp.savefig (write_dir + "/param-map-vr_vis.png", dpi = 1200); 




colors = numpy.hypot (u, v); 

pp.figure (); 
pp.quiver (x, y, u, v, colors); 
pp.savefig (write_dir + "/exp1_001.png", dpi = 1200); 

pp.figure (); 
pp.quiver (x, y, u / v_r, v / v_r, colors); 
pp.savefig (write_dir + "/exp1_001unit.png", dpi = 1200); 




with open (write_dir + "/velocity-profile-fit.txt", "w") as file: 
	print ("Velocity Profile Fit Results: ", file = file); 
	print ("\tAngular Velocity:  w = {} * r^({}) [deg/s]".format (A_d, B_d), file = file); 
	print ("\tMagnitude Velocity:  v = {} * r^({}) [mm/s]".format (A_v, B_v), file = file); 




