# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 13:19:18 2017

@author: Borrero Lab
"""

import openpiv.tools; 

import os; 
import numpy; 
import scipy.misc; 
import scipy.optimize; 
import matplotlib.pyplot as pp; 

from PIL import Image; 

def get_dir (what = "in", letter = "a"): 
	folder = "CameraTests"; 
	if what == "out-piv": 
		folder = "PIV-Output"; 
	return "/Ruvim/{}/Keplerian-setup01-2-21-{}".format (folder, letter); 

def spyder_is_silly (): 
	return "Silly!"; 

n_frames = 1250; 

l = "e"; 
read_dir = get_dir ("in", l); 
write_dir = get_dir ("out-piv", l); 

first_frame = openpiv.tools.imread (read_dir + "/frame0001.bmp"); 
need_shape = (n_frames,) + first_frame.shape; 

big_data = numpy.zeros (need_shape, dtype = first_frame.dtype); 

for i in range (0, n_frames): 
	n = i + 1; 
	frame = openpiv.tools.imread (read_dir + "/frame{}.bmp".format ("%04d" % n)); 
	big_data[i] = frame; 

# Calculate statistics: 
avg = big_data.mean (axis = 0); 
#std = big_data.std (axis = 0); 
mn = big_data.min (axis = 0); 
mx = big_data.max (axis = 0); 

scipy.misc.imsave (write_dir + "/stat_mean.bmp", avg); 
scipy.misc.imsave (write_dir + "/stat_min.bmp", mn); 
scipy.misc.imsave (write_dir + "/stat_max.bmp", mx); 

