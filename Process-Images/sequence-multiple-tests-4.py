# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 17:03:21 2017

@author: Borrero Lab
"""

import openpiv.tools; 
import openpiv.process; 
import openpiv.scaling; 
import openpiv.validation; 
import openpiv.filters; 

import os; 
import numpy; 
import scipy.misc; 
import scipy.optimize; 
import matplotlib.pyplot as pp; 

def get_dir (what = "in", amps = "0.10A"): 
	folder = "Recordings"; 
	if what == "out-piv": 
		folder = "PIV-Output"; 
	return "/Ruvim/{}/Setup-3-e/{}".format (folder, amps); 

def spyder_is_silly (): 
	return "Silly!"; 

#ls = ["0.005A"]; 
#ls = ["0.005A", "0.010A", "0.015A", "0.020A", "0.025A", "0.030A", "0.035A", \
#						"0.040A", "0.045A", "0.050A", "0.055A", \
#						"0.060A", "0.065A", "0.070A", "0.075A", \
#						"0.080A", "0.085A", "0.090A", "0.095A", \
#						"0.100A"]; 
ls = ["0.000A", "0.001A", "0.002A", "0.003A", 
						"0.004A", 
						"0.005A", 
						"0.006A", 
						"0.007A", 
						"0.008A", 
						"0.009A", 
						"0.010A", 
						"0.011A", 
						"0.012A", 
						"0.013A", 
						"0.014A", 
						"0.015A", 
						"0.016A", 
						"0.017A", 
						"0.018A", 
						"0.019A", 
						"0.020A", 
						"0.021A", 
						"0.022A", 
						"0.023A", 
						"0.024A", 
						"0.025A", 
						"0.030A", 
						"0.035A", 
						"0.040A", 
						"0.045A", 
						"0.050A", 
						"0.055A", 
						"0.060A", 
						"0.065A", 
						"0.070A", 
						"0.075A", 
						"0.080A", 
						"0.085A", 
						"0.090A", 
						"0.095A", 
						"0.100A", 
						"0.150A", 
						"0.200A", 
						"0.250A", 
						"0.300A", 
						"0.350A", 
						"0.400A", 
						"0.450A", 
						"0.500A", 
						"0.550A", 
						"0.600A", 
						"0.650A", 
						"0.700A", 
						"0.750A", 
						"0.800A", 
						"0.850A", 
						"0.900A", 
						"0.950A", 
						"1.000A", 
						"1.050A" 
]; 

error_log_filename = "errors.txt"; 

analyze_frames = 1250; #1000; 
process_every = 50; 

for l in ls: 
	read_dir = get_dir ("in", l); 
	write_dir = get_dir ("out-piv", l); 
	
	if not os.path.exists (read_dir): 
		with open (error_log_filename, "a") as file: 
			print ("Could not find directory {}".format (read_dir), file = file); 
		continue; 
	
	if l == "0.010A": 
		process_every = 5; 
	elif l == "0.100A": 
		process_every = 1; 
	
	n2 = process_every + 1; 
	frame_1 = openpiv.tools.imread (read_dir + "/frame0001.bmp"); 
	frame_2 = openpiv.tools.imread (read_dir + "/frame{}.bmp".format ("%04d" % n2)); 
	
	#bg_frame = openpiv.tools.imread (write_dir + "/stat_min.bmp"); 
	bg_frame = numpy.load (write_dir + "/stat_min.bin.npy"); 
	
	frame_1 = (frame_1 - bg_frame).astype (numpy.int32); 
	frame_2 = (frame_2 - bg_frame).astype (numpy.int32); 
	
	#px_per_mm = 87 / 18; # Setup-3-b 
	px_per_mm = 202 / 36; # Setup-3-c 
	
	#mm_center = numpy.array ([514, 636]) / px_per_mm; # Setup-3-b 
	mm_center = numpy.array ([495, 635]) / px_per_mm; # Setup-3-c 
	mm_innerR = 210 / px_per_mm; 
	mm_outerR = 450 / px_per_mm; 
	mm_uncertainty = 3; # How many mm uncertain about the center. 
	
	x = numpy.arange (frame_1.shape[0]).reshape (frame_1.shape[0], 1) / px_per_mm; 
	y = numpy.arange (frame_1.shape[1]).reshape (1, frame_1.shape[1]) / px_per_mm; 
	r = numpy.sqrt ((x - mm_center[0]) ** 2 + (y - mm_center[1]) ** 2); 
	
	spyder_is_silly (); 
	
	cut_out_mask = (r < mm_innerR) + (r > mm_outerR); 
	
	frame_1[cut_out_mask] = 0; 
	frame_2[cut_out_mask] = 0; 
	
	scipy.misc.imsave (write_dir + "/minus-bg-0001.bmp", frame_1); 
	scipy.misc.imsave (write_dir + "/minus-bg-0002.bmp", frame_2); 
	
	vis_frame = frame_1.copy (); 
	
	vis_frame[r < 2] = 255; 
	vis_frame[(r > mm_innerR - 2) * (r < mm_innerR)] = 255; 
	
	scipy.misc.imsave (write_dir + "/minus-bg-vis.bmp", vis_frame); 
	
	
	def optimize_center (center, x, y, u, v, r_from, r_to): 
		r = numpy.sqrt ((x - center[0]) ** 2 + (y - center[1]) ** 2); 
		v_r = numpy.sqrt (u ** 2 + v ** 2); 
		v_mask = (r > r_from) * (r < r_to); 
		v_part = v_r[v_mask]; 
		r_part = r[v_mask]; 
		fit_x = 1 / numpy.log (r_part); 
		fit_y = numpy.log (v_part) * fit_x; 
		bad_mask = numpy.isnan (fit_x) * numpy.isnan (fit_y) * \
				numpy.isinf (fit_x) * numpy.isinf (fit_y); 
		good_mask = ~bad_mask; 
		if good_mask.sum () < 1: 
			print ("Warning:  good_mask has no True values; center: {}, {}" \
										.format (center[0], center[1])); 
			return 1e8; 
		fit = numpy.polyfit (fit_x[good_mask], fit_y[good_mask], 1); 
		A = numpy.exp (fit[0]); 
		B = fit[1]; 
		f_part = A * r_part ** B; 
		diff = numpy.abs (v_part - f_part).sum (); 
		print ("Diff: {}, {} -> {}".format (center[0] * px_per_mm, center[1] * px_per_mm, diff)); 
		return diff; 
	# optimize_center (mm_center + numpy.array ([0, 0]) / px_per_mm, x, y, u, v, mm_innerR, mm_outerR) 
	
	def optimize_center2 (center, x, y, u, v, r_from, r_to, rel_offset): 
		return optimize_center (center, x, y, u, v, r_from, r_to) - rel_offset; 
	
	win_size = 20; 
	search_size = 30; 
	piv_overlap = 10; 
	
	x, y = openpiv.process.get_coordinates (image_size = frame_1.shape, \
				window_size = win_size, overlap = piv_overlap); 
	
	x0, y0 = x.copy (), y.copy (); 
	
	use_count = numpy.zeros_like (x, dtype = int); 
	
	u_tot = numpy.zeros_like (x); 
	v_tot = numpy.zeros_like (y); 
	
	for i in range (1, 1 + int (analyze_frames / process_every)): 
		m = i; 
		n = i * process_every + 1; 
		
		if i > 1: 
			if not os.path.exists (read_dir + "/frame{}.bmp".format ("%04d" % n)): 
				continue; 
			frame_1 = frame_2; 
			frame_2 = openpiv.tools.imread (read_dir + "/frame{}.bmp".format ("%04d" % n)); 
			frame_2 = (frame_2 - bg_frame).astype (numpy.int32); 
			frame_2[cut_out_mask] = 0; 
		
		print ("Processing: {}; Frame {} of {} ... ".format (l, (i + 1), 1 + int (analyze_frames / process_every))); 
		
		u, v, sig2noise = openpiv.process.extended_search_area_piv (frame_1, frame_2, \
								window_size = win_size, overlap = piv_overlap, \
								dt = 0.02, \
								search_area_size = search_size, 
								sig2noise_method = 'peak2peak'); 
		
		px_x = x.copy (); 
		px_y = y.copy (); 
		
		if i > 1: 
			x1, y1, u, v = openpiv.scaling.uniform (x0, y0, u, v, scaling_factor = px_per_mm); 
		else: 
			x, y, u, v = openpiv.scaling.uniform (x, y, u, v, scaling_factor = px_per_mm); 
		
		x_copy = x.copy (); 
		y_copy = y.copy (); 
		
		# Find the center: 
	#	a = (x, y, u, v, \
	#								(mm_innerR + mm_outerR) / 2, \
	#								mm_outerR); 
	#	a2 = a + (optimize_center (mm_center, *a),); 
	#	b = ((mm_center[0] - mm_uncertainty, \
	#										mm_center[0] + mm_uncertainty), \
	#		(mm_center[1] - mm_uncertainty, mm_center[1] + mm_uncertainty)); 
		# This line used to have the optimize center code. 
		
		spyder_is_silly (); 
		
		u_copy = u.copy (); 
		v_copy = v.copy (); 
		
		spyder_is_silly (); 
		
		val_u, val_v = u, v; 
		
		val_u, val_v, mask = openpiv.validation.local_median_val (val_u, val_v, \
						u_threshold = 2, v_threshold = 2); 
		
		use_mask = (-numpy.isnan (val_u)) * (-numpy.isnan (val_v)); 
		use_count[use_mask] += 1; 
		u_tot[use_mask] += val_u[use_mask]; 
		v_tot[use_mask] += val_v[use_mask]; 
	
	# Now take the average or median: 
	divisor = use_count.copy (); 
	divisor[divisor == 0] = 1; # Don't divide by 0. 
	avg_u, avg_v = u_tot / divisor, v_tot / divisor; 
	val_u, val_v = avg_u.copy (), avg_v.copy (); 
	
	numpy.save (write_dir + "/out-x", x); 
	numpy.save (write_dir + "/out-y", y); 
	numpy.save (write_dir + "/out-avg-u", avg_u / process_every); 
	numpy.save (write_dir + "/out-avg-v", avg_v / process_every); 
	numpy.save (write_dir + "/out-used-counts", use_count); 
	numpy.save (write_dir + "/out-center-xy", numpy.concatenate ((mm_center, numpy.array ([px_per_mm])))); 




