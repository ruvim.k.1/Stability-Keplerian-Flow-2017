# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 17:03:21 2017

@author: Borrero Lab
"""

import openpiv.tools; 
import openpiv.process; 
import openpiv.scaling; 
import openpiv.validation; 
import openpiv.filters; 

import os; 
import numpy; 
import scipy.optimize; 
import matplotlib.pyplot as pp; 

def get_dir (what = "in", letter = "a"): 
	folder = "CameraTests"; 
	if what == "out-piv": 
		folder = "PIV-Output"; 
	return "/Ruvim/{}/Keplerian-setup01-2-21-{}".format (folder, letter); 

def spyder_is_silly (): 
	return "Silly!"; 

l = "e"; 
read_dir = get_dir ("in", l); 
write_dir = get_dir ("out-piv", l); 

frame_1 = openpiv.tools.imread (read_dir + "/frame0001.bmp"); 
frame_2 = openpiv.tools.imread (read_dir + "/frame0002.bmp"); 

frame_1 = frame_1.astype (numpy.int32); 
frame_2 = frame_2.astype (numpy.int32); 

px_per_mm = 108.977 / 20.05; 

mm_center = numpy.array ([616 + 22, 529 - 29]) / px_per_mm; 
mm_innerR = 500 / 2 / px_per_mm; 
mm_outerR = 850 / 2 / px_per_mm; 
mm_uncertainty = 3; # How many mm uncertain about the center. 

def optimize_center (center, x, y, u, v, r_from, r_to): 
	r = numpy.sqrt ((x - center[0]) ** 2 + (y - center[1]) ** 2); 
	v_r = numpy.sqrt (u ** 2 + v ** 2); 
	v_mask = (r > r_from) * (r < r_to); 
	v_part = v_r[v_mask]; 
	r_part = r[v_mask]; 
	fit_x = 1 / numpy.log (r_part); 
	fit_y = numpy.log (v_part) * fit_x; 
	bad_mask = numpy.isnan (fit_x) * numpy.isnan (fit_y) * \
			numpy.isinf (fit_x) * numpy.isinf (fit_y); 
	good_mask = ~bad_mask; 
	if good_mask.sum () < 1: 
		print ("Warning:  good_mask has no True values; center: {}, {}" \
									.format (center[0], center[1])); 
		return 1e8; 
	fit = numpy.polyfit (fit_x[good_mask], fit_y[good_mask], 1); 
	A = numpy.exp (fit[0]); 
	B = fit[1]; 
	f_part = A * r_part ** B; 
	diff = numpy.abs (v_part - f_part).sum (); 
	print ("Diff: {}, {} -> {}".format (center[0] * px_per_mm, center[1] * px_per_mm, diff)); 
	return diff; 

def optimize_center2 (center, x, y, u, v, r_from, r_to, rel_offset): 
	return optimize_center (center, x, y, u, v, r_from, r_to) - rel_offset; 

win_size = 10; 
search_size = 15; 
piv_overlap = 2; 

u, v, sig2noise = openpiv.process.extended_search_area_piv (frame_1, frame_2, \
							window_size = win_size, overlap = piv_overlap, \
							dt = 0.02, \
							search_area_size = search_size, 
							sig2noise_method = 'peak2peak'); 

x, y = openpiv.process.get_coordinates (image_size = frame_1.shape, \
			window_size = win_size, overlap = piv_overlap); 

px_x = x.copy (); 
px_y = y.copy (); 

x, y, u, v = openpiv.scaling.uniform (x, y, u, v, scaling_factor = px_per_mm); 

x_copy = x.copy (); 
y_copy = y.copy (); 

# Find the center: 
a = (x, y, u, v, \
								(mm_innerR + mm_outerR) / 2, \
								mm_outerR); 
a2 = a + (optimize_center (mm_center, *a),); 
b = ((mm_center[0] - mm_uncertainty, \
									mm_center[0] + mm_uncertainty), \
	(mm_center[1] - mm_uncertainty, mm_center[1] + mm_uncertainty)); 
opt_r = scipy.optimize.minimize (optimize_center2, mm_center, \
							args = a2, \
							bounds = b); 
mm_center = opt_r.x; 

spyder_is_silly (); 

u_copy = u.copy (); 
v_copy = v.copy (); 

u[numpy.abs (u) > 25] = numpy.nan; 
v[numpy.abs (v) > 25] = numpy.nan; 

spyder_is_silly (); 

x -= mm_center[0]; 
y -= mm_center[1]; 
r = numpy.sqrt (x ** 2 + y ** 2); 

u[r < mm_innerR] = numpy.nan; 
u[r > mm_outerR] = numpy.nan; 

v[r < mm_innerR] = numpy.nan; 
v[r > mm_outerR] = numpy.nan; 

u[numpy.isnan (u)] = 0; 
v[numpy.isnan (v)] = 0; 

pp.quiver (x, y, u, v, numpy.hypot (u, v)); 
pp.savefig ("test1.png", dpi = 1200); 

val_u, val_v, mask = openpiv.validation.sig2noise_val (u, v, sig2noise, threshold = 1.3); 

pp.quiver (x, y, val_u, val_v, numpy.hypot (val_u, val_v)); 
pp.savefig ("test2m.png", dpi = 1200); 

val_u, val_v = openpiv.filters.replace_outliers (val_u, val_v, method = 'localmean', \
			kernel_size = 2); 

raw_u = u.copy (); 
raw_v = v.copy (); 


v_r = numpy.sqrt (u ** 2 + v ** 2); 
v_w = v_r / r; # angular velocity 


r_icnt = 50; 
r_ints = numpy.linspace (mm_innerR, mm_outerR, r_icnt); 

v_m = []; 
v_s = []; 

d_m = []; 
d_s = []; 

r_m = []; 
r_s = []; 

for i in range (0, r_icnt - 1): 
	r_from = r_ints[i]; 
	r_to = r_ints[i + 1]; 
	r_mask = (r >= r_from) * (r < r_to); 
	v_sub = v_r[r_mask]; 
	w_sub = v_w[r_mask]; 
	v_m.append (v_sub.mean ()); 
	v_s.append (v_sub.std ()); 
	d_m.append (w_sub.mean () * 180 / numpy.pi); 
	d_s.append (w_sub.std () * 180 / numpy.pi); 
	r_m.append ((r_from + r_to) / 2); 
	r_s.append ((r_to - r_from) / 2); 


v_m = numpy.array (v_m); 
v_s = numpy.array (v_s); 

d_m = numpy.array (d_m); 
d_s = numpy.array (d_s); 

r_m = numpy.array (r_m); 
r_s = numpy.array (r_s); 



r_tail = 50; 



rav_r = r.ravel (); 
rav_v = v_r.ravel (); 
rav_d = v_w.ravel () * 180 / numpy.pi; 


fit_mask = (rav_v > 0) * (rav_r > mm_innerR) * (rav_r < mm_outerR); 

fit_ds = rav_d[fit_mask]; 
fit_rs = rav_r[fit_mask]; 

fit_x = 1 / numpy.log (fit_rs); 
fit_y = numpy.log (fit_ds) * fit_x; 

fit = numpy.polyfit (fit_x, fit_y, 1); 

A_d = numpy.exp (fit[0]); 
B_d = fit[1]; 



fit_vs = rav_v[fit_mask]; 
fit_y = numpy.log (fit_vs) * fit_x; 

fit = numpy.polyfit (fit_x, fit_y, 1); 
A_v = numpy.exp (fit[0]); 
B_v = fit[1]; 



some_rs = numpy.linspace (mm_innerR, mm_outerR, 128); 
some_x = 1 / numpy.log (some_rs); 







if not os.path.exists (write_dir): 
	os.makedirs (write_dir); 
openpiv.tools.save (x, y, u, v, mask, write_dir + "/exp1_001.txt"); 




pp.figure (); 
pp.title ("Angular Velocity Fit Intermediate"); 
pp.xlabel ("1/ln(r)"); 
pp.ylabel ("ln($\omega$)/ln(r)"); 
#pp.errorbar (r_m, d_m, d_s, r_s, fmt = 'o', label = "Data"); 
pp.plot (fit_x, numpy.log (fit_ds) * fit_x, 'o', label = "Samples"); 
pp.plot (some_x, numpy.log (A_d) * some_x + B_d, linewidth = 2, label = "Linear Fit"); 
pp.legend (loc = "best"); 
pp.savefig (write_dir + "/interm-angular-velocity.png", dpi = 1200); 


pp.figure (); 
pp.title ("|Velocity| Fit Intermediate"); 
pp.xlabel ("1/ln(r)"); 
pp.ylabel ("ln($v$)/ln(r)"); 
#pp.errorbar (r_m, v_m, v_s, r_s, fmt = 'o', label = "Data"); 
pp.plot (fit_x, numpy.log (fit_vs) * fit_x, 'o', label = "Samples"); 
pp.plot (some_x, numpy.log (A_d) * some_x + B_d, linewidth = 2, label = "Linear Fit"); 
pp.legend (loc = "best"); 
pp.savefig (write_dir + "/interm-magnitude-velocity.png", dpi = 1200); 




pp.figure (); 
pp.title ("Angular Velocity Distribution"); 
pp.xlabel ("Radial Position (mm)"); 
pp.ylabel ("Angular Velocity (deg/s)"); 
#pp.errorbar (r_m, d_m, d_s, r_s, fmt = 'o', label = "Data"); 
pp.plot (rav_r, rav_d, 'o', label = "Samples")
pp.plot (some_rs, A_d * some_rs ** B_d, linewidth = 2, label = "Exp. Fit"); 
pp.legend (loc = "best"); 
pp.savefig (write_dir + "/angular-velocity.png", dpi = 1200); 


pp.figure (); 
pp.title ("|Velocity| Distribution"); 
pp.xlabel ("Radial Position (mm)"); 
pp.ylabel ("|Velocity| (mm/s)"); 
#pp.errorbar (r_m, v_m, v_s, r_s, fmt = 'o', label = "Data"); 
pp.plot (rav_r, rav_v, 'o', label = "Samples"); 
pp.plot (some_rs, A_v * some_rs ** B_v, linewidth = 2, label = "Exp. Fit"); 
pp.legend (loc = "best"); 
pp.savefig (write_dir + "/magnitude-velocity.png", dpi = 1200); 






pp.figure (); 
pp.title ("Angular Velocity Histogram"); 
pp.xlabel ("Angular Velocity (deg/s)"); 
pp.ylabel ("Likelihood"); 
#pp.errorbar (r_m, v_m, v_s, r_s, fmt = 'o', label = "Data"); 
pp.hist (fit_ds, bins = 50); 
#pp.legend (loc = "best"); 
pp.savefig (write_dir + "/hist-angular-velocity.png", dpi = 1200); 

pp.figure (); 
pp.title ("Velocity Histogram"); 
pp.xlabel ("|Velocity| (mm/s)"); 
pp.ylabel ("Likelihood"); 
#pp.errorbar (r_m, v_m, v_s, r_s, fmt = 'o', label = "Data"); 
pp.hist (fit_vs, bins = 50); 
#pp.legend (loc = "best"); 
pp.savefig (write_dir + "/hist-magnitude-velocity.png", dpi = 1200); 




colors = numpy.hypot (u, v); 

pp.figure (); 
pp.quiver (x, y, u, v, colors); 
pp.savefig (write_dir + "/exp1_001.png", dpi = 1200); 

pp.figure (); 
pp.quiver (x, y, u / v_r, v / v_r, colors); 
pp.savefig (write_dir + "/exp1_001unit.png", dpi = 1200); 




with open (write_dir + "/velocity-profile-fit.txt", "w") as file: 
	print ("Velocity Profile Fit Results: ", file = file); 
	print ("\tAngular Velocity:  w = {} * r^({}) [deg/s]".format (A_d, B_d), file = file); 
	print ("\tMagnitude Velocity:  v = {} * r^({}) [mm/s]".format (A_v, B_v), file = file); 




