# -*- coding: utf-8 -*-
"""
Created on Fri Mar 24 18:05:15 2017

@author: Borrero Lab
"""

import os; 
import numpy; 
import scipy.misc; 
import scipy.optimize; 
import matplotlib.pyplot as pp; 
import openpiv.scaling; 

def get_dir (what = "in", amps = "0.10A"): 
	folder = "Recordings"; 
	if what == "out-piv": 
		folder = "PIV-Output"; 
	return "/Ruvim/{}/Setup-01/{}".format (folder, amps); 

# The default '%' modulus operator was behaving a bit weird: 
# float ("0.009") % 0.001 
# => 0.0009999999999999992 
# when in fact it should be zero (0). So, here is my (Ruvim's) 
# whip-up of a custom modulus function ... 
def mod (dividend, divisor): 
	quotient = numpy.round (dividend / divisor, 2); 
	return (quotient - int (quotient)) * divisor; 
# mod (0.009, 0.001) returns 0, correctly. 
# The 'quotient' rounding to 2 decimal places fixes the error of mod (0.3, 0.05); 

def spyder_is_silly (): 
	return "Silly!"; 

# Water radii: 
r_min = 30; 
r_max = 90; 

r_fit_start = 60; 
r_fit_stop = 65; 

r_best_min = 55; 
r_best_max = 65; 

v2_filter_parameter = 0.5; # 1 = filter just below mean, 1/2 = filter below 1/rad(2) of mean, etc. 
filter_bottom_percentile = 10; 
filter_top_percentile = 90; 
need_scale = 1; 

plot_dpi = 300; 

flag__do_plot = True; 
flag__include_title = False; 

ls = ["e" 
]; 

error_log_filename = "errors.txt"; 

overall_dir = get_dir ("out-piv", "Overall"); 
results_dir = "{}/{}".format (overall_dir, "Calculations"); 
if os.path.exists (results_dir + "/overall-numbers.dat"): 
	the_results = numpy.loadtxt (results_dir + "/overall-numbers.dat"); 
	Ap_best_x = the_results[0, 2]; # Take from the "500 and below" data, i.e., row index 0 
	# because "500 and below" comes first alphabetically. 
else: 
	Ap_best_x = None; 

overall_head = "I (A)	A_d()	B_d()	delta{B_d}	A_p()	delta{A_p}	Ap yint est	Ap yint std	Ap slope est	Ap slope std	10% w	90% w	Ak est	Ak std"; 

overall_01 = numpy.zeros ((0, 14)); 
overall_05 = numpy.zeros ((0, 14)); 
overall_50 = numpy.zeros ((0, 14)); 
overall_500_and_below = numpy.zeros ((0, 14)); 
overall_below_100 = numpy.zeros ((0, 14)); 
overall_all = numpy.zeros ((0, 14)); 

last_A = 0; 

pp.close ("all"); 

for l in ls: 
	read_dir = "/Ruvim/CameraTests/Keplerian-setup01-2-21-e"; # get_dir ("in", l); 
	write_dir = get_dir ("out-piv", l); 
	
	if not os.path.exists (read_dir): 
		with open (error_log_filename, "a") as file: 
			print ("Could not find directory {}".format (read_dir), file = file); 
		continue; 
	
	print ("Plotting (or just analyzing) results from {} ...".format (l)); 
	spyder_is_silly (); 
	
	x = numpy.load (write_dir + "/out-x.npy"); 
	y = numpy.load (write_dir + "/out-y.npy"); 
	val_u = numpy.load (write_dir + "/out-avg-u.npy"); 
	val_v = numpy.load (write_dir + "/out-avg-v.npy"); 
	use_count = numpy.load (write_dir + "/out-used-counts.npy"); 
	center_data = numpy.load (write_dir + "/out-center-xy.npy"); 
	mm_center = center_data[0 : 2]; 
	px_per_mm = center_data[2]; 
	
	need_scale = (202 / 36) / px_per_mm; 
	
	val_u[numpy.isnan (val_u)] = 0; 
	val_v[numpy.isnan (val_v)] = 0; 
	
	if need_scale != 1: 
		x, y, val_u, val_v = openpiv.scaling.uniform (x, y, val_u, val_v, scaling_factor = need_scale); 
	
	#val_u, val_v = openpiv.filters.replace_outliers (val_u, val_v, method = 'localmean', \
	#			kernel_size = 2); 
	
	# Test plot: 
	#pp.quiver (x, y, val_u, val_v, numpy.hypot (val_u, val_v)); 
	#pp.savefig ("test2q.png", dpi = 1200); 
	
	u, v = val_u, val_v; 
	
	r_center = numpy.array ([mm_center[1], mm_center[0]]); 
	
	def mean_vr (center): 
		r = numpy.sqrt ((x - center[0]) ** 2 + (y - center[1]) ** 2); 
		r[r == 0] = 1; # Prevent divide by 0. 
		xu = (x - center[0]) / r; 
		yu = (y - center[1]) / r; 
		v_r_dir = u * xu + v * yu; 
		return 100 * (v_r_dir ** 2).mean (); 
	
	result = scipy.optimize.minimize (mean_vr, r_center); 
	r_center = result.x; 
	
	r = numpy.sqrt ((x - r_center[0]) ** 2 + (y - r_center[1]) ** 2); 
	divisor = r.copy (); 
	divisor[divisor == 0] = 1; 
	x_unit = (x - r_center[0]) / divisor; 
	y_unit = (y - r_center[1]) / divisor; 
	v_r_dir = u * x_unit + v * y_unit; 
	v_r_dir_copy = v_r_dir.copy (); 
	mean_v2 = (v_r_dir ** 2).mean (); 
	mask_below = v_r_dir ** 2 < v2_filter_parameter * mean_v2; 
	mask_above = ~mask_below; 
	u[mask_above] = 0; 
	v[mask_above] = 0; 
	
	result = scipy.optimize.minimize (mean_vr, r_center); 
	r_center = result.x; 
	
	x -= r_center[0]; 
	y -= r_center[1]; 
	r = numpy.sqrt (x ** 2 + y ** 2); 
	
	x_unit = x / r; 
	y_unit = y / r; 
	
	x_unit[numpy.isnan (x_unit)] = 0; 
	y_unit[numpy.isnan (y_unit)] = 0; 
	
	v_r_dir = u * x_unit + v * y_unit; 
	v_t_dir = u * (-y_unit) + v * x_unit; 
	
	if flag__do_plot: 
		pp.figure (figsize = (6, 4)); 
		if flag__include_title: 
			pp.title ("Comparison of $v_r$ before and after filter"); 
		pp.xlabel ("Pixel Ravel Position (px)"); 
		pp.ylabel ("$v_r$ (mm/s)"); 
		pp.plot (v_r_dir_copy.ravel (), label = "Before"); 
		pp.plot (v_r_dir.ravel (), label = "After"); 
		pp.savefig (write_dir + "/filter-v_r-before-after.png", dpi = plot_dpi); 
	
	#v_r = numpy.sqrt (u ** 2 + v ** 2); 
	v_r = v_t_dir.copy (); # Take just the azimuthal component. 
	
	v_r_vis = v_r.copy (); 
	
	# Create a mask where the radial component of v^2 is less than the mean of v^2: 
	#mean_v = (v_r_dir ** 2).mean (); 
	#mask_below = v_r_dir ** 2 < mean_v; 
	# This is a simple kind of validation scheme, I guess (I'm Ruvim, by the way). 
	
	if flag__do_plot: 
		pp.figure (figsize = (6, 6)); 
		pp.pcolormesh (x, y, v_r_vis); 
		pp.savefig (write_dir + "/param-map-draft-vr_vis.png", dpi = plot_dpi); 
	
	# Let's find the x and y for the center: 
	v_max = v_r.max (); 
	#v_check = v_max * 0.5; 
	#c_mask = v_r > v_check; 
	#x_possible = numpy.array ([x[c_mask].min (), x[c_mask].max ()]); 
	#y_possible = numpy.array ([y[c_mask].min (), y[c_mask].max ()]); 
	#r_center = numpy.array ([x_possible.mean (), y_possible.mean ()]); 
	
	#r_center = numpy.array ([mm_center[1], mm_center[0]]); 
	
	#need_x_min = x[(x >= x_possible[0]) * (x <= x_possible[1]) * \
	#			(y >= y_possible[0]) * (y <= r_center[1]) * (v_r == 0)].min (); 
	#need_x_max = x[(x >= x_possible[0]) * (x <= x_possible[1]) * \
	#			(y >= r_center[1]) * (y <= y_possible[1]) * (v_r == 0)].max (); 
	#need_y_min = y[(y >= y_possible[0]) * (y <= y_possible[1]) * \
	#			(x >= x_possible[0]) * (x <= r_center[0]) * (v_r == 0)].min (); 
	#need_y_max = y[(y >= y_possible[0]) * (y <= y_possible[1]) * \
	#			(x >= r_center[0]) * (y <= x_possible[1]) * (v_r == 0)].max (); 
	#r_center = numpy.array ([(need_x_min + need_x_max) / 2, \
	#					(need_y_min + need_y_max) / 2]); 
	
	#x -= r_center[0]; 
	#y -= r_center[1]; 
	#r = numpy.sqrt (x ** 2 + y ** 2); 
	
	
	v_w = v_r / r; # angular velocity 
	
	
	
	
	
	
	
	
	
	rav_r = r.ravel (); 
	rav_v = v_r.ravel (); 
	rav_d = v_w.ravel () * 180 / numpy.pi; 
	rav_w = v_w.ravel (); 
	
	r_tail = r_fit_start; 
	r_stop = r_fit_stop; 
	
	fit_mask = (rav_v > 0); # * (rav_r > mm_innerR) * (rav_r < mm_outerR); 
	fit_input_additional_mask = (rav_r[fit_mask] >= r_tail) * \
							(rav_r[fit_mask] <= r_stop); 
	
	fit_ds = rav_d[fit_mask]; 
	fit_rs = rav_r[fit_mask]; 
	fit_ws = rav_w[fit_mask]; 
	fit_vs = rav_v[fit_mask]; 
	
#	fit_bottom_d = numpy.percentile (fit_ds[fit_input_additional_mask], filter_bottom_percentile); 
#	#fit_top_10d = numpy.percentile (fit_ds, 90); 
#	fit_bottom_v = numpy.percentile (fit_vs[fit_input_additional_mask], filter_bottom_percentile); 
#	#fit_top_10v = numpy.percentile (fit_vs, 90); 
#	
#	fit_midrange_mask_d = (fit_ds >= fit_bottom_d); # * (fit_ds <= fit_top_10d); 
#	fit_midrange_mask_v = (fit_vs >= fit_bottom_v); # * (fit_vs <= fit_top_10v); 
	
	# Let's not use these masks for the flow profiles because they create problems ... 
	fit_midrange_mask_d = True; 
	fit_midrange_mask_v = True; 
	# They are more appropriate for the A_p calculations, actually. 
	
	fit_used_ds = fit_ds[fit_input_additional_mask * fit_midrange_mask_d]; 
	fit_used_ws = fit_ws[fit_input_additional_mask * fit_midrange_mask_d]; 
	
	fit_used_rs_d = fit_rs[fit_input_additional_mask * fit_midrange_mask_d]; 
	fit_used_rs_v = fit_rs[fit_input_additional_mask * fit_midrange_mask_v]; 
	
	
	
	fit_x = numpy.log (fit_rs); 
	fit_y = numpy.log (fit_ds); 
	
	samples_x = fit_x[fit_input_additional_mask * fit_midrange_mask_d]; 
	samples_y = fit_y[fit_input_additional_mask * fit_midrange_mask_d]; 
	
	taylor_fit_N = (fit_input_additional_mask * fit_midrange_mask_d).sum (); # Number of points. 
	taylor_fit_delta = taylor_fit_N * (samples_x ** 2).sum () - samples_x.sum () ** 2; 
	taylor_fit_A = ((samples_x ** 2).sum () * samples_y.sum () - \
					samples_x.sum () * (samples_x * samples_y).sum ()) \
				/ taylor_fit_delta; 
	taylor_fit_B = (taylor_fit_N * (samples_x * samples_y).sum () - \
					samples_x.sum () * samples_y.sum ()) \
				/ taylor_fit_delta; 
	taylor_sigma_y = numpy.sqrt (1 / (taylor_fit_N - 2) * ( 
					(samples_y - taylor_fit_A - taylor_fit_B * samples_x) ** 2 
				).sum ()); 
	taylor_sigma_A = taylor_sigma_y * numpy.sqrt ((samples_x ** 2).sum () / taylor_fit_delta); 
	taylor_sigma_B = taylor_sigma_y * numpy.sqrt (taylor_fit_N / taylor_fit_delta); 
	spyder_is_silly (); 
	
	A_d = numpy.exp (taylor_fit_A); 
	B_d = taylor_fit_B; 
	
	sigma_Ad = taylor_sigma_A * numpy.exp (A_d); 
	sigma_Bd = taylor_sigma_B; 
	
	
	
	
	
	fit_x = numpy.log (fit_rs); 
	fit_y = numpy.log (fit_vs); 
	
	samples_x = fit_x[fit_input_additional_mask * fit_midrange_mask_v]; 
	samples_y = fit_y[fit_input_additional_mask * fit_midrange_mask_v]; 
	
	taylor_fit_N = (fit_input_additional_mask * fit_midrange_mask_v).sum (); # Number of points. 
	taylor_fit_delta = taylor_fit_N * (samples_x ** 2).sum () - samples_x.sum () ** 2; 
	taylor_fit_A = ((samples_x ** 2).sum () * samples_y.sum () - \
					samples_x.sum () * (samples_x * samples_y).sum ()) \
				/ taylor_fit_delta; 
	taylor_fit_B = (taylor_fit_N * (samples_x * samples_y).sum () - \
					samples_x.sum () * samples_y.sum ()) \
				/ taylor_fit_delta; 
	taylor_sigma_y = numpy.sqrt (1 / (taylor_fit_N - 2) * ( 
					(samples_y - taylor_fit_A - taylor_fit_B * samples_x) ** 2 
				).sum ()); 
	taylor_sigma_A = taylor_sigma_y * numpy.sqrt ((samples_x ** 2).sum () / taylor_fit_delta); 
	taylor_sigma_B = taylor_sigma_y * numpy.sqrt (taylor_fit_N / taylor_fit_delta); 
	spyder_is_silly (); 
	
	A_v = numpy.exp (taylor_fit_A); 
	B_v = taylor_fit_B; 
	
	sigma_Av = taylor_sigma_A * numpy.exp (A_d); 
	sigma_Bv = taylor_sigma_B; 
	
	
	
	fit_used_x = fit_x[fit_input_additional_mask * fit_midrange_mask_d]; 
	
	
	
	fit_used_rs = fit_used_rs_d; 
	
	fit_used_rs_min = fit_used_rs.min () if fit_used_rs.shape[0] > 0 else 30; 
	fit_used_rs_max = fit_used_rs.max () if fit_used_rs.shape[0] > 0 else 90; 
	some_rs = numpy.linspace (max (fit_used_rs_min, 1), fit_used_rs_max, 128); 
	some_x = 1 / numpy.log (some_rs); 
	
	
	
	
	if os.path.exists (read_dir + "/measurements.txt"): 
		meas = numpy.loadtxt (read_dir + "/measurements.txt"); 
	else: 
		meas = numpy.zeros ((0, 2)); 
	
	r_offs = 18; # mm 
	meas[:, 0] += r_offs; 
	
	#m_mask = (meas[:, 0] >= 50) * (meas[:, 0] <= 80); 
	#w_mask = (fit_rs >= 50) * (fit_rs <= 80); 
	
	m_mask = (meas[:, 0] >= r_fit_start) * (meas[:, 0] <= r_fit_stop); 
	w_mask = (fit_rs >= r_fit_start) * (fit_rs <= r_fit_stop); 
	
	
	Ap_params = numpy.loadtxt (read_dir + "/Ap-params.txt"); 
	water_level = Ap_params[0]; # in mm 
	electric_current = Ap_params[1]; # in A 
	
	
	
	if meas.shape[0] > 1: 
		should_prop = meas[:, 1] / meas[:, 0] ** 2; 
		should_data = fit_ds[w_mask] / numpy.interp (fit_rs[w_mask], meas[:, 0], should_prop); 
		should_mean = should_data.mean (); 
		#s_mask1 = (fit_rs >= 50) * (fit_rs <= 80); 
		s_mask1 = (fit_rs >= r_best_min) * (fit_rs <= r_best_max); 
		#bottom_should = numpy.percentile (should_data[s_mask1[w_mask]], filter_bottom_percentile); 
		#s_mask1[w_mask] *= (should_data[s_mask1[w_mask]] >= bottom_should); 
		if Ap_best_x is not None: 
			should_A = Ap_best_x * electric_current / (2 * numpy.pi * water_level); 
			should_A *= 180 / numpy.pi; # Convert to degrees. 
			should_using_ap = True; 
		else: 
			should_A = should_data[s_mask1[w_mask]].mean (); 
			should_using_ap = False; 
	else: 
		should_prop = None; 
		should_A = None; 
	
	
	
	
	if not os.path.exists (write_dir): 
		os.makedirs (write_dir); 
	
	
	
	if flag__do_plot: 
		to_y_d = 2 * numpy.percentile (numpy.log (fit_ds), 90); 
		to_y_v = 2 * numpy.percentile (numpy.log (fit_vs), 90); 
		if to_y_d < 0: # Negative; find the more negative percentile instead! 
			to_y_d = 2 * numpy.percentile (numpy.log (fit_ds), 10); 
		if to_y_v < 0: # Negative; find the more negative percentile instead! 
			to_y_v = 2 * numpy.percentile (numpy.log (fit_vs), 10); 
		
		pp.figure (figsize = (6, 4)); 
		if flag__include_title: 
			pp.title ("Angular Velocity Fit Intermediate"); 
		pp.xlabel ("ln(r)"); 
		pp.ylabel ("ln($\omega$)"); 
		pp.xlim (numpy.log (r_min + 10), numpy.log (r_max - 10)); 
		if to_y_d < 0: 
			pp.ylim (to_y_d, 0); 
		else: 
			pp.ylim (0, to_y_d); 
		pp.plot (numpy.log (fit_rs), numpy.log (fit_ds), 'o', markersize = 1, label = "Samples"); 
		pp.plot (fit_used_x, numpy.log (A_d) + B_d * fit_used_x, linewidth = 2, label = "Linear Fit"); 
		pp.legend (loc = "best"); 
		pp.savefig (write_dir + "/interm-angular-velocity.png", dpi = plot_dpi); 
		
		
		pp.figure (figsize = (6, 4)); 
		if flag__include_title: 
			pp.title ("Velocity Fit Intermediate"); 
		pp.xlabel ("ln(r)"); 
		pp.ylabel ("ln($v$)"); 
		pp.xlim (numpy.log (r_min + 10), numpy.log (r_max - 10)); 
		if to_y_v < 0: 
			pp.ylim (to_y_v, 0); 
		else: 
			pp.ylim (0, to_y_v); 
		pp.plot (numpy.log (fit_rs), numpy.log (fit_vs), 'o', markersize = 1, label = "Samples"); 
		pp.plot (fit_used_rs_v, numpy.log (A_v) + fit_used_rs_v * B_v, linewidth = 2, label = "Linear Fit"); 
		pp.legend (loc = "best"); 
		pp.savefig (write_dir + "/interm-magnitude-velocity.png", dpi = plot_dpi); 
	
	
	if meas.shape[0] > 0: 
		our_ap_mask_1 = (rav_r >= r_best_min) * (rav_r <= r_best_max) * (rav_d > 0); 
		meas_mask = (meas[:, 0] >= r_best_min) * (meas[:, 0] <= r_best_max); 
		Ap_data = 2 * numpy.pi * water_level / (electric_current * \
				numpy.interp (rav_r, meas[:, 0], meas[:, 1])) * \
				rav_r ** 2 * rav_w; 
		bottom_ap = numpy.percentile (Ap_data[our_ap_mask_1], filter_bottom_percentile); 
		top_ap = numpy.percentile (Ap_data[our_ap_mask_1], filter_top_percentile); 
		our_ap_mask = our_ap_mask_1 * (Ap_data >= bottom_ap) * (Ap_data <= top_ap); 
		Ap_fit = numpy.array ([Ap_data[our_ap_mask].mean (), Ap_data[our_ap_mask].std ()]); 
		#r_range = numpy.linspace (r_tail, r_stop, 2); 
		r_range = numpy.linspace (r_best_min, r_best_max, 2); 
		
		samples_x = rav_r[our_ap_mask]; 
		samples_y = Ap_data[our_ap_mask]; 
		
		taylor_fit_N = fit_input_additional_mask.sum (); # Number of points. 
		taylor_fit_delta = taylor_fit_N * (samples_x ** 2).sum () - samples_x.sum () ** 2; 
		taylor_fit_A = ((samples_x ** 2).sum () * samples_y.sum () - \
						samples_x.sum () * (samples_x * samples_y).sum ()) \
					/ taylor_fit_delta; 
		taylor_fit_B = (taylor_fit_N * (samples_x * samples_y).sum () - \
						samples_x.sum () * samples_y.sum ()) \
					/ taylor_fit_delta; 
		taylor_sigma_y = numpy.sqrt (1 / (taylor_fit_N - 2) * ( 
						(samples_y - taylor_fit_A - taylor_fit_B * samples_x) ** 2 
					).sum ()); 
		taylor_sigma_A = taylor_sigma_y * numpy.sqrt ((samples_x ** 2).sum () / taylor_fit_delta); 
		taylor_sigma_B = taylor_sigma_y * numpy.sqrt (taylor_fit_N / taylor_fit_delta); 
		spyder_is_silly (); 
		
		Ap_A = [taylor_fit_A, taylor_sigma_A]; 
		Ap_B = [taylor_fit_B, taylor_sigma_B]; 
		
		Ak_data = Ap_fit[0] * electric_current * meas[:, 1][meas_mask] / \
			(2 * numpy.pi * water_level * meas[:, 0][meas_mask] ** (B_d + 2)); 
		Ak = numpy.array ([Ak_data.mean (), Ak_data.std ()]); 
		
		if flag__do_plot: 
			pp.figure (figsize = (6, 4)); 
			pp.xlabel ("Radial Position (mm)"); 
			pp.ylabel ("$A_p$ (mm^3 rad / A s gauss)"); 
			pp.ylim (0, 2 * max (numpy.percentile (Ap_data, 90), Ap_fit[0])); 
			pp.plot (rav_r, Ap_data, 'o', markersize = 1, label = "Samples"); 
			pp.plot (r_range, r_range * 0 + Ap_fit[0], linewidth = 2, label = "Mean"); 
			pp.legend (loc = "best"); 
			pp.savefig (write_dir + "/Ap-data.png", dpi = 300); 
			
			pp.figure (figsize = (6, 4)); 
			pp.xlabel ("Radial Position (mm)"); 
			pp.ylabel ("$A_p$ (mm^3 rad / A s gauss)"); 
			pp.ylim (0, 2 * max (numpy.percentile (Ap_data, 90), Ap_fit[0])); 
			pp.plot (rav_r, Ap_data, 'o', markersize = 1, label = "Samples"); 
			pp.plot (r_range, r_range * Ap_B[0] + Ap_A[0], linewidth = 2, label = "Best Fit"); 
			pp.legend (loc = "best"); 
			pp.savefig (write_dir + "/Ap-linear.png", dpi = plot_dpi); 
	
	
	top_percentile = numpy.percentile (fit_ds, 99); 
	if top_percentile < 0.5: 
		y_plot_max = int (top_percentile / 0.1) * 0.1 + 0.2; 
	elif top_percentile < 5: 
		y_plot_max = int (top_percentile) + 1; 
	else: 
		y_plot_max = int (top_percentile / 5) * 5 + 5; 
	
	if flag__do_plot: 
		pp.figure (figsize = (6, 4)); 
		if flag__include_title: 
			pp.title ("Angular Velocity Distribution"); 
		pp.xlabel ("Radial Position (mm)"); 
		pp.ylabel ("Angular Velocity (deg/s)"); 
		pp.xlim (r_min - 10, r_max + 20); 
		pp.ylim (0, y_plot_max); 
		pp.plot (fit_rs, fit_ds, 'o', markersize = 1, label = "Samples $(r, \Omega)$"); 
		pp.plot (some_rs, A_d * some_rs ** B_d, linewidth = 2, label = "Fit $\propto r^P$"); 
		pp.legend (loc = "best"); 
		pp.savefig (write_dir + "/angular-velocity--samples-fit.png", dpi = plot_dpi); 
		
		label_should = "Expected $\propto B_z(r) / r^2$"; 
		if should_using_ap: 
			label_should = "Expected $\\Omega (r) = \\frac{A_p I B (r)}{2 \\pi z r^2}$"; 
		
		pp.figure (figsize = (6, 4)); 
		if flag__include_title: 
			pp.title ("Angular Velocity Distribution"); 
		pp.xlabel ("Radial Position (mm)"); 
		pp.ylabel ("Angular Velocity (deg/s)"); 
		pp.xlim (r_min - 10, r_max + 20); 
		pp.ylim (0, y_plot_max); 
		pp.plot (fit_rs, fit_ds, 'o', markersize = 1, label = "Samples $(r, \Omega)$"); 
		if should_A is not None: 
			positive_mask = should_prop >= 0; 
			in_water_mask = (meas[:, 0] >= r_min) * (meas[:, 0] <= r_max); 
			should_mask = positive_mask * in_water_mask; 
			pp.plot (meas[:, 0][should_mask], should_A * should_prop[should_mask], color = "#cc8800", linewidth = 2, label = label_should); 
		pp.legend (loc = "best"); 
		pp.savefig (write_dir + "/angular-velocity--samples-should.png", dpi = plot_dpi); 
		
		pp.figure (figsize = (6, 4)); 
		if flag__include_title: 
			pp.title ("Angular Velocity Distribution"); 
		pp.xlabel ("Radial Position (mm)"); 
		pp.ylabel ("Angular Velocity (deg/s)"); 
		pp.xlim (r_min - 10, r_max + 20); 
		pp.ylim (0, y_plot_max); 
		if should_A is not None: 
			positive_mask = should_prop >= 0; 
			in_water_mask = (meas[:, 0] >= r_min) * (meas[:, 0] <= r_max); 
			should_mask = positive_mask * in_water_mask; 
			pp.plot (meas[:, 0][should_mask], should_A * should_prop[should_mask], color = "#cc8800", linewidth = 2, label = label_should); 
		pp.plot (some_rs, A_d * some_rs ** B_d, 'g', linewidth = 2, label = "Fit $\propto r^P$"); 
		pp.legend (loc = "best"); 
		pp.savefig (write_dir + "/angular-velocity--fit-should.png", dpi = plot_dpi); 
		
		pp.figure (figsize = (6, 4)); 
		if flag__include_title: 
			pp.title ("Angular Velocity Distribution"); 
		pp.xlabel ("Radial Position (mm)"); 
		pp.ylabel ("Angular Velocity (deg/s)"); 
		pp.xlim (r_min - 10, r_max + 20); 
		pp.ylim (0, y_plot_max); 
		pp.plot (fit_rs, fit_ds, 'o', markersize = 1, label = "Samples $(r, \Omega)$"); 
		if should_A is not None: 
			positive_mask = should_prop >= 0; 
			in_water_mask = (meas[:, 0] >= r_min) * (meas[:, 0] <= r_max); 
			should_mask = positive_mask * in_water_mask; 
			pp.plot (meas[:, 0][should_mask], should_A * should_prop[should_mask], color = "#cc8800", linewidth = 2, label = label_should); 
		pp.legend (loc = "best"); 
		pp.savefig (write_dir + "/angular-velocity--samples-should.png", dpi = plot_dpi); 
		
		pp.figure (figsize = (6, 4)); 
		if flag__include_title: 
			pp.title ("Angular Velocity Distribution"); 
		pp.xlabel ("Radial Position (mm)"); 
		pp.ylabel ("Angular Velocity (deg/s)"); 
		pp.xlim (r_min - 10, r_max + 20); 
		pp.ylim (0, y_plot_max); 
		pp.plot (fit_rs, fit_ds, 'o', markersize = 1, label = "Samples $(r, \Omega)$"); 
		if should_A is not None: 
			positive_mask = should_prop >= 0; 
			in_water_mask = (meas[:, 0] >= r_min) * (meas[:, 0] <= r_max); 
			should_mask = positive_mask * in_water_mask; 
			pp.plot (meas[:, 0][should_mask], should_A * should_prop[should_mask], color = "#cc8800", linewidth = 2, label = label_should); 
		pp.plot (some_rs, A_d * some_rs ** B_d, 'g', linewidth = 2, label = "Fit $\propto r^P$"); 
		pp.legend (loc = "best"); 
		pp.savefig (write_dir + "/angular-velocity--all.png", dpi = plot_dpi); 
		
		
		pp.figure (figsize = (6, 4)); 
		if flag__include_title: 
			pp.title ("Velocity Distribution"); 
		pp.xlabel ("Radial Position (mm)"); 
		pp.ylabel ("Velocity (mm/s)"); 
		pp.plot (fit_rs, fit_vs, 'o', markersize = 1, label = "Samples"); 
		pp.plot (fit_used_rs_v, A_v * fit_used_rs_v ** B_v, linewidth = 2, label = "Power Fit"); 
		pp.legend (loc = "best"); 
		pp.savefig (write_dir + "/magnitude-velocity.png", dpi = plot_dpi); 
	
	
	
	
	
	
	if flag__do_plot: 
		pp.figure (figsize = (6, 4)); 
		if flag__include_title: 
			pp.title ("Angular Velocity Histogram"); 
		pp.xlabel ("Angular Velocity (deg/s)"); 
		pp.ylabel ("Likelihood"); 
		pp.hist (fit_ds, range = (0.01, fit_ds.max ()), bins = 50, normed = True); 
		pp.savefig (write_dir + "/hist-angular-velocity.png", dpi = plot_dpi); 
		
		pp.figure (figsize = (6, 4)); 
		if flag__include_title: 
			pp.title ("Velocity Histogram"); 
		pp.xlabel ("Velocity (mm/s)"); 
		pp.ylabel ("Likelihood"); 
		pp.hist (fit_vs, range = (0.01, fit_vs.max ()), bins = 50, normed = True); 
		pp.savefig (write_dir + "/hist-magnitude-velocity.png", dpi = plot_dpi); 
	
	
	
	
	
	
	if flag__do_plot: 
		pp.figure (figsize = (6, 4)); 
		if flag__include_title: 
			pp.title ("Radial Position"); 
		pp.xlabel ("Position X (mm)"); 
		pp.ylabel ("Position Y (mm)"); 
		pp.xlim (x.min (), x.max ()); 
		pp.pcolormesh (x, y, r); 
		pp.colorbar (); 
		pp.savefig (write_dir + "/param-map-r.png", dpi = plot_dpi); 
	
	
	if flag__do_plot: 
		pp.figure (figsize = (6, 4.5)); 
		#pp.title ("Azimuthal Velocity (mm/s)"); 
		pp.xlabel ("Position X (mm)"); 
		pp.ylabel ("Position Y (mm)"); 
		pp.xlim (-r_max - 10, +r_max + 10); 
		pp.ylim (-r_max - 10, +r_max + 10); 
		pp.pcolormesh (x, y, v_r); 
		pp.colorbar (); 
		pp.savefig (write_dir + "/param-map-v_t.png", dpi = plot_dpi); 
	
	v_copy = v_r_dir.copy (); 
	v_copy[~mask_below] = 0; 
	if flag__do_plot: 
		pp.figure (figsize = (6, 4.5)); 
		#pp.title ("Radial Velocity (mm/s)"); 
		pp.xlabel ("Position X (mm)"); 
		pp.ylabel ("Position Y (mm)"); 
		pp.xlim (-r_max - 10, +r_max + 10); 
		pp.ylim (-r_max - 10, +r_max + 10); 
		pp.pcolormesh (x, y, v_copy); 
		pp.colorbar (); 
		pp.savefig (write_dir + "/param-map-v_r.png", dpi = plot_dpi); 
		
		pp.figure (figsize = (6, 4.5)); 
		#pp.title ("Validated Velocity Sample Grid"); 
		pp.xlabel ("Position X (mm)"); 
		pp.ylabel ("Position Y (mm)"); 
		pp.xlim (-r_max - 10, +r_max + 10); 
		pp.ylim (-r_max - 10, +r_max + 10); 
		pp.pcolormesh (x, y, use_count); 
		pp.colorbar (); 
		pp.savefig (write_dir + "/result-use-count.png", dpi = plot_dpi); 
	
	
	
	
	
	
	if flag__do_plot: 
		colors = numpy.hypot (u, v); 
		
		pp.figure (figsize = (6, 6)); 
		pp.quiver (x, y, u, v, colors); 
		pp.savefig (write_dir + "/exp1_001.png", dpi = plot_dpi); 
		
		pp.figure (figsize = (6, 6)); 
		pp.quiver (x, y, u / v_r, v / v_r, colors); 
		pp.savefig (write_dir + "/exp1_001unit.png", dpi = plot_dpi); 
		
		
		
		select_none = numpy.zeros_like (x, dtype = bool); 
		sparse_mask = select_none.copy (); 
		sparse_mask[0::6, 0::6] = True; 
		
		sparse_x = x[sparse_mask]; 
		sparse_y = y[sparse_mask]; 
		sparse_u = u[sparse_mask]; 
		sparse_v = v[sparse_mask]; 
		
		both_zero = (sparse_u == 0) & (sparse_v == 0); 
		sparse_u[both_zero] = numpy.nan; 
		sparse_v[both_zero] = numpy.nan; 
		
		pp.figure (figsize = (6, 5)); 
		#pp.title ("Velocimetry Result"); 
		pp.xlabel ("Position X (mm)"); 
		pp.ylabel ("Position Y (mm)"); 
		pp.xlim (-r_max - 10, +r_max + 10); 
		pp.ylim (-r_max - 10, +r_max + 10); 
		pp.quiver (sparse_x, sparse_y, sparse_u, sparse_v, numpy.hypot (sparse_u, sparse_v)); 
		pp.colorbar (); 
		pp.savefig (write_dir + "/uv-field.png", dpi = plot_dpi); 
		
		pp.close ("all"); # Close all figures. We already saved them, so yeah. 
	
	
	# Parameters to go into "row", and their order: 
	# I (A)	A_d()	B_d()	delta{B_d}	A_p()	delta{A_p}	
	# Ap yint est	Ap yint std	Ap slope est	Ap slope std
	# 10% w	90% w 
	now_A = 1.0; #float (l.split ("A") [0]); # Parse the number part of "x.xxxA". 
	dA = now_A - last_A; 
	row = numpy.array ([ 
		now_A, 
		A_d, B_d, sigma_Bd, 
		Ap_fit[0], Ap_fit[1], 
		Ap_A[0], Ap_A[1], Ap_B[0], Ap_B[1], 
		numpy.percentile (rav_w[rav_w > 0], 10), 
		numpy.percentile (rav_w[rav_w > 0], 90), 
		Ak[0], Ak[1] 
	]).reshape ((1, -1)); 
	if mod (now_A, 0.001) == 0 and int (dA * 1e3) <= 1: 
		overall_01 = numpy.concatenate ((overall_01, row), axis = 0); 
	if mod (now_A, 0.005) == 0 and int (dA * 1e3) <= 5: 
		overall_05 = numpy.concatenate ((overall_05, row), axis = 0); 
	if mod (now_A, 0.050) == 0 and int (dA * 1e3) <= 50: 
		overall_50 = numpy.concatenate ((overall_50, row), axis = 0); 
	if now_A <= 0.500: 
		overall_500_and_below = numpy.concatenate ((overall_500_and_below, row), axis = 0); 
	if now_A < 0.100: 
		overall_below_100 = numpy.concatenate ((overall_below_100, row), axis = 0); 
	overall_all = numpy.concatenate ((overall_all, row), axis = 0); 
	
	last_A = now_A; 
	
	
	with open (write_dir + "/velocity-profile-fit.txt", "w") as file: 
		print ("Velocity Profile Fit Results: ", file = file); 
		print ("\tAngular Velocity:  w = ({} ± {}) * r^({} ± {}) [deg/s]".format (A_d, sigma_Ad, B_d, sigma_Bd), file = file); 
		print ("\tMagnitude Velocity:  v = ({} ± {}) * r^({} ± {}) [mm/s]".format (A_v, sigma_Av, B_v, sigma_Bv), file = file); 
	
	if meas.shape[0] > 0: 
		with open (write_dir + "/Ap-fit.txt", "w") as file: 
			print ("A_p Fit Results: ", file = file); 
			print ("\tA_p = {} ± {} [mm^3 rad / A s gauss]".format (Ap_fit[0], Ap_fit[1]), file = file); 
			print ("\n", file = file); 
			print ("\tA_p = ({} ± {}) + ({} ± {}) * r".format (Ap_A[0], Ap_A[1], Ap_B[0], Ap_B[1]), file = file); 
			print ("\t\tUnits: [mm^3 rad / A s gauss] + [mm^2 rad / A s gauss] * [mm]", file = file); 
		with open (write_dir + "/Ak-fit.txt", "w") as file: 
			print ("A_k Fit Results: ", file = file); 
			print ("\tA_k = {} ± {}".format (Ak[0], Ak[1]), file = file); 


# Save the overall arrays: \
if not os.path.exists (overall_dir): 
	os.makedirs (overall_dir); 
numpy.savetxt (overall_dir + "/overall-by01mA.csv", overall_01, delimiter = "\t", 
			header = overall_head); 
numpy.savetxt (overall_dir + "/overall-by05mA.csv", overall_05, delimiter = "\t", 
			header = overall_head); 
numpy.savetxt (overall_dir + "/overall-by50mA.csv", overall_50, delimiter = "\t", 
			header = overall_head); 
numpy.savetxt (overall_dir + "/overall-all.csv", overall_all, delimiter = "\t", 
			header = overall_head); 
numpy.savetxt (overall_dir + "/overall-500-and-below.csv", overall_500_and_below, delimiter = "\t", 
			header = overall_head); 
numpy.savetxt (overall_dir + "/overall-below-100.csv", overall_below_100, delimiter = "\t", 
			header = overall_head); 
spyder_is_silly (); 


